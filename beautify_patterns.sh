if [[ $# -ne 2 ]]; then
    echo "Generates more readable (but likely unusable) versions of the bug patterns."
    echo "Usage: beautify_patterns.sh pattern_dir output_dir"
    exit 0
fi

pattern_dir=$1
output_dir=$2

if [[ $pattern_dir == $output_dir ]]; then
    echo "Refusing to use as output directory the pattern directory"
    exit 0
fi

mkdir -p $output_dir

for dot_file in $pattern_dir/*dot; do
    filename=`basename $dot_file`
    readable_dot_file=$output_dir/$filename
    sed 's/|/\|\n/g' $dot_file > $readable_dot_file
done


