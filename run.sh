#!/bin/bash
#
# A simply wrapper which makes easier executing dtls-fuzzer using an arguments file 

# SCRIPT_DIR should correpond to dtls-fuzzer's root directory
readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

opt_jvm_param=''

if [ $# = 0 ]; then 
    echo "Usage run.sh [options] argument_file [dtls-fuzzer params]"
else
    while [[ "$1" =~ ^- ]]; do case $1 in
        -jp | --jvm-param )
            shift
            opt_jvm_param=$1
            ;;
        * )
            echo "Unsupported option $1"
            return
            ;;
        esac; shift; 
    done

    args_file=''
    if [[ -n "$1" ]]; then 
        args_file=@$1
        shift
    fi

    echo "Running: java -jar $opt_jvm_param $SCRIPT_DIR/target/dtls-fuzzer.jar $args_file $@"
    java -jar $opt_jvm_param $SCRIPT_DIR/target/dtls-fuzzer.jar $args_file $@
fi
