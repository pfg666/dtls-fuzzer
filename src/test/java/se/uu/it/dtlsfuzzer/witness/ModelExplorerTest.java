package se.uu.it.dtlsfuzzer.witness;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Iterators;
import com.google.common.collect.Sets;

import net.automatalib.automata.fsa.impl.FastDFA;
import net.automatalib.automata.fsa.impl.FastDFAState;
import net.automatalib.words.Word;
import net.automatalib.words.impl.SimpleAlphabet;
import se.uu.it.dtlsfuzzer.specification.MessageLabel;

public class ModelExplorerTest {

	@Test
	public void testStateVisitBoundTwo() {
		MessageLabel a = new MessageLabel(true, "a");
		MessageLabel b = new MessageLabel(true, "b");
		
		List<MessageLabel> messageLabels = Arrays.asList(a, b); 
		
		FastDFA<MessageLabel> spec = new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		
		FastDFAState s0 = spec.addInitialState(false);
		FastDFAState s1 = spec.addState(false);
		FastDFAState s2 = spec.addState(false);
		FastDFAState s3 = spec.addState(true);
		FastDFAState sink = spec.addState(false);
		spec.addTransition(s0, a, s1);
		spec.addTransition(s0, b, s2);
		spec.addTransition(s1, a, s3);
		spec.addTransition(s1, b, s3);
		spec.addTransition(s2, a, s2);
		spec.addTransition(s2, b, s3);
		spec.addTransition(s3, a, sink);
		spec.addTransition(s3, b, sink);
		
		ModelExplorer<FastDFAState, MessageLabel> explorer = new ModelExplorer<>(spec, messageLabels);
		SearchConfig config = new SearchConfig();
		config.setStateVisitBound(2);
		
		Iterator<Word<MessageLabel>> wordsIter = explorer.wordsToTargetStates(Arrays.asList(s3), config).iterator();
		Set<Word<MessageLabel>> generatedWords = new LinkedHashSet<>();
		Iterators.addAll(generatedWords, wordsIter);
		Set<Word<MessageLabel>> expectedWords = new LinkedHashSet<>(
				Arrays.asList(
						Word.fromSymbols(a, a), 
						Word.fromSymbols(a, b),
						Word.fromSymbols(b, b),
						Word.fromSymbols(b, a, b)));
		
		Assert.assertEquals( expectedWords, generatedWords);
	}

	
	@Test
	public void testStateVisitBoundTwoInitialStateVisitedTwice() {
		MessageLabel a = new MessageLabel(true, "a");
		MessageLabel b = new MessageLabel(true, "b");
		
		List<MessageLabel> messageLabels = Arrays.asList(a, b); 
		
		FastDFA<MessageLabel> spec = new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		
		FastDFAState s0 = spec.addInitialState(false);
		FastDFAState s1 = spec.addState(false);
		FastDFAState s2 = spec.addState(true);
		FastDFAState sink = spec.addState(false);
		spec.addTransition(s0, a, s0);
		spec.addTransition(s0, b, s1);
		spec.addTransition(s1, a, s1);
		spec.addTransition(s1, b, s2);
		spec.addTransition(s2, a, sink);
		spec.addTransition(s2, b, sink);
		
		ModelExplorer<FastDFAState, MessageLabel> explorer = new ModelExplorer<>(spec, messageLabels);
		SearchConfig config = new SearchConfig();
		config.setStateVisitBound(2);
		
		Iterator<Word<MessageLabel>> wordsIter = explorer.wordsToTargetStates(Arrays.asList(s2), config).iterator();
		Set<Word<MessageLabel>> generatedWords = new LinkedHashSet<>();
		Iterators.addAll(generatedWords, wordsIter);
		Set<Word<MessageLabel>> expectedWords = new LinkedHashSet<>(
				Arrays.asList(
						Word.fromSymbols(b, b), 
						Word.fromSymbols(b, a, b),
						Word.fromSymbols(a, b, b),
						Word.fromSymbols(a, b, a, b)));
		
		Assert.assertEquals( expectedWords, generatedWords);
	}
	
	@Test
	public void testStateVisitBoundTwoMinState() {
		MessageLabel a = new MessageLabel(true, "a");
		MessageLabel b = new MessageLabel(true, "b");
		
		List<MessageLabel> messageLabels = Arrays.asList(a, b); 
		
		FastDFA<MessageLabel> spec = new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		
		FastDFAState s0 = spec.addInitialState(false);
		FastDFAState s1 = spec.addState(false);
		FastDFAState s2 = spec.addState(false);
		FastDFAState s3 = spec.addState(true);
		FastDFAState sink = spec.addState(false);
		spec.addTransition(s0, a, s1);
		spec.addTransition(s0, b, s2);
		spec.addTransition(s1, a, s1);
		spec.addTransition(s1, b, s3);
		spec.addTransition(s2, a, s2);
		spec.addTransition(s2, b, s3);
		spec.addTransition(s3, a, sink);
		spec.addTransition(s3, b, sink);
		
		ModelExplorer<FastDFAState, MessageLabel> explorer = new ModelExplorer<>(spec, messageLabels);
		SearchConfig config = new SearchConfig();
		config.setStateVisitBound(2);
		
		Iterator<Word<MessageLabel>> wordsIter = explorer.wordsToTargetStates(Arrays.asList(s3), config).iterator();
		Set<Word<MessageLabel>> generatedWords = new LinkedHashSet<>();
		Iterators.addAll(generatedWords, wordsIter);
		Set<Word<MessageLabel>> expectedWords = new LinkedHashSet<>(
				Arrays.asList(
						Word.fromSymbols(a, b), 
						Word.fromSymbols(b, b),
						Word.fromSymbols(a, a, b),
						Word.fromSymbols(b, a, b)));
		Assert.assertEquals( expectedWords, generatedWords);
		
		Set<Word<MessageLabel>> expectedFirstWords = new LinkedHashSet<>(
				Arrays.asList(
						Word.fromSymbols(a, b), 
						Word.fromSymbols(b, b)));
		Set<Word<MessageLabel>> generatedFirstWords = new LinkedHashSet<>();
		Iterators.addAll(generatedFirstWords, Iterators.limit(generatedWords.iterator(), 2));
		Assert.assertEquals( expectedFirstWords, generatedFirstWords);
	}
}
