package se.uu.it.dtlsfuzzer.witness;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Iterables;

import net.automatalib.automata.fsa.impl.FastDFA;
import net.automatalib.automata.fsa.impl.FastDFAState;
import net.automatalib.words.Word;
import net.automatalib.words.impl.SimpleAlphabet;
import se.uu.it.dtlsfuzzer.specification.MessageLabel;

public class DeviantTransitionSequenceGeneratorTest {

	@Test
	public void testDeviant() {

		MessageLabel a = new MessageLabel(true, "a");
		MessageLabel b = new MessageLabel(true, "b");
		
		List<MessageLabel> messageLabels = Arrays.asList(a, b); 
		
		FastDFA<MessageLabel> spec = new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		
		
		FastDFAState s0 = spec.addInitialState(true);
		FastDFAState s1 = spec.addState(true);
		FastDFAState s2 = spec.addState(true);
		
		spec.addTransition(s0, a, s1);
		spec.addTransition(s1, b, s2);
		
		FastDFA<MessageLabel> bugLanguage = new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		s0 = bugLanguage.addInitialState(false);
		s1 = bugLanguage.addState(false);
		s2 = bugLanguage.addState(false);
		FastDFAState s3 = bugLanguage.addState(true);
		bugLanguage.addTransition(s0, a, s1);
		bugLanguage.addTransition(s0, b, s2);
		bugLanguage.addTransition(s1, a, s3);
		bugLanguage.addTransition(s2, b, s3);
		
		DeviantTransitionSequenceGenerator<MessageLabel> gen = new DeviantTransitionSequenceGenerator<MessageLabel>(spec);
		Iterable<Word<MessageLabel>> seq = gen.generateSequences(bugLanguage, messageLabels);
		Set<Word<MessageLabel>> generatedWords = new LinkedHashSet<>();
		Iterables.addAll(generatedWords, seq);
		Set<Word<MessageLabel>> expectedWords = new LinkedHashSet<Word<MessageLabel>>( Arrays.asList(
				Word.fromSymbols(b, b), Word.fromSymbols(a, a)));
		Assert.assertEquals( expectedWords, generatedWords);
	}

}
