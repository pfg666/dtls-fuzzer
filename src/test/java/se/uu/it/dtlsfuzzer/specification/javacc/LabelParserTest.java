package se.uu.it.dtlsfuzzer.specification.javacc;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import se.uu.it.dtlsfuzzer.specification.BinaryExpressionLabel;
import se.uu.it.dtlsfuzzer.specification.BinarySetOperation;
import se.uu.it.dtlsfuzzer.specification.EnumerationLabel;
import se.uu.it.dtlsfuzzer.specification.FilterLabel;
import se.uu.it.dtlsfuzzer.specification.Label;
import se.uu.it.dtlsfuzzer.specification.MessageLabel;
import se.uu.it.dtlsfuzzer.specification.OtherLabel;

public class LabelParserTest {
	
	@Before
	public void setUp() {
	}
	
	@Test
	public void inputMessageLabel() throws ParseException {
		Label actual = LabelParserFacade.parseLabelString("I_PSK_CLIENT_HELLO");
		MessageLabel expected = new MessageLabel(true, "PSK_CLIENT_HELLO");
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void outputMessageLabel() throws ParseException {
		Label actual = LabelParserFacade.parseLabelString("O_PSK_CLIENT_HELLO");
		MessageLabel expected = new MessageLabel(false, "PSK_CLIENT_HELLO");
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void filterLabel() throws ParseException {
		Label actual = LabelParserFacade.parseLabelString("F_I_Alert.*");
		FilterLabel expected = new FilterLabel("I_Alert.*");
		Assert.assertEquals(expected, actual);
		
		actual = LabelParserFacade.parseLabelString("F_(?!I_CHANGE_CIPHER_SPEC|O_CHANGE_CIPHER_SPEC).*");
		expected = new FilterLabel("(?!I_CHANGE_CIPHER_SPEC|O_CHANGE_CIPHER_SPEC).*");
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void enumerationLabel() throws ParseException {
		Label actual = LabelParserFacade.parseLabelString("{I_Alert, O_Alert}");
		EnumerationLabel expected = new EnumerationLabel(Arrays.asList(
				new MessageLabel(true, "Alert"),
				new MessageLabel(false, "Alert")
				));
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void expressionLabel() throws ParseException {
		Label actual = LabelParserFacade.parseLabelString(OtherLabel.other().toString() + " " + BinarySetOperation.DIFFERENCE.getSign() + " " + "I_Alert");
		BinaryExpressionLabel expected = new BinaryExpressionLabel(OtherLabel.other(), 
				BinarySetOperation.DIFFERENCE, 
				new MessageLabel(true, "Alert"));
		Assert.assertEquals(expected, actual);
		
		actual = LabelParserFacade.parseLabelString(OtherLabel.other().toString() + " " + BinarySetOperation.UNION.getSign() + " " + "I_Alert");
		expected = new BinaryExpressionLabel(OtherLabel.other(), 
				BinarySetOperation.UNION, 
				new MessageLabel(true, "Alert"));
		Assert.assertEquals(expected, actual);
	}

}
