package se.uu.it.dtlsfuzzer.specification;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.FastDFA;
import net.automatalib.automata.fsa.impl.FastDFAState;
import net.automatalib.util.automata.Automata;
import net.automatalib.words.Word;
import net.automatalib.words.impl.SimpleAlphabet;
import se.uu.it.dtlsfuzzer.utils.TestUtils;

/*
 * TODO It would be better if our inputs for specs/expected automata were .dot files, as it would allow us to reduce the code/create better tests.
 */
public class DtlsLanguageAdapterBuilderTest {

	@Test
	public void testUnfoldOtherNonCapturingFilter() {
		MessageLabel a = new MessageLabel(true, "a");
		MessageLabel b = new MessageLabel(true, "b");
		MessageLabel c = new MessageLabel(true, "c");
		
		
		Label other = OtherLabel.other();
		FilterLabel aOrB = new FilterLabel(MessageLabel.INPUT_PREFIX + "(?:a|b)");
		
		List<Label> specLabels = Arrays.asList(a,b,aOrB, other);
		List<MessageLabel> messageLabels = Arrays.asList(a, b, c); 
		
		FastDFA<Label> spec = new FastDFA<>(new SimpleAlphabet<>(specLabels));
		
		FastDFAState s0 = spec.addInitialState(true);
		FastDFAState s1 = spec.addState(true);
		FastDFAState s2 = spec.addState(true);
		FastDFAState sink = spec.addState(false);
		
		spec.addTransition(s0, a, s1);
		spec.addTransition(s0, other, sink);
		spec.addTransition(s1, b, s2);
		spec.addTransition(s1, other, sink);
		spec.addTransition(s2, other, s2);
		spec.addTransition(s2, aOrB, sink);
		spec.addTransition(sink, other, sink);
				
		FastDFA<MessageLabel> unfolded = DtlsLanguageAdapterBuilder.unfold(spec, messageLabels);
		
		Assert.assertArrayEquals(new Object [] {a, b, c}, unfolded.getInputAlphabet().toArray());
		Assert.assertFalse(unfolded.accepts(Arrays.asList(b)));
		Assert.assertFalse(unfolded.accepts(Arrays.asList(a, a)));
		Assert.assertTrue(unfolded.accepts(Arrays.asList(a, b)));
		Assert.assertTrue(unfolded.accepts(Arrays.asList(a, b, c)));
		Assert.assertFalse(unfolded.accepts(Arrays.asList(a, b, a)));
	}
	
	@Test
	public void testUnfoldOtherCapturingFilter() {
		MessageLabel chRsa = new MessageLabel(true, "chrsa");
		MessageLabel chPsk = new MessageLabel(true, "chpsk");
		FilterLabel chStoreKex = new FilterLabel(MessageLabel.INPUT_PREFIX + "ch(.*)");
		FilterLabel chSameKex = new FilterLabel(MessageLabel.INPUT_PREFIX + "ch\\1");
		FilterLabel chDifferentKex = new FilterLabel(MessageLabel.INPUT_PREFIX + "ch(?!\\1).*");
		FilterLabel chAnyKex = new FilterLabel(MessageLabel.INPUT_PREFIX + "ch.*");
		OtherLabel other = OtherLabel.other();
		
		List<Label> specLabels = Arrays.asList(chRsa, chPsk,chSameKex, chStoreKex, chDifferentKex, chAnyKex, other);
		List<MessageLabel> messageLabels = Arrays.asList(chRsa, chPsk); 
		
		FastDFA<Label> spec = new FastDFA<>(new SimpleAlphabet<>(specLabels));
		
		FastDFAState s0 = spec.addInitialState(false);
		FastDFAState s1 = spec.addState(false);
		FastDFAState s2 = spec.addState(true);
		FastDFAState s3 = spec.addState(false);
		
		spec.addTransition(s0, chStoreKex, s1);
		spec.addTransition(s1, chSameKex, s2);
		spec.addTransition(s1, chDifferentKex, s3);
		spec.addTransition(s2, other, s2);
		spec.addTransition(s3, other, s3);
		
		FastDFA<MessageLabel> expected =  new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		s0 = expected.addInitialState(false);
		s1 = expected.addState(false);
		s2 = expected.addState(false);
		s3 = expected.addState(true);
		FastDFAState s4 = expected.addState(false);
		expected.addTransition(s0, chRsa, s1);
		expected.addTransition(s0, chPsk, s2);
		expected.addTransition(s1, chRsa, s3);
		expected.addTransition(s1, chPsk, s4);
		expected.addTransition(s2, chRsa, s4);
		expected.addTransition(s2, chPsk, s3);
		expected.addTransition(s3, chRsa, s3);
		expected.addTransition(s3, chPsk, s3);
		expected.addTransition(s4, chRsa, s4);
		expected.addTransition(s4, chPsk, s4);
		
		
		FastDFA<MessageLabel> unfolded = DtlsLanguageAdapterBuilder.unfold(spec, messageLabels);
		assertEquivalent(expected, unfolded, messageLabels, spec, specLabels);
	}
	
	@Test
	public void testUnfoldActual() {
		MessageLabel chRsa = new MessageLabel(true, "chrsa");
		MessageLabel chPsk = new MessageLabel(true, "chpsk");
		MessageLabel hvr = new MessageLabel(false, "hvr");
		FilterLabel chAny = new FilterLabel(MessageLabel.INPUT_PREFIX + "ch.*");
		MessageLabel sh = new MessageLabel(false, "sh");
		OtherLabel other = OtherLabel.other();
		FilterLabel anyOutput = new FilterLabel(MessageLabel.OUTPUT_PREFIX + ".*");
		

		List<Label> specLabels = Arrays.asList(chRsa, chPsk, hvr, chAny, anyOutput, sh, other);
		List<MessageLabel> messageLabels = Arrays.asList(chRsa, chPsk, hvr, sh); 
		
		FastDFA<Label> spec = new FastDFA<>(new SimpleAlphabet<>(specLabels));
		
		FastDFAState s0 = spec.addInitialState(false);
		FastDFAState s1 = spec.addState(false);
		FastDFAState s2 = spec.addState(false);
		FastDFAState s3 = spec.addState(false);
		FastDFAState s4 = spec.addState(true);
		FastDFAState sink = spec.addState(false);
		
		spec.addTransition(s0, anyOutput, s4);
		spec.addTransition(s0, chRsa, s1);
		spec.addTransition(s0, other, s1);
		spec.addTransition(s1, other, s4);
		spec.addTransition(s1, hvr, s2);
		spec.addTransition(s2, other, s4);
		spec.addTransition(s2, chAny, s3);
		spec.addTransition(s3, other, s4);
		spec.addTransition(s3, sh, sink);
		spec.addTransition(s4, other, s4);
		spec.addTransition(sink, other, sink);
		
		FastDFA<MessageLabel> unfolded = DtlsLanguageAdapterBuilder.unfold(spec, messageLabels);
		Assert.assertArrayEquals(messageLabels.toArray(), unfolded.getInputAlphabet().toArray());
		Assert.assertTrue(unfolded.accepts(Arrays.asList(hvr)));
		Assert.assertFalse(unfolded.accepts(Arrays.asList(chPsk)));
	}
	
	@Test
	public void testUnfoldEnumeration() {
		MessageLabel a = new MessageLabel(true, "a");
		MessageLabel b = new MessageLabel(true, "b");
		MessageLabel c = new MessageLabel(true, "c");
		MessageLabel d = new MessageLabel(true, "d");
		OtherLabel other = OtherLabel.other();
		EnumerationLabel bOrC = new EnumerationLabel(Arrays.asList(b, c));
		List<Label> specLabels = Arrays.asList(a, b, c, d, other, bOrC);
		List<MessageLabel> messageLabels = Arrays.asList(a, b, c, d);
		
		FastDFA<Label> spec = new FastDFA<>(new SimpleAlphabet<>(specLabels));
		
		FastDFAState s0 = spec.addInitialState(false);
		FastDFAState s1 = spec.addState(false);
		FastDFAState s2 = spec.addState(true);
		
		spec.addTransition(s0, bOrC, s2);
		spec.addTransition(s0, d, s1);
		spec.addTransition(s0, other, s0);
		spec.addTransition(s1, other, s1);
		spec.addTransition(s2, other, s2);	
		
		FastDFA<MessageLabel> expected = new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		s0 = expected.addInitialState(false);
		s1 = expected.addState(false);
		s2 = expected.addState(true);
		
		expected.addTransition(s0, a, s0);
		expected.addTransition(s0, b, s2);
		expected.addTransition(s0, c, s2);
		expected.addTransition(s0, d, s1);
		expected.addTransition(s1, a, s1);
		expected.addTransition(s1, b, s1);
		expected.addTransition(s1, c, s1);
		expected.addTransition(s1, d, s1);
		expected.addTransition(s2, a, s2);
		expected.addTransition(s2, b, s2);
		expected.addTransition(s2, c, s2);
		expected.addTransition(s2, d, s2);
		
		FastDFA<MessageLabel> unfolded = DtlsLanguageAdapterBuilder.unfold(spec, messageLabels);
		assertEquivalent(expected, unfolded, messageLabels, spec, specLabels);
	}
	
	@Test
	public void testUnfoldOtherMinusEnumeration() {
		MessageLabel a = new MessageLabel(true, "a");
		MessageLabel b = new MessageLabel(true, "b");
		MessageLabel c = new MessageLabel(true, "c");
		OtherLabel other = OtherLabel.other();
		BinaryExpressionLabel otherThanC = new BinaryExpressionLabel(other, BinarySetOperation.DIFFERENCE, new EnumerationLabel(Arrays.asList(c)));
		List<Label> specLabels = Arrays.asList(a, b, c, otherThanC);
		List<MessageLabel> messageLabels = Arrays.asList(a, b, c);
		
		FastDFA<Label> spec = new FastDFA<>(new SimpleAlphabet<>(specLabels));
		
		FastDFAState s0 = spec.addInitialState(false);
		FastDFAState s1 = spec.addState(true);
		
		spec.addTransition(s0, b, s0);
		spec.addTransition(s0, otherThanC, s1);
		
		FastDFA<MessageLabel> expectedMut = new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		s0 = expectedMut.addInitialState(false);
		s1 = expectedMut.addState(true);
		expectedMut.addTransition(s0, a, s1);
		expectedMut.addTransition(s0, b, s0);
		
		DFA<?, MessageLabel> expected = TestUtils.completeDFA(expectedMut, messageLabels);
		FastDFA<MessageLabel> unfolded = DtlsLanguageAdapterBuilder.unfold(spec, messageLabels);
		assertEquivalent(expected, unfolded, messageLabels, spec, specLabels);
	}
	
	@Test
	public void testUnfoldUnionEnumeration() {
		MessageLabel a = new MessageLabel(true, "a");
		MessageLabel b = new MessageLabel(true, "b");
		MessageLabel c = new MessageLabel(true, "c");
		BinaryExpressionLabel aOrC = new BinaryExpressionLabel(new EnumerationLabel(Arrays.asList(a)), BinarySetOperation.UNION, new EnumerationLabel(Arrays.asList(c)));
		List<Label> specLabels = Arrays.asList(a, b, c, aOrC);
		List<MessageLabel> messageLabels = Arrays.asList(a, b, c);
		
		FastDFA<Label> spec = new FastDFA<>(new SimpleAlphabet<>(specLabels));
		
		FastDFAState s0 = spec.addInitialState(false);
		FastDFAState s1 = spec.addState(true);
		
		spec.addTransition(s0, b, s0);
		spec.addTransition(s0, aOrC, s1);
		
		FastDFA<MessageLabel> expectedMut = new FastDFA<>(new SimpleAlphabet<>(messageLabels));
		s0 = expectedMut.addInitialState(false);
		s1 = expectedMut.addState(true);
		expectedMut.addTransition(s0, a, s1);
		expectedMut.addTransition(s0, b, s0);
		expectedMut.addTransition(s0, c, s1);
		
		DFA<?, MessageLabel> expected = TestUtils.completeDFA(expectedMut, messageLabels);
		FastDFA<MessageLabel> unfolded = DtlsLanguageAdapterBuilder.unfold(spec, messageLabels);
		assertEquivalent(expected, unfolded, messageLabels, spec, specLabels);
	}
	
	private void assertEquivalent(DFA<?, MessageLabel> expected, DFA<?, MessageLabel> actual, Collection<MessageLabel> messageLabels, DFA<?, Label> spec, Collection<Label> specLabels) {
		Word<MessageLabel> sepWord = Automata.findSeparatingWord(expected, actual, messageLabels);
		Assert.assertNull(String.format("The DFA resulting from the unfolding of a test specification differs from what is expected. \n"
				+ "Specification: %s\n Expected: %s\n Actual: %s\n SepWord: %s\n",
				TestUtils.getAutomataString(spec, specLabels),
				TestUtils.getAutomataString(expected, messageLabels),
				TestUtils.getAutomataString(actual, messageLabels),
				sepWord), sepWord);
	}
}
