# Bugs and Patterns
We define patterns for various client/server bugs that are detectable in learned models.
The bugs were derived from two sources.
The first is the state-of-the-art on testing DTLS/TLS implementation 
This includes prior state-fuzzing ventures, model-based testing and other papers describing relevant control flow bugs that may affect DTLS/TLS.

The second source is conformance testing to a manually-written Handshake protocol specification, which uses the supported abstract input/output messages.
SUT traces that violate this specification capture non-conformance bugs, and are used to define new bug patterns.
Note that all the bugs derived from the first source represent non-conformance bugs, and can be derived as such.
The fact they were reported in prior works makes them more significant (they were significant enough to warrant reporting, etc.).


## Uncategorized bugs
Uncategorized bugs comprise invalid behavior with respect to Handshake protocol specification that is not captured by any of the defined bug patterns.
We strive to eliminate uncategorized bugs, that is, categorize all undesirable behaviors in the model in reference to the specification. 
This is done by:
1. defining new bug patterns (e.g. such as the bug patterns present here but not in the USENIX work)
2. adapting existing bug patterns by making them more general. 
  
  
We log below these adaptations:

* Bug state self-loop in all bug patterns (this ensures continuations of traces exhibiting a bug are categorized as having the same bug)   
* Rehandshake transitions in most bug patterns, ensuring the bug pattern applies to both the initial handshake, as well as to handshake restarts 