package se.uu.it.dtlsfuzzer.specification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;
import se.uu.it.dtlsfuzzer.bugcheck.ModelOutputs;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;
import se.uu.it.dtlsfuzzer.utils.Flow;
import se.uu.it.dtlsfuzzer.utils.MealyUtils;

/**
 * Class implementing the mapping from TlsInput/Outputs to MessageLabels and back.
 */
public class DtlsLabelMapping {
	
	/**
	 * The number of times a repeating output will be unrolled. 
	 */
	private static final int NUMBER_UNROLLINGS = 2;

	public static DtlsLabelMapping buildMapping(MealyMachine<?, TlsInput, ?, TlsOutput> mealy, Collection<TlsInput> inputs, boolean client) {
		List<TlsOutput> outputs = new LinkedList<>();
		MealyUtils.reachableOutputs(mealy, inputs, outputs);
		Map<TlsOutput, MessageLabel> outputMaping = new LinkedHashMap<>();
		for (TlsOutput output : outputs) {
			if (!output.isTimeout()) {
				for (TlsOutput atomicOutput : output.getRepeatedOutput().getAtomicOutputs()) {
					if (!outputMaping.containsKey(atomicOutput)) {
						outputMaping.put(atomicOutput, new MessageLabel(false, atomicOutput.getAbstractOutput()));
					}
				} 
			}
		}
		
		Map<TlsInput, MessageLabel> inputMapping = new LinkedHashMap<>();
		for (TlsInput input : inputs) {
			inputMapping.put(input, new MessageLabel(true, input.getName()));
		}
		return new DtlsLabelMapping(inputMapping, outputMaping, client);
	}

	private Map<TlsInput, MessageLabel> inputMapping;
	private Map<TlsOutput, MessageLabel> outputMapping;
	private boolean client;
	
	private DtlsLabelMapping(Map<TlsInput, MessageLabel> inputMapping, Map<TlsOutput, MessageLabel> outputMapping, boolean client) {
		this.inputMapping = inputMapping;
		this.outputMapping = outputMapping;
		this.client = client;
	}
	
	public List<MessageLabel> getIOLabels() {
		List<MessageLabel> labels = new ArrayList<MessageLabel>(inputMapping.size() + outputMapping.size());
		labels.addAll(inputMapping.values());
		labels.addAll(outputMapping.values());
		return labels;
	}
	
	public MessageLabel labelForInput(TlsInput input) {
		return inputMapping.get(input);
	}
	
	public List<MessageLabel> labelsForOutput(TlsOutput output) {
		List<MessageLabel> messageLabelList = new ArrayList<>(); 
		addOutputLabels(output, messageLabelList);
		return messageLabelList;
	}
	

	public Word<TlsInput> toTlsInputWord(Word<MessageLabel> word) {
		WordBuilder<TlsInput> builder = new WordBuilder<TlsInput>();
		word.stream().filter(l -> l.isInput()).forEach(l -> builder.add(inputForLabel(l)));
		return builder.toWord();
	}
	
	public Word<MessageLabel> toLabelWord(Flow<TlsInput, TlsOutput, ?> flow) {
		WordBuilder<MessageLabel> builder = new WordBuilder<>();
		boolean processedFirst = false;
		for(Pair<TlsInput,TlsOutput> io : flow.getInputOutputIterable()) {
			// in our client models, the initial CH message the client sends (before we have sent anything) is seen as an output to the first message we send 
			if (client && !processedFirst) {
				List<TlsOutput> atomicOutputs = new ArrayList<>(io.getSecond().getAtomicOutputs(NUMBER_UNROLLINGS));
				TlsOutput initialCH = atomicOutputs.remove(0);
				addOutputLabels(initialCH, builder);
				builder.append(new MessageLabel(true, io.getFirst().getName()));
				for (TlsOutput output : atomicOutputs) {
					addOutputLabels(output, builder);
				}
				
				processedFirst = true;
			} else {
				builder.append(new MessageLabel(true, io.getFirst().getName()));
				addOutputLabels(io.getSecond(), builder);
			}
		}
		
		// required by other methods, should be fixed once we find a proper way of handling initial CH messages.
		if (flow.getLength() == 0 && client) {
			builder.add(new MessageLabel(false, ModelOutputs.getClientHelloString()));
		}
		
		return builder.toWord();
	}
	
	private void addOutputLabels(TlsOutput output, Collection<MessageLabel> collection) {
		if (!output.isTimeout()) {
			if (output.isRepeating()) {
				TlsOutput repeatingOutput = output.getRepeatedOutput();
				for (int i=0; i < NUMBER_UNROLLINGS; i++) {
					collection.add(new MessageLabel(false, repeatingOutput.getAbstractOutput()));
				}
			} else {
				if (output.isComposite()) {
					for (TlsOutput constituentOutput : output.getAtomicOutputs(NUMBER_UNROLLINGS)) {
						addOutputLabels(constituentOutput, collection);
					}
				} else {
					collection.add(new MessageLabel(false, output.getAbstractOutput()));
				}
			}
		}
	} 
	
	
	public TlsInput inputForLabel(MessageLabel label) {
		return inputMapping.entrySet().stream().filter(e -> e.getValue().equals(label)).map(e -> e.getKey()).findFirst().orElse(null);
	}
	
	public TlsOutput outputForLabel(MessageLabel label) {
		return outputMapping.entrySet().stream().filter(e -> e.getValue().equals(label)).map(e -> e.getKey()).findFirst().orElse(null);
	}
}
