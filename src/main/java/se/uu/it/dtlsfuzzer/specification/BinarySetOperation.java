package se.uu.it.dtlsfuzzer.specification;

public enum BinarySetOperation {
	DIFFERENCE("-"),
	UNION("U");
	
	private String sign;

	private BinarySetOperation(String sign) {
		this.sign = sign;
	}
	
	public String getSign() {
		return sign;
	}
}
