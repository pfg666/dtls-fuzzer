package se.uu.it.dtlsfuzzer.specification;

public class OtherLabel extends Label{
	private static OtherLabel OTHER_LABEL;
	
	public static OtherLabel other() {
		if (OTHER_LABEL == null) {
			OTHER_LABEL = new OtherLabel();
		}
		return OTHER_LABEL;
	}

	private OtherLabel() {
		super();
	}
	
	public boolean requiresOtherComputation() {
		return true;
	}

	@Override
	public LabelType getType() {
		return LabelType.OTHER;
	}

	@Override
	public String toString() {
		return "other";
	}
}
