package se.uu.it.dtlsfuzzer.specification;

public class InvalidLabelException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidLabelException(String label) {
		super("Label " + label + " is invalid. ");
	}
	
	public InvalidLabelException(String label, LabelType type) {
		super("Label " + label + " of type " + type.name() + " is invalid.");
	}

	public InvalidLabelException(String label, LabelType type, Throwable exception) {
		super("Label " + label + " of type " + type.name() + " is invalid.", exception);
	}

	public InvalidLabelException(String label, Throwable exception) {
		super("Label " + label + " is invalid.", exception);
	}
}
