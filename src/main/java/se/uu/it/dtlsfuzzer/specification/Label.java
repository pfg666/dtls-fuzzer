package se.uu.it.dtlsfuzzer.specification;

import java.io.IOException;

public abstract class Label {
	
	public abstract LabelType getType();
	
	/**
	 * How the label should look when printed.
	 */
	public abstract String toString();
	
	public boolean requiresOtherComputation() {
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getType().ordinal();
		result = prime * result + ((toString() == null) ? 0 : toString().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Label other = (Label) obj;
		if (toString() == null) {
			if (other.toString() != null)
				return false;
		} else if (!toString().equals(other.toString()))
			return false;
		return this.getType().equals(other.getType());
	}
	
	
	public static void main(String args[]) throws IOException {
		byte[] bytes = new byte[20];
		int read = System.in.read(bytes);
		System.out.println(read);
	}
}
