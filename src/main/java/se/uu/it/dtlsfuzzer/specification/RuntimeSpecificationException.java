package se.uu.it.dtlsfuzzer.specification;

public class RuntimeSpecificationException  extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6181930060058549481L;

	public RuntimeSpecificationException(String message) {
		super(message);
	}

}
