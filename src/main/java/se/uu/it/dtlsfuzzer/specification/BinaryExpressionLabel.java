package se.uu.it.dtlsfuzzer.specification;

public class BinaryExpressionLabel extends Label {

	private BinarySetOperation operation;
	private Label left;
	private Label right;
	
	public BinaryExpressionLabel(Label left, BinarySetOperation operation, Label right) {
		this.left = left;
		this.operation = operation;
		this.right = right;
	}
	
	public boolean requiresOtherComputation() {
		return left.requiresOtherComputation() || right.requiresOtherComputation();
	}
	
	
	
	public BinarySetOperation getOperation() {
		return operation;
	}

	public Label getLeft() {
		return left;
	}

	public Label getRight() {
		return right;
	}

	@Override
	public LabelType getType() {
		return LabelType.BINARY_EXPRESSION;
	}

	@Override
	public String toString() {
		return left + " " + operation.getSign() + " " + right;
	}
	

}
