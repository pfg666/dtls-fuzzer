package se.uu.it.dtlsfuzzer.specification;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Iterators;

import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.words.Word;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;
import se.uu.it.dtlsfuzzer.utils.Flow;

public class TlsFlow extends Flow<TlsInput, TlsOutput, TlsFlow> {

	public static final <S> TlsFlow  buildTlsFlow(MealyMachine<S, TlsInput, ?, TlsOutput> machine, Word<TlsInput> inputWord, boolean fromStart) {
		return new TlsFlow(inputWord, machine.computeOutput(inputWord), fromStart); 
	}
	
	TlsFlow(Word<TlsInput> input, Word<TlsOutput> output, boolean fromStart) {
		super(input, output, fromStart);
	}
	
	public TlsFlow(Word<TlsInput> input, Word<TlsOutput> output) {
		super(input, output, true);
	}

	@Override
	protected TlsFlow build(Word<TlsInput> input, Word<TlsOutput> output, boolean fromStart) {
		return new TlsFlow(input, output, fromStart);
	}
	
	public TlsOutput lastOutput() {
		if (super.getOutputWord().isEmpty()) {
			return null;
		} else {
			return super.getOutputWord().lastSymbol();
		}
	}
	
	public int inputFlightSize() {
		return Iterators.size(inputFlightIterator());
	}
	
	public TlsInputFlight getInputFlight(int index) {
		return Iterators.get(inputFlightIterator(), index, null);
	}
	
	public Iterable<TlsInputFlight> inputFlights() {
		return new Iterable<TlsInputFlight>() {
			public Iterator<TlsInputFlight> iterator() {
				return inputFlightIterator();
			}
		};
	}
	
	public Iterator<TlsInputFlight> inputFlightIterator() {
		return new InputFlightIterator();
	}
	
	private class InputFlightIterator implements Iterator<TlsInputFlight> {
		private int idx=0;

		@Override
		public boolean hasNext() {
			return idx < TlsFlow.this.getLength();
		}

		@Override
		public TlsInputFlight next() {
			TlsInputFlight flight = null;
			if (hasNext()) {
				flight = new TlsInputFlight(idx);
				while(hasNext() && !TlsFlow.this.getOutput(idx).isRecordResponse()) {
					flight.add(TlsFlow.this.getInput(idx));
					idx ++;
				}
				
				if (hasNext()) {
					flight.add(TlsFlow.this.getInput(idx));
					idx ++;
				}
			}
			return flight;
		}
	}
	
	public List<String> getAtomicOutputRecordResponses() {
		return outputFlights().stream()
				.flatMap(of -> of.stream())
				.collect(Collectors.toList());
	}
	
	public List<String> getAtomicOutputRecordResponses(int unrollIndex) {
		return outputFlights().stream()
				.flatMap(of -> of.stream())
				.collect(Collectors.toList());
	}
	
	public int outputFlightSize() {
		return Iterators.size(outputFlightIterator());
	}
	
	public Iterator<TlsOutputFlight> outputFlightIterator() {
		return new OutputFlightIterator();
	}
	
	public List<TlsOutputFlight> outputFlights() {
		List<TlsOutputFlight> flights = new ArrayList<>(this.getLength());
		outputFlightIterator().forEachRemaining(f -> flights.add(f));
		return flights;
	}
	
	public TlsOutputFlight getOutputFlight(int index) {
		return Iterators.get(outputFlightIterator(), index, null);
	}
	
	
	private class OutputFlightIterator implements Iterator<TlsOutputFlight> {
		private int idx=0;

		@Override
		public boolean hasNext() {
			for (int fIdx=idx; fIdx <TlsFlow.this.getLength(); fIdx++) {
				if (TlsFlow.this.getOutput(fIdx).isRecordResponse()) {
					return true;
				}
			}
			return false;
		}

		@Override
		public TlsOutputFlight next() {
			TlsOutputFlight flight = null;
			if (hasNext()) {
				while(hasNext() && !TlsFlow.this.getOutput(idx).isRecordResponse()) {
					idx ++;
				}
				
				if (hasNext()) {
					flight = new TlsOutputFlight(TlsFlow.this.getOutput(idx).getAtomicAbstractOutputs(), idx);
					idx ++;
				}
			}
			return flight;
		}
	}
}
