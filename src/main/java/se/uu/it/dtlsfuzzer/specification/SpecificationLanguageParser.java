package se.uu.it.dtlsfuzzer.specification;

import java.io.FileNotFoundException;
import java.io.Reader;

import se.uu.it.dtlsfuzzer.specification.javacc.ParseException;
import se.uu.it.dtlsfuzzer.specification.javacc.TokenMgrError;

import com.pfg666.dotparser.fsm.dfa.AbstractDFAProcessor;
import com.pfg666.dotparser.fsm.dfa.DFADotParser;

import net.automatalib.automata.fsa.impl.FastDFA;
import se.uu.it.dtlsfuzzer.specification.javacc.LabelParserFacade;

public class SpecificationLanguageParser {

	private DFADotParser<Label> dotParser;

	public SpecificationLanguageParser() {
		this.dotParser = new DFADotParser<Label>( new SpecificationLabelProcessor());
	}
	
	public FastDFA<Label> parseDfaModel(Reader specSource) throws FileNotFoundException, ParseException, com.alexmerz.graphviz.ParseException {
		return dotParser.parseAutomaton(specSource).get(0);
	}
	
	static class SpecificationLabelProcessor extends AbstractDFAProcessor<Label>{

		@Override
		protected Label processDFALabel(String labelString) {
			labelString = labelString.replace("\\\\", "\\");
			try {
				return LabelParserFacade.parseLabelString(labelString);
			} catch (ParseException | TokenMgrError e) {
				throw new InvalidLabelException(labelString, e);
			}
		}
	}
}
