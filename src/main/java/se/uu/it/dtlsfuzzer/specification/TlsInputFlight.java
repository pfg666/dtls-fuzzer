package se.uu.it.dtlsfuzzer.specification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import se.uu.it.dtlsfuzzer.sut.input.TlsInput;

public class TlsInputFlight extends ArrayList<TlsInput>{

	private static final long serialVersionUID = 1L;
	
	private int startIndex;
	
	TlsInputFlight(int startIndex) {
		this(Collections.emptyList(), startIndex);
	}
	
	TlsInputFlight(Collection<TlsInput> inputs,int startIndex) {
		super(inputs);
		this.startIndex = startIndex;
	}
	
	public List<TlsInputFlight> split(Predicate<TlsInput> pred) {
		List<TlsInputFlight> flightList = new ArrayList<TlsInputFlight>(super.size());
		if (super.isEmpty()) {
			return flightList;
		}
		TlsInputFlight crtFlight = new TlsInputFlight(startIndex);
		int crtIdx = startIndex;
		flightList.add(crtFlight);
		for (TlsInput input : this) {
			if (pred.test(input)) {
				crtFlight = new TlsInputFlight(crtIdx);
				flightList.add(crtFlight);
			} else {
				crtFlight.add(input);
			}
			crtIdx ++;
		}
		return flightList;
	}
	
	/**
	 * The input index the flight starts at in the flow containing it.
	 */
	public int getStartIndex() {
		return startIndex;
	}
	
	public String toString() {
		return "(startIndex=" + startIndex +")" + super.toString();
	}
}
