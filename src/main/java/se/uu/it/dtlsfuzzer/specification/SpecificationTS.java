package se.uu.it.dtlsfuzzer.specification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.automatalib.automata.fsa.DFA;
import net.automatalib.commons.util.mappings.Mapping;
import net.automatalib.ts.acceptors.DeterministicAcceptorTS;

/**
 * A transition system (TS) over {@link MessageLabel} used to unfold the specification DFA.
 * The TS maintains a set of registers for storing capture groups following transitions on filter labels. 
 * A state in the TS is a state of the specification X valuation over the registers. 
 * For a state in the TS and a MessageLabel, the TS uses the specification to determine the destination state (which means destination state in the specification plus the new valuation). 
 * 
 * The types of such labels currently supported are:
 * <ul>
 * 	<li>{@link MessageLabel} </li>
 * 	<li>{@link FilterLabel} </li>
 * 	<li>{@link OtherLabel} </li>
 *  <li> {@link EnumerationLabel} </li>
 *  <li> {@link BinaryExpressionLabel} </li>
 * </ul>
 * 
 * For simplicity, registers maintain their values by default. 
 */

public class SpecificationTS <S> implements DeterministicAcceptorTS<RegisterState<S,String>, MessageLabel>
{
	private static final Logger LOGGER = LogManager.getLogger(SpecificationTS.class.getName());
	
	private Map<String, Pattern> patternCache;
	
	private DFA<S, Label> specification;
	private Mapping<S,Collection<Label>> stateLabelsMapping;
	private RegisterState<S, String> initial;
	private RegisterState<S, String> sink;
	

	public SpecificationTS(DFA<S, Label> specification, S sink, Mapping<S,Collection<Label>> specificationLabels, Collection<MessageLabel> messageLabels) {
		this.specification = specification;
		this.stateLabelsMapping = specificationLabels;
		this.initial = new RegisterState<S, String> (specification.getInitialState(), Collections.emptyList());
		this.sink = new RegisterState<S, String> (sink, null);
		this.patternCache = new HashMap<>();
	}

    public RegisterState<S, String> getInitialState() {
        return initial;
    }

	@Override
	public Collection<RegisterState<S, String>> getTransitions(RegisterState<S, String> state, MessageLabel input) {
		return Arrays.asList(getTransition(state, input));
	}

	@Override
	public Set<RegisterState<S, String>> getInitialStates() {
		return Collections.singleton(initial); 
	}

	@Override
	public RegisterState<S, String> getSuccessor(RegisterState<S, String> state, MessageLabel messageLabel) {
		if (state.equals(sink)) {
			return sink;
		} else {
			S dfaState = state.getState();
			Collection<Label> localSpecificationLabels = stateLabelsMapping.get(dfaState);
			Label otherLabel = null;
			
			// variables used to check for non-determinism/inconsistency in the specification
			RegisterState<S,String> nextState = null, nextStateCandidate = null;
			Label previouslyMatchedLabel = null;
			
			for (Label specificationLabel : localSpecificationLabels) {
				if (specificationLabel.requiresOtherComputation()) {
					if (otherLabel == null) {
						otherLabel = specificationLabel;
						continue;
					} else {
						throw new RuntimeSpecificationException(
								String.format("Multiple other-dependent transitions from state %s, causing ambiguity. \n"
										+ "Other transitions: %s and %s", dfaState, otherLabel, specificationLabel));
					}
				} else {
					nextStateCandidate = transition(state, messageLabel, specificationLabel);
					if (nextStateCandidate != null) {
						if (previouslyMatchedLabel != null) {
								throw new RuntimeSpecificationException(
										String.format("Non-determinism in state %s. \n"
										+ "The concrete label %s can trigger two transitions: %s and %s.", 
										dfaState, messageLabel, previouslyMatchedLabel, specificationLabel) );
						} else {
							nextState = nextStateCandidate;
							previouslyMatchedLabel = specificationLabel;	
						}
					}
				}
			}
			
			if (nextState == null) {
				if (otherLabel != null) {
					nextState = transition(state, messageLabel, otherLabel);
					if (nextState != null) {
						return nextState;
					}
				} 
				return sink;
			} else {
				return nextState;
			}
		}
	}
	
	private RegisterState<S, String> transition(RegisterState<S, String> state, MessageLabel messageLabel, Label specificationLabel) {
		S specificationState = state.getState();
		S nextSpecificationState = specification.getTransition(specificationState, specificationLabel);
		RegisterState<S, String> nextState = null;
		if (nextSpecificationState != null) {
			nextState = transition(state, messageLabel, specificationLabel, nextSpecificationState);
		}
		return nextState;
	}
	
	private RegisterState<S, String> transition(RegisterState<S, String> state, MessageLabel messageLabel, Label specificationLabel, S nextSpecificationState ) {
		RegisterState<S, String> nextState = null;
		List<String> nextValuation = new ArrayList<>(state.getValuation());
		switch(specificationLabel.getType()) {
		case MESSAGE:
			if (specificationLabel.equals(messageLabel)) {
				nextState =  new RegisterState<S, String> (nextSpecificationState, nextValuation);
			}
			break;
			
		case FILTER:
			FilterLabel filterLabel = (FilterLabel) specificationLabel;
			String regex = filterLabel.getRegexFilter();
			String resolvedRegex = resolveBackRefs(state.getValuation(), regex);
			Pattern pattern = patternCache.get(resolvedRegex);
			if (pattern == null) {
				pattern = Pattern.compile(resolvedRegex);
				patternCache.put(resolvedRegex, pattern);
			}
			Matcher matcher = pattern.matcher(messageLabel.toString());
			
			if (matcher.matches()) {
				for (int i=1; i<=matcher.groupCount(); i++) {
					if (i>nextValuation.size()) {
						nextValuation.add(matcher.group(i));
					} else {
						nextValuation.set(i-1, matcher.group(i));
					}
				}
				nextState = new RegisterState<S, String> (nextSpecificationState, nextValuation);
			}
			break;
		
		case ENUMERATION:
			EnumerationLabel enumerationLabel = (EnumerationLabel) specificationLabel;
			if (enumerationLabel.containsLabel(messageLabel)) {
				nextState = new RegisterState<S, String> (nextSpecificationState, nextValuation);
			}
			break;
			
		case OTHER:
			nextState = new RegisterState<S, String> (nextSpecificationState, nextValuation);
			break;

		case BINARY_EXPRESSION:
			BinaryExpressionLabel expressionLabel = (BinaryExpressionLabel) specificationLabel;
			BinarySetOperation operation = expressionLabel.getOperation();
			Supplier<RegisterState<S, String>> nextStateLeft = () -> transition(state, messageLabel, expressionLabel.getLeft(), nextSpecificationState);
			Supplier<RegisterState<S, String>> nextStateRight = () -> transition(state, messageLabel, expressionLabel.getRight(), nextSpecificationState);
			switch (operation) {
			case DIFFERENCE:
				if (nextStateLeft.get() != null && nextStateRight.get() == null) {
					nextState = new RegisterState<S, String> (nextSpecificationState, nextValuation);
				}
				break;
			case UNION:
				if (nextStateLeft.get() != null || nextStateRight.get() != null) {
					return nextState = new RegisterState<S, String> (nextSpecificationState, nextValuation);
				}
				break;
			default:
				throw new RuntimeSpecificationException(String.format("Unsupported binary operation type %s", specificationLabel.getType().name()));
			}
			break;
		
		default:
			throw new RuntimeSpecificationException(String.format("Unsupported label type %s", specificationLabel.getType().name())); 
		}
		return nextState;
	}
	
	private String resolveBackRefs(List<String> valuation, String regex) {
		for (int i=0; i<valuation.size(); i++) {
			regex = regex.replace("\\" + (i+1), valuation.get(i));
		}
		return regex;
	}

	@Override
	public RegisterState<S, String> getTransition(RegisterState<S, String> state, MessageLabel input) {
		RegisterState<S, String> successor = getSuccessor(state, input);
		return successor;
	}



	@Override
	public Boolean getStateProperty(RegisterState<S, String> state) {
		return specification.getStateProperty(state.getState());
	}

	@Override
	public Void getTransitionProperty(RegisterState<S, String> transition) {
		return specification.getTransitionProperty(transition.getState());
	}

	@Override
	public boolean isAccepting(RegisterState<S, String> state) {
		if (state.equals(sink)) {
			return false;
		} else {
			return specification.isAccepting(state.getState());
		}
	}
	
}
