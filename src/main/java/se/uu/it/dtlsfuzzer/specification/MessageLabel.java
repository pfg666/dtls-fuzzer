package se.uu.it.dtlsfuzzer.specification;

public class MessageLabel extends Label {
	
	public static final String INPUT_PREFIX="I_";
	public static final String OUTPUT_PREFIX="O_";
	private boolean input;
	private String messageString;
	
	public MessageLabel(boolean input, String messageString) {
		this.messageString = messageString;
		this.input = input;
	}

	@Override
	public LabelType getType() {
		return LabelType.MESSAGE;
	}
	
	public boolean isInput() {
		return input;
	}
	
	public String getMessageString() {
		return messageString;
	}

	@Override
	public String toString() {
		return (input? INPUT_PREFIX : OUTPUT_PREFIX) + messageString;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		return prime * (input ? 0 : 1) + super.hashCode(); 
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return this.input == ((MessageLabel) obj).isInput();
		}
		return false;
	}

}
