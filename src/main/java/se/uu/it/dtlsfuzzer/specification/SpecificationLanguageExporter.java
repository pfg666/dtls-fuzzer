package se.uu.it.dtlsfuzzer.specification;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import net.automatalib.automata.fsa.DFA;
import net.automatalib.serialization.dot.GraphDOT;

public class SpecificationLanguageExporter {
	
	public static void export(DFA<?, Label> language, Collection<Label> alphabet, Writer writer) throws IOException {
		GraphDOT.write(language, alphabet, writer);
	}
}
