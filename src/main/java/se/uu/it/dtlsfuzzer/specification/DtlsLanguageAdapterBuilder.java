package se.uu.it.dtlsfuzzer.specification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import net.automatalib.automata.fsa.impl.FastDFA;
import net.automatalib.automata.fsa.impl.FastDFAState;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.commons.util.Pair;
import net.automatalib.commons.util.mappings.Mapping;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.ts.copy.TSCopy;
import net.automatalib.util.ts.traversal.TSTraversalMethod;
import net.automatalib.words.impl.SimpleAlphabet;
import se.uu.it.dtlsfuzzer.bugcheck.ModelOutputs;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;
import se.uu.it.dtlsfuzzer.utils.DFAUtils;
import se.uu.it.dtlsfuzzer.utils.MealyUtils;

public class DtlsLanguageAdapterBuilder {
	
	public static <S> DtlsLanguageAdapter fromSystemModel(MealyMachine<S, TlsInput, ?, TlsOutput> mealy,
			Collection<TlsInput> inputs, DtlsLabelMapping mapping, boolean isClient) {
		List<TlsOutput> outputs = new LinkedList<>();
		MealyUtils.reachableOutputs(mealy, inputs, outputs);
		Mapping<TlsInput, MessageLabel> inputMapping = i -> mapping.labelForInput(i);
		
		
		Mapping<Pair<S, TlsOutput>, List<MessageLabel>> outputMapping = pair -> {
			return  mapping.labelsForOutput(pair.getSecond());
		};
		
		Mapping<Pair<S, TlsOutput>, List<MessageLabel>> clientOutputMapping = pair -> {
			List<MessageLabel> outputLabels;
			if (mealy.getInitialState().equals(pair.getFirst())) {
				if (!pair.getSecond().getRepeatedOutput().getAtomicAbstractOutputs().get(0).equals(ModelOutputs.getClientHelloString())) {
					throw new InternalError("Expected a client hello in the initial state. Got: " + pair.getSecond());
				}
				outputLabels = mapping.labelsForOutput(pair.getSecond());
				List<MessageLabel> outputLabelsWithoutCh = new ArrayList<>(outputLabels);
				outputLabelsWithoutCh.remove(0);
				outputLabels = outputLabelsWithoutCh;
			} else {
				 outputLabels = outputMapping.apply(pair);
			}
			return outputLabels;
		};
		
		List<MessageLabel> labelList = mapping.getIOLabels();
		
		SimpleAlphabet<MessageLabel> alphabet = new SimpleAlphabet<>(labelList);
		FastDFA<MessageLabel> dfa = new FastDFA<>(alphabet);
		
		DFAUtils.convertMealyToDFA(mealy, inputs, labelList, inputMapping, isClient ? clientOutputMapping : outputMapping, new LinkedHashMap<>(), dfa);
		
		List<FastDFAState> rejecting = dfa.getStates().stream().filter(s -> !s.isAccepting()).collect(Collectors.toList());
		if (rejecting.size() != 1) {
			throw new InternalError("The DFA generated from the learned model is expected to have only one rejecting state");
		}
		
		if (isClient) {
			FastDFAState initialState = dfa.getInitialState();
			dfa.setInitial(initialState, false);
			FastDFAState newInitialState = dfa.addInitialState(true);
			dfa.setTransition(newInitialState, new MessageLabel(false, ModelOutputs.getClientHelloString()), initialState);
			for (MessageLabel label : labelList) {
				if (!label.getMessageString().equals(ModelOutputs.getClientHelloString())) {
					dfa.setTransition(newInitialState, label, rejecting.get(0));
				} 
			}
		}
		
		return new DtlsLanguageAdapter(dfa, mapping).minimize();
	}
	
	public static DtlsLanguageAdapter fromSpecification(FastDFA<Label> dtlsSpecification, DtlsLabelMapping mapping) {
		List<MessageLabel> supportedLabels = mapping.getIOLabels();
		FastDFA<MessageLabel> unfoldedSpec = unfold(dtlsSpecification, mapping.getIOLabels());
		FastDFA<MessageLabel> inputCompleteSpec = new FastDFA<>(new SimpleAlphabet<MessageLabel>(supportedLabels));
		DFAs.complete(unfoldedSpec, inputCompleteSpec.getInputAlphabet(), inputCompleteSpec);
		return new DtlsLanguageAdapter(unfoldedSpec, mapping)
				.minimize();
	}
	
	public static FastDFA<MessageLabel> unfold(FastDFA<Label> dtlsSpecification, Collection<MessageLabel> supportedLabels) {
		SimpleAlphabet<MessageLabel> alphabet = new SimpleAlphabet<>(supportedLabels);
		FastDFA<MessageLabel> unfoldedSpecification = new FastDFA<>(alphabet);
		Mapping<FastDFAState,Collection<Label>> stateLabelMapping = s -> dtlsSpecification.getLocalInputs(s);
		FastDFAState sink = dtlsSpecification.addState(false);
		SpecificationTS<FastDFAState> dtlsLabelTS = new SpecificationTS<>(dtlsSpecification, sink, stateLabelMapping, supportedLabels);
		TSCopy.copy(TSTraversalMethod.DEPTH_FIRST, dtlsLabelTS, -1, supportedLabels, unfoldedSpecification);
		return unfoldedSpecification;
	}
}
