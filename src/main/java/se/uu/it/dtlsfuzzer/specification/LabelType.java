package se.uu.it.dtlsfuzzer.specification;

public enum LabelType {
	FILTER,
	MESSAGE,
	ENUMERATION,
	COMPREHENSION,
	BINARY_EXPRESSION,
	OTHER
}
