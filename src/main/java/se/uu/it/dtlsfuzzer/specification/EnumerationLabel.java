package se.uu.it.dtlsfuzzer.specification;

import java.util.Collection;
import java.util.LinkedHashSet;

public class EnumerationLabel extends Label {

	private LinkedHashSet<MessageLabel> messageLabels;

	public EnumerationLabel(Collection<MessageLabel> messageLabels) {
		this.messageLabels = new LinkedHashSet<>(messageLabels); 
	}
	
	public LinkedHashSet<MessageLabel> getMessageLabels() {
		return messageLabels;
	}
	
	public boolean containsLabel(MessageLabel label) {
		return messageLabels.contains(label);
	}

	@Override
	public LabelType getType() {
		return LabelType.ENUMERATION;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		messageLabels.forEach(m -> builder.append(m).append(", "));
		builder.delete(builder.length()-2, builder.length());
		return builder.append("}").toString();
	}

}
