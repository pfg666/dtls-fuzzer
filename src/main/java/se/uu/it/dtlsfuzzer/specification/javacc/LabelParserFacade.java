package se.uu.it.dtlsfuzzer.specification.javacc;

import se.uu.it.dtlsfuzzer.specification.Label;

public class LabelParserFacade {
	public static Label parseLabelString(String label) throws ParseException {
        java.io.StringReader sr = new java.io.StringReader(label);
        LabelParser parser = new LabelParser( sr );
        Label result = parser.label();
        return result;
	}
}
