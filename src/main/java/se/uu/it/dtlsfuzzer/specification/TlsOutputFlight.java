package se.uu.it.dtlsfuzzer.specification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class TlsOutputFlight extends ArrayList<String> {

	/**
	 */
	private static final long serialVersionUID = 1L;
	private int startIndex;

	TlsOutputFlight(int startIndex) {
		this(Collections.emptyList(), startIndex);
	}
	
	TlsOutputFlight(Collection<String> abstractOutputs,int startIndex) {
		super(abstractOutputs);
		this.startIndex = startIndex;
	}
	
	public int getStartIndex() {
		return startIndex;
	}
}
