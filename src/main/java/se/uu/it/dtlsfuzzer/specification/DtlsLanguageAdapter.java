package se.uu.it.dtlsfuzzer.specification;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.FastDFA;
import net.automatalib.automata.fsa.impl.FastDFAState;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.serialization.dot.GraphDOT;
import net.automatalib.util.automata.Automata;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.ts.acceptors.AcceptanceCombiner;
import net.automatalib.words.Word;
import net.automatalib.words.impl.SimpleAlphabet;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.utils.DFAUtils;


/** An adapter for DTLS models formulated as DFAs with input/output labels instead of Mealy machines. 
 */
public class DtlsLanguageAdapter {
	
	private FastDFA<MessageLabel> dfaModel;
	private boolean client;
	private DtlsLabelMapping mapping;
	
	DtlsLanguageAdapter(FastDFA<MessageLabel> dtlsSpec, DtlsLabelMapping mapping) {
		this.dfaModel = dtlsSpec;
		this.mapping = mapping;
	}
	
	/**
	 * Returns an adapter containing the complement of the underlying specification.
	 */
	public DtlsLanguageAdapter complement() {
		FastDFA<MessageLabel> complementModel = new FastDFA<>(dfaModel.getInputAlphabet());
		DFAs.complement(dfaModel, dfaModel.getInputAlphabet(), complementModel);
		return new DtlsLanguageAdapter(complementModel, mapping);
	}
	
	/**
	 * Returns an adapter containing the intersection between the underlying model and the model of another adapter.
	 * It is advisable that this model is reduced/minimized, since it may have many unreachable states.
	 */
	public DtlsLanguageAdapter intersect(DtlsLanguageAdapter adapter) {
		FastDFA<MessageLabel> intersectionModel = new FastDFA<>(dfaModel.getInputAlphabet());
		DFAs.combine(adapter.dfaModel, dfaModel, dfaModel.getInputAlphabet(), intersectionModel, AcceptanceCombiner.AND);
		return new DtlsLanguageAdapter(intersectionModel, mapping);
	}
	
	
	public DtlsLanguageAdapter union(DtlsLanguageAdapter adapter) {
		FastDFA<MessageLabel> unionModel = new FastDFA<>(dfaModel.getInputAlphabet());
		DFAs.combine(adapter.dfaModel, dfaModel, dfaModel.getInputAlphabet(), unionModel, AcceptanceCombiner.OR);
		return new DtlsLanguageAdapter(unionModel, mapping);
	}
	
	/**
	 * Returns an adapter containing a reduced copy of the underlying model in which unreachable states are removed.
	 */
	public DtlsLanguageAdapter reduce() {
		List<FastDFAState> reachableStates = new ArrayList<>();
		FastDFA<MessageLabel> reducedModel = new FastDFA<>(dfaModel.getInputAlphabet());
		AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, dfaModel, dfaModel.getInputAlphabet(), reducedModel);
		DFAUtils.reachableStates(reducedModel, dfaModel.getInputAlphabet(), reachableStates);
		List<FastDFAState> statesToRemove = new LinkedList<>(reducedModel.getStates());
		statesToRemove.removeAll(reachableStates);
		statesToRemove.forEach(s -> reducedModel.removeState(s));
		return new DtlsLanguageAdapter(reducedModel, mapping);
	}
	
	/**
	 * Returns an adapter containing a reduced copy in which the last transition corresponding to this flow is directed to a rejecting sink state. 
	 */
	public DtlsLanguageAdapter eliminateTransition(TlsFlow prefix, TlsInput input) {
		FastDFA<MessageLabel> reducedModel = new FastDFA<>(dfaModel.getInputAlphabet());
		AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, dfaModel, dfaModel.getInputAlphabet(), reducedModel);
		Word<MessageLabel> labels = mapping.toLabelWord(prefix);
		MessageLabel transitionInput = mapping.labelForInput(input);
		FastDFAState state = reducedModel.getState(labels);
		FastDFAState sink = reducedModel.addState(false);
		reducedModel.setTransition(state, transitionInput, sink);
		for (MessageLabel label : reducedModel.getInputAlphabet()) {
			reducedModel.setTransition(sink, label, sink);
		}
		return new DtlsLanguageAdapter(reducedModel, mapping);
	}
	
	
	/**
	 * Returns an adapter containing a reduced copy which excludes the given inputs from the alphabet.
	 */
	public DtlsLanguageAdapter eliminateInputs(Collection<MessageLabel> inputs) {
		List<MessageLabel> currentInputs = new ArrayList<>(dfaModel.getInputAlphabet());
		currentInputs.removeAll(inputs);
		SimpleAlphabet<MessageLabel> alphabet = new SimpleAlphabet<>(inputs);
		FastDFA<MessageLabel> reducedModel = new FastDFA<>(alphabet);
		AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, dfaModel, alphabet, reducedModel);
		return new DtlsLanguageAdapter(reducedModel, mapping);
	}
	
	public DtlsLanguageAdapter eliminateInputs(Collection<TlsInput> inputs, DtlsLabelMapping mapping) {
		List<MessageLabel> labels = inputs.stream().map(i -> mapping.labelForInput(i)).collect(Collectors.toList());
		return eliminateInputs(labels);
	}
	
	/**
	 * Minimizes using Hopcroft's minimization algorithm.
	 * Returns a copy containing the minimized model.
	 */
	public DtlsLanguageAdapter minimize() {
		CompactDFA<MessageLabel> result = DFAs.minimize(dfaModel);
		FastDFA<MessageLabel> minimizedCopy = new FastDFA<>(dfaModel.getInputAlphabet());
		AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, result, result.getInputAlphabet(), minimizedCopy);
		return new DtlsLanguageAdapter(minimizedCopy, mapping);
	}
	
	/**
	 * True if the specification is equivalent to the empty language.
	 */
	public boolean isEmpty() {
		return DFAs.acceptsEmptyLanguage(dfaModel);
	}
	
	/**
	 * Accepts the flow if the flow leads to an accepting state. 
	 */
	public boolean accepts(TlsFlow handshake) {
		Word<MessageLabel> labelWord = mapping.toLabelWord(handshake);
		FastDFAState state = dfaModel.getState(labelWord);
		boolean accepts = state != null && state.isAccepting();
		return accepts;
	}
	

	/**
	 * True if it accepts the flow or any prefix of it. 
	 */
	public boolean acceptsPrefix(TlsFlow handshake) {
		Word<MessageLabel> labelWord = mapping.toLabelWord(handshake);
		FastDFAState crtState = dfaModel.getInitialState();
		for (MessageLabel label : labelWord) {
			if (!dfaModel.getInputAlphabet().contains(label)) {
				throw new InternalError("Label " +  label + "is not contained in alphabet");
			}
			if (crtState.isAccepting()) {
				return true;
			}
			crtState = dfaModel.getTransition(crtState, label);
		}
		return crtState.isAccepting();
	}
	
	public boolean accepts(Collection<MessageLabel> labels) {
		return dfaModel.accepts(labels);
	}
	
	/**
	 * Accepts the flow if the flow leads to a rejecting state. 
	 */
	public boolean rejects(TlsFlow handshake) {
		Word<MessageLabel> labelWord = mapping.toLabelWord(handshake);
		FastDFAState state = dfaModel.getState(labelWord);
		boolean rejects = state != null && state.isAccepting();
		return rejects;
	}
	
	/**
	 * Accepts the flow if the flow is captured by the underlying model. 
	 */
	public boolean specifies(TlsFlow handshake) {
		Word<MessageLabel> labelWord = mapping.toLabelWord(handshake);
		FastDFAState state = dfaModel.getState(labelWord);
		boolean specifies = state != null;
		return specifies;
	}
	
	public DFA<?, MessageLabel> getDfaModel() {
		return dfaModel;
	}
	
	public List<MessageLabel> getAlphabet() {
		return new ArrayList<>(dfaModel.getInputAlphabet());
	}
	
	/**
	 * Returns the index of the first transition which deviates from the specification, which means, from that point forward it an acceptable state can no longer be reached.
	 * 
	 */
	public int getDeviantTransitionIndex(TlsFlow flow, DtlsLabelMapping mapping) {
		Word<MessageLabel> labels;
		for (int i=0; i < flow.getLength(); i++) {
			TlsFlow prefix = flow.prefix(i+1);
			labels = mapping.toLabelWord(prefix);
			if (!dfaModel.accepts(labels)) {
				return i;
			}
		}
		return -1;
	}
	
	public List<Word<TlsInput>> generateAllAcceptingInputWordsWithOneStateVisitBound(DtlsLabelMapping mapping) {
		Collection<Word<MessageLabel>> acceptingInputWords = new LinkedList<>();
		
		dfaModel.getStates().stream().filter(s -> s.isAccepting()).forEach(s ->
			DFAUtils.wordsToTargetState(dfaModel, dfaModel.getInputAlphabet(), s, acceptingInputWords)
		);
		
		return acceptingInputWords.stream().map(lw -> mapping.toTlsInputWord(lw)).collect(Collectors.toList());
	}
	
	public Word<TlsInput> generateShortestAcceptingInputWord(DtlsLabelMapping mapping) {
		FastDFA<MessageLabel> rejModel = new FastDFA<>(dfaModel.getInputAlphabet());
		rejModel.addInitialState(false);
		CompactDFA<MessageLabel> completeRejModel = DFAs.complete(rejModel, dfaModel.getInputAlphabet());
		Word<MessageLabel> word = Automata.findShortestSeparatingWord(dfaModel, completeRejModel, dfaModel.getInputAlphabet());
		if (word != null) {
			return mapping.toTlsInputWord(word);
		}
		return null;
	}
	
	public boolean isClient() {
		return client;
	}
	
	public void export(Writer writer) throws IOException {
		GraphDOT.write(dfaModel, dfaModel.getInputAlphabet(), writer);
	}
}
