package se.uu.it.dtlsfuzzer.specification;

public class FilterLabel extends Label{

	private String regexFilter;

	public FilterLabel(String regexFilter) {
		this.regexFilter = regexFilter;
	}
	
	public String getRegexFilter() {
		return regexFilter;
	}
	
	@Override
	public LabelType getType() {
		return LabelType.FILTER;
	}

	@Override
	public String toString() {
		return "F_" + regexFilter;
	}
}
