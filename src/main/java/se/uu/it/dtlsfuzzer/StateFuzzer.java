package se.uu.it.dtlsfuzzer;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alexmerz.graphviz.ParseException;

import net.automatalib.words.Alphabet;
import se.uu.it.dtlsfuzzer.config.StateFuzzerConfig;
import se.uu.it.dtlsfuzzer.learn.Extractor;
import se.uu.it.dtlsfuzzer.learn.Extractor.ExtractorResult;
import se.uu.it.dtlsfuzzer.sut.input.AlphabetFactory;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;

public class StateFuzzer {
	private static final Logger LOGGER = LogManager.getLogger(StateFuzzer.class);

	private StateFuzzerConfig config;
	private CleanupTasks cleanupTasks;

	public StateFuzzer(StateFuzzerConfig config) {
		this.config = config;
	}

	public void startFuzzing() throws ParseException, IOException {
		this.cleanupTasks = new CleanupTasks();
		try {
				// setting up our output directory
				File folder = new File(config.getOutput());
				folder.mkdirs();
				extractModel(config);
		} catch (Exception e) {
			cleanupTasks.execute();
			throw e;
		}
		cleanupTasks.execute();
	}
	
	private ExtractorResult extractModel(StateFuzzerConfig config) {
		Alphabet<TlsInput> alphabet = AlphabetFactory.buildAlphabet(config);
		Extractor extractor = new Extractor(config, alphabet, cleanupTasks);
		ExtractorResult result = extractor.extractStateMachine();
		return result;
	}
}
