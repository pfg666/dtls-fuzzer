package se.uu.it.dtlsfuzzer.mutate.record;

import java.util.LinkedList;
import java.util.List;

import de.rub.nds.tlsattacker.core.record.Record;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.AbstractMutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class RecordFlushMutation extends AbstractMutation{
	private boolean before;
	public RecordFlushMutation(boolean before) {
		this.before = before;
	}

	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		List<Record> records = new LinkedList<>();
		if (before) {
			records.addAll(exContext.getDeferredRecords()); 
			records.addAll(unit.getRecordsToSend());
		} else {
			records.addAll(unit.getRecordsToSend());
			records.addAll(exContext.getDeferredRecords()); 
		}
		unit.setRecordsToSend(records);
	}

	@Override
	public MutationType getType() {
		return MutationType.RECORD_FLUSH;
	}

}
