package se.uu.it.dtlsfuzzer.mutate;

import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;

public interface Mutation {

	void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext);
	
	default Phase [] getPhases() {
		return getType().getPhases();
	}

	MutationType getType();
	
}
