package se.uu.it.dtlsfuzzer.mutate;

public abstract class AbstractMutation implements Mutation {

	public String toString() {
		return this.getClass().getSimpleName() + JsonMutationParser.getInstance().serialize(this);
	}
}
