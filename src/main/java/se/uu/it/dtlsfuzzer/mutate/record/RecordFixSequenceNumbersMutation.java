package se.uu.it.dtlsfuzzer.mutate.record;

import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;

import de.rub.nds.modifiablevariable.biginteger.BigIntegerExplicitValueModification;
import de.rub.nds.modifiablevariable.biginteger.ModifiableBigInteger;
import de.rub.nds.tlsattacker.core.record.Record;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.AbstractMutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

/**
 * Fixes record sequence numbers in the records that are about to be sent and also updates the context so that the correct sequence number is used.
 */
public class RecordFixSequenceNumbersMutation extends AbstractMutation {
	private Integer from=0;
	
	public RecordFixSequenceNumbersMutation() {
	}
	
	public RecordFixSequenceNumbersMutation(Integer from) {
		this.from = from;
	}

	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		Map<Integer, BigInteger> epochSequenceMap = new LinkedHashMap<>();
		switch(phase) {
		case RECORD_PREPARATION:
			for (Record record : exContext.getSentRecords()) {
				Integer epoch = record.getEpoch().getValue();
				if (!epochSequenceMap.containsKey(epoch) || record.getSequenceNumber().getValue().compareTo(epochSequenceMap.get(epoch)) > 0) {
					epochSequenceMap.put(epoch, record.getSequenceNumber().getValue());
				}
			}
			int idx = 0;
			
			for (Record record : unit.getRecordsToSend()) {
				Integer sendEpoch = record.getEpoch().getValue();
				BigInteger correctRsn = epochSequenceMap.containsKey(sendEpoch) ? 
						epochSequenceMap.get(sendEpoch).add(BigInteger.ONE) : 
							BigInteger.ZERO;
				epochSequenceMap.put(sendEpoch, correctRsn);
				
				if (idx >= from) {
					if (record.getSequenceNumber() == null) {
						record.setSequenceNumber(new ModifiableBigInteger());
					}
					record.getSequenceNumber().setModification(new BigIntegerExplicitValueModification(correctRsn));
				}					
				idx ++;
			}
			BigInteger correctRsn = epochSequenceMap.containsKey(state.getTlsContext().getDtlsSendEpoch()) ? 
					epochSequenceMap.get(state.getTlsContext().getDtlsSendEpoch()).add(BigInteger.ONE) : 
						BigInteger.ZERO;
			state.getTlsContext().setWriteSequenceNumber(correctRsn.longValue());
			break;
		default:
			break;
		}
	}
	
	@Override
	public MutationType getType() {
		return MutationType.RECORD_FIX_SEQUENCE;
	}
	
}
