package se.uu.it.dtlsfuzzer.mutate;

import de.rub.nds.tlsattacker.core.state.TlsContext;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;

public interface Mutator {

	Mutation generateMutation(ProcessingUnit unit, TlsContext context,
			ExecutionContext exContext);
	
	MutationType getMutationType();
	
	
	
	/**
	 * Provides a compact and readable representation of the mutator.
	 */
	String toString();
}
