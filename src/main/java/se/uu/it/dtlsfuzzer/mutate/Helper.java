package se.uu.it.dtlsfuzzer.mutate;

import java.util.LinkedList;
import java.util.List;

public class Helper {
	public static <T> List<T> reorder(List<T> elements, Integer[] mapping, boolean preserveUnmapped) {
		List<T> orderedList = new LinkedList<T>();
		for (Integer i = 0; i < mapping.length; i++) {
			if (mapping[i] != null) {
				orderedList.add(elements.get(mapping[i]));
			}
		}
		if (preserveUnmapped) {
			for (Integer i=mapping.length; i < elements.size(); i++) {
				orderedList.add(elements.get(i));
			}
		}
		
		return orderedList;
	}
}
