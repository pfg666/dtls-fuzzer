package se.uu.it.dtlsfuzzer.mutate.record;

import java.util.ArrayList;
import java.util.List;

import de.rub.nds.tlsattacker.core.record.Record;
import de.rub.nds.tlsattacker.core.state.State;
import de.rub.nds.tlsattacker.core.state.TlsContext;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.AbstractMutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class RecordDuplicateMutation extends AbstractMutation {

	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		List<Record> records = new ArrayList<>(unit.getRecordsToSend().size() * 2);
		records.addAll(unit.getRecordsToSend());
		for (Record record : unit.getRecordsToSend()) {
			try {
				Record recordCopy = makeCopy(record, state.getTlsContext());
	            records.add((Record) recordCopy);
			} catch(Exception E) {
				throw new RuntimeException("Could not duplicate record", E);
			} 
		}
		unit.setRecordsToSend(records);
	}
	
	private Record makeCopy(Record record, TlsContext context) {
		Record recordCopy = new Record(context.getConfig());
		recordCopy.setContentType(record.getContentType().getValue());
		recordCopy.setProtocolVersion(record.getProtocolVersion().getValue());
		recordCopy.setEpoch(record.getEpoch().getValue());
		recordCopy.setSequenceNumber(record.getSequenceNumber().getValue());
		recordCopy.setLength(record.getLength());
		recordCopy.setProtocolMessageBytes(record.getProtocolMessageBytes());
		recordCopy.setCompleteRecordBytes(record.getCompleteRecordBytes().getValue());
		recordCopy.setCleanProtocolMessageBytes(record.getCleanProtocolMessageBytes().getValue());
		return recordCopy;
	}

	@Override
	public MutationType getType() {
		return MutationType.RECORD_DUPLICATE;
	}

}
