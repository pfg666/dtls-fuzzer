package se.uu.it.dtlsfuzzer.mutate.record;

import de.rub.nds.tlsattacker.core.state.State;
import de.rub.nds.tlsattacker.core.workflow.action.ActivateEncryptionAction;
import de.rub.nds.tlsattacker.core.workflow.action.DeactivateEncryptionAction;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.AbstractMutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class SendUnencryptedMutation extends AbstractMutation {
	
	public SendUnencryptedMutation() {
		
	}

	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		switch(phase) {
		case RECORD_GENERATION:
			if (state.getTlsContext().getDtlsSendEpoch() > 0) {
				DeactivateEncryptionAction action = new DeactivateEncryptionAction();
				action.setConnectionAlias(exContext.getSulDelegate().getRole());
				action.execute(state);
			}
			break;
		case RECORD_SENDING:
			if (state.getTlsContext().getDtlsSendEpoch() > 0) {
				long readSequenceNumber = state.getTlsContext().getReadSequenceNumber();
				long writeSequenceNumber = state.getTlsContext().getWriteSequenceNumber();
				ActivateEncryptionAction action = new ActivateEncryptionAction();
				action.setConnectionAlias(exContext.getSulDelegate().getRole());
				action.execute(state);
				state.getTlsContext().setReadSequenceNumber(readSequenceNumber);
				state.getTlsContext().setWriteSequenceNumber(writeSequenceNumber);
			}
			break;
			default :
				break;
		}
	}

	
	@Override
	public MutationType getType() {
		return MutationType.INPUT_SEND_UNENCRYPTED;
	}
}
