package se.uu.it.dtlsfuzzer.mutate.context;

import java.util.List;

import de.rub.nds.tlsattacker.core.crypto.MessageDigestCollector;
import de.rub.nds.tlsattacker.core.record.AbstractRecord;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.Mutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class ContextFixMessageDigest implements Mutation{
	
	private int [] includedRecords;

	public ContextFixMessageDigest(int includedRecords []) {
		this.includedRecords = includedRecords;
	}

	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		List<AbstractRecord> records = exContext.getAllRecords();
		MessageDigestCollector digest = state.getTlsContext().getDigest();
		digest.reset();
		for (int i = 0; i < includedRecords.length; i++) {
			int recIndex = includedRecords[i];
			digest.append(records.get(recIndex).getCleanProtocolMessageBytes().getValue());
		}
	}

	@Override
	public MutationType getType() {
		return MutationType.CONTEXT_FIX_DIGEST;
	}
}
