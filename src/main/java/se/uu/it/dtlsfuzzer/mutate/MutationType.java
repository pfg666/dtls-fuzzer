package se.uu.it.dtlsfuzzer.mutate;

import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.mutate.record.*;
import se.uu.it.dtlsfuzzer.mutate.message.*;
import se.uu.it.dtlsfuzzer.mutate.context.*;

/**
 * Each mutation type is associated with exactly one implementing class and the phases after which the mutation applies
 */
public enum MutationType {
	CONTEXT_DONT_UPDATE(ContextDontUpdate.class, Phase.MESSAGE_GENERATION),
	CONTEXT_FIX_DIGEST(ContextFixMessageDigest.class, Phase.MESSAGE_GENERATION),
	SET_MESSAGE_SEQUENCE(SetMessageSequenceMutation.class, Phase.MESSAGE_GENERATION),
	RECORD_CHANGE_EPOCH(SetEpochMutation.class, Phase.RECORD_PREPARATION),
	RECORD_FIX_SEQUENCE(RecordFixSequenceNumbersMutation.class, Phase.RECORD_PREPARATION),
	RECORD_REORDER(RecordReorderMutation.class, Phase.RECORD_PREPARATION),
	RECORD_DEFER(RecordDeferMutation.class, Phase.RECORD_PREPARATION),
	RECORD_FLUSH(RecordFlushMutation.class, Phase.RECORD_PREPARATION),
	RECORD_DUPLICATE(RecordDuplicateMutation.class, Phase.RECORD_PREPARATION),
	INPUT_SEND_UNENCRYPTED(SendUnencryptedMutation.class, Phase.RECORD_GENERATION, Phase.RECORD_SENDING),
	;

	private Class<? extends Mutation> mutationClass;
	private Phase [] phases;

	private MutationType(Class<? extends Mutation> mutationClass, Phase ...phases) {
		this.mutationClass = mutationClass;
		this.phases = phases;
		
	}
	
	public Class<? extends Mutation> getMutationClass() {
		return mutationClass;
	}
	
	public Phase [] getPhases() {
		return phases;
	}
}
