 package se.uu.it.dtlsfuzzer.mutate.record;

import de.rub.nds.tlsattacker.core.record.Record;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.AbstractMutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class SetEpochMutation extends AbstractMutation {
	
	private int [] epochs = new int [] {};
	
	public SetEpochMutation(int ... usedEpochs) {
		this.epochs  = usedEpochs;
	}

	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		int idx =0;
		for (Record record : unit.getRecordsToSend()) {
			if (idx < epochs.length) {
				record.setEpoch(epochs[idx]);
			} else {
				break;
			}
			idx ++;
		}
	}

	public MutationType getType() {
		return MutationType.RECORD_CHANGE_EPOCH;
	}

}
