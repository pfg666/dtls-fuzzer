package se.uu.it.dtlsfuzzer.mutate;

import java.util.Arrays;
import java.util.List;

import se.uu.it.dtlsfuzzer.config.MapperConfig;
import se.uu.it.dtlsfuzzer.config.SulDelegate;
import se.uu.it.dtlsfuzzer.execute.InputExecutor;
import se.uu.it.dtlsfuzzer.execute.MutatedInputExecutor;
import se.uu.it.dtlsfuzzer.sut.input.DtlsInput;
import se.uu.it.dtlsfuzzer.sut.input.DtlsInputWrapper;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;

public class MutatedDtlsInput extends DtlsInputWrapper {
	
	private List<Mutation> mutations;
	
	public MutatedDtlsInput(TlsInput input, Mutation ...mutations) {
		this(input, Arrays.asList(mutations));
	}

	public MutatedDtlsInput(TlsInput input, List<Mutation> mutations) {
		super((DtlsInput)input);
		this.mutations = mutations;
		this.setName("@"
				+ input.getName()
				+ "_["
				+ getCompactMutationDescription() + "]");
	}
	
	public InputExecutor getPreferredExecutor(SulDelegate config, MapperConfig mapperConfig) {
		return new MutatedInputExecutor(mapperConfig, mutations);
	}
	
	/**
	 * Mutated inputs allow changing of the name to improve readability. 
	 */
	public void setName(String name) {
		super.setName(name);
	}

	public List<Mutation> getMutations() {
		return mutations;
	}
	
	public String getCompactMutationDescription() {
		return mutations.stream()
				.map(m -> m.toString())
				.reduce((s1, s2) -> s1 + "_" + s2)
				.orElse("");
	}
}
