package se.uu.it.dtlsfuzzer.mutate.context;

import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.Mutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class ContextDontUpdate implements Mutation {

	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		unit.getMessages().forEach(m -> m.setAdjustContext(false));
	}

	@Override
	public MutationType getType() {
		return MutationType.CONTEXT_DONT_UPDATE;
	}
	
	public String toString() {
		return "ContextDontUpdate";
	}
}
