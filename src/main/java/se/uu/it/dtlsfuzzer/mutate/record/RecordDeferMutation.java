package se.uu.it.dtlsfuzzer.mutate.record;

import java.util.Collections;

import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.AbstractMutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class RecordDeferMutation extends AbstractMutation{

	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		exContext.addDeferredRecords(unit.getRecordsToSend());
		unit.setRecordsToSend(Collections.emptyList());
	}

	@Override
	public MutationType getType() {
		return MutationType.RECORD_DEFER;
	}

}
