package se.uu.it.dtlsfuzzer.mutate.message;

import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.Mutation;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class SetMessageSequenceMutation implements Mutation {
	private int mseq = -1;
	
	public SetMessageSequenceMutation() {
	}
	
	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		if (phase.equals(Phase.MESSAGE_GENERATION)) {
			state.getTlsContext().setDtlsNextSendSequenceNumber(mseq);
		}
	}

	@Override
	public MutationType getType() {
		return MutationType.SET_MESSAGE_SEQUENCE;
	}

}
