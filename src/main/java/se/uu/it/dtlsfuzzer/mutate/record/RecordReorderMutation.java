package se.uu.it.dtlsfuzzer.mutate.record;

import java.util.List;

import de.rub.nds.tlsattacker.core.record.Record;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.Phase;
import se.uu.it.dtlsfuzzer.execute.ProcessingUnit;
import se.uu.it.dtlsfuzzer.mutate.AbstractMutation;
import se.uu.it.dtlsfuzzer.mutate.Helper;
import se.uu.it.dtlsfuzzer.mutate.MutationType;

public class RecordReorderMutation extends AbstractMutation{
	
	private Integer [] mapping;

	public RecordReorderMutation(Integer mapping []) {
		this.mapping = mapping;
	}

	@Override
	public void mutate(Phase phase, ProcessingUnit unit, State state, ExecutionContext exContext) {
		List<Record> records = unit.getRecordsToSend();
		List<Record> newRecords = Helper.<Record>reorder(records, mapping, true);
		unit.setRecordsToSend(newRecords);
	}

	@Override
	public MutationType getType() {
		return MutationType.RECORD_REORDER;
	}

}
