package se.uu.it.dtlsfuzzer.mutate;

import java.util.Arrays;
import java.util.List;

import se.uu.it.dtlsfuzzer.config.MapperConfig;
import se.uu.it.dtlsfuzzer.config.SulDelegate;
import se.uu.it.dtlsfuzzer.execute.InputExecutor;
import se.uu.it.dtlsfuzzer.execute.MutatingInputExecutor;
import se.uu.it.dtlsfuzzer.sut.input.DtlsInput;
import se.uu.it.dtlsfuzzer.sut.input.DtlsInputWrapper;

public class MutatingDtlsInput extends DtlsInputWrapper {

	private List<Mutator> mutators;
	
	public MutatingDtlsInput(DtlsInput input, Mutator ... mutators) {
		this(input, Arrays.asList(mutators));
	}

	public MutatingDtlsInput(DtlsInput input, List<Mutator> mutators) {
		super(input);
		this.mutators = mutators;
		super.setName("MUTATING_" + input.toString() + "_" + getCompactMutatorDescription());
	}
	
	public InputExecutor getPreferredExecutor(SulDelegate config, MapperConfig mapperConfig) {
		return new MutatingInputExecutor(mapperConfig, mutators);
	}
	
	private String getCompactMutatorDescription() {
		return mutators.stream()
				.map(m -> m.toString())
				.reduce((s1, s2) -> s1 + "_" + s2)
				.orElse("");
	}
}
