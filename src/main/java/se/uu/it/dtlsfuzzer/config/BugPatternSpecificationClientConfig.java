package se.uu.it.dtlsfuzzer.config;

public class BugPatternSpecificationClientConfig extends BugPatternSpecificationConfig{

	@Override
	public boolean isClient() {
		return true;
	}

}
