package se.uu.it.dtlsfuzzer.config;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;

@Parameters(commandDescription = "Performs automatic bug-checking on a DTLS server using a learned model")
public class BugCheckerServerConfig extends BugCheckerConfig {
	@ParametersDelegate
	private SulClientDelegate sulClientDelegate;
	
	@ParametersDelegate
	private BugPatternSpecificationServerConfig specificationConfig;
	
	public BugCheckerServerConfig() {
		super();
		sulClientDelegate = new SulClientDelegate();
		specificationConfig = new BugPatternSpecificationServerConfig();
	}

	@Override
	public SulDelegate getSulDelegate() {
		return sulClientDelegate;
	}

	@Override
	public boolean isClient() {
		return false;
	}

	@Override
	public BugPatternSpecificationConfig getSpecificationConfig() {
		return specificationConfig;
	}
}
