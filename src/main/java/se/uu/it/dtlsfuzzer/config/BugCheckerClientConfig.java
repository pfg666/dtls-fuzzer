package se.uu.it.dtlsfuzzer.config;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;

@Parameters(commandDescription = "Performs automatic bug-checking on a DTLS client using a learned model")
public class BugCheckerClientConfig extends BugCheckerConfig{

	@ParametersDelegate
	private SulServerDelegate sulServerDelegate;
	
	@ParametersDelegate
	private BugPatternSpecificationClientConfig specificationConfig;
	
	
	public BugCheckerClientConfig() {
		super();
		sulServerDelegate = new SulServerDelegate();
		specificationConfig = new BugPatternSpecificationClientConfig();
	}

	@Override
	public SulDelegate getSulDelegate() {
		return sulServerDelegate;
	}

	@Override
	public boolean isClient() {
		return true;
	}

	@Override
	public BugPatternSpecificationConfig getSpecificationConfig() {
		return specificationConfig;
	}
}
