package se.uu.it.dtlsfuzzer.config;

import com.beust.jcommander.Parameter;

public abstract class BugPatternSpecificationConfig {
	
	@Parameter(names = "-patterns", required = false, description = "Use external patterns.xml file at location")
	private String patterns = null;
	
	public String getPatterns() {
		return patterns;
	}

	public abstract boolean isClient();
}
