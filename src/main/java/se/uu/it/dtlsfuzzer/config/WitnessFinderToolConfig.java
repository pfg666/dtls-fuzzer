package se.uu.it.dtlsfuzzer.config;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;

import se.uu.it.dtlsfuzzer.witness.SearchOrder;

public abstract class WitnessFinderToolConfig implements TestRunnerEnabler, RoleProvider, SulDelegateProvider {
	
	@ParametersDelegate
	private MapperConfig mapperConfig;
	
	@ParametersDelegate
	private TestRunnerConfig testRunnerConfig;
	
	@Parameter(names = "-alphabet", required=false, description = "The alphabet defining the inputs of the input labels appearing in the automaton.")
	private String alphabet;
	
	@Parameter(names = "-generationStrategy", required=false, description = "Strategy for generating witnesses")
	private GenerationStrategy witnessGenerationStrategy;
	
	@Parameter(names = "-searchStrategy", required=false, description = "For BFS witnesses, ")
	private SearchOrder searchStrategy;
	
	public WitnessFinderToolConfig() {
		mapperConfig = new MapperConfig();
		testRunnerConfig = new TestRunnerConfig();
	}
	
	@Override
	public MapperConfig getMapperConfig() {
		return mapperConfig;
	}

	@Override
	public String getAlphabet() {
		return alphabet;
	}

	@Override
	public TestRunnerConfig getTestRunnerConfig() {
		return testRunnerConfig;
	}
}
