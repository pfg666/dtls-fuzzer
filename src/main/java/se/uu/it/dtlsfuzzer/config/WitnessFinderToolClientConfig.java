package se.uu.it.dtlsfuzzer.config;

public class WitnessFinderToolClientConfig extends WitnessFinderToolConfig {
	
	private SulClientDelegate sulDelegate;

	public WitnessFinderToolClientConfig() {
		sulDelegate = new SulClientDelegate();
		
	}
	

	@Override
	public SulDelegate getSulDelegate() {
		return sulDelegate;
	}

	@Override
	public boolean isClient() {
		return true;
	}

}
