package se.uu.it.dtlsfuzzer.config;

public enum GenerationStrategy {
	SHORTEST,
	DEVIANT,
	BFS
}
