package se.uu.it.dtlsfuzzer.config;

public class WitnessFinderToolServerConfig extends WitnessFinderToolConfig {
	private SulDelegate delegate;
	
	public WitnessFinderToolServerConfig() {
		super();
		delegate = new SulServerDelegate();
	}

	@Override
	public SulDelegate getSulDelegate() {
		return delegate;
	}

	@Override
	public boolean isClient() {
		// TODO Auto-generated method stub
		return false;
	}
}
