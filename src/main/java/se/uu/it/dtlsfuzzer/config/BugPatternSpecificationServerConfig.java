package se.uu.it.dtlsfuzzer.config;

import com.beust.jcommander.Parameter;

import se.uu.it.dtlsfuzzer.ClientCertAuth;

public class BugPatternSpecificationServerConfig extends BugPatternSpecificationConfig {

	@Parameter(names = "-clientCertAuth", required=false, description = "The client certificate authentication setting used (applicable only for servers).")
	private ClientCertAuth clientCertAuth = ClientCertAuth.REQUIRED;
	
	public ClientCertAuth getClientCertAuth() {
		return clientCertAuth;
	}

	public void setClientCertAuth(ClientCertAuth clientCertAuth) {
		this.clientCertAuth = clientCertAuth;
	}
	
	@Override
	public boolean isClient() {
		return false;
	}

}
