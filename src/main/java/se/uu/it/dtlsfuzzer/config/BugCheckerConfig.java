package se.uu.it.dtlsfuzzer.config;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;

public abstract class BugCheckerConfig extends ToolConfig implements TestRunnerEnabler, RoleProvider, SulDelegateProvider,  MapperConfigProvider, AlphabetOptionProvider  {
	@ParametersDelegate
	private MapperConfig mapperConfig;

	@ParametersDelegate
	private TestRunnerConfig testRunnerConfig;
	
	@Parameter(names = "-model", required=false, description = "The Mealy machine describing the system's behavior. "
			+ "If not provided, it will be generated using state fuzzing.")
	private String model;
	
	@Parameter(names = "-alphabet", required=true, description = "The alphabet defining the inputs which appear in the Mealy machine.")
	private String alphabet;
	
	@Parameter(names = "-output", required=false, description = "The directory where to store the report plus and bug test cases.")
	private String output = "output";
	
	@Parameter(names = "-logTests", required=false, description = "Create a log of all the tests that are executed.")
	private boolean logTests;
	
	@Parameter(names = "-verifyBugs", required=false, description = "For each bug found verify that it is an actual bug of the SUT, and not an artifact of the model."
			+ "Verification involves finding a witness in the bug model that verifies also on the system.")
	private boolean verifyBugs = false;
	
	@ParametersDelegate
	private WitnessFinderConfig witnessFinderConfig;
	
	@Parameter(names = "-uncategorizedBugBound", required=false, description = "The maximum number of uncategorized bugs to display.")
	private int uncategorizedBugBound = 10;
	

	@ParametersDelegate 
	private LearningConfig learningConfig;
	
	public BugCheckerConfig() {
		mapperConfig = new MapperConfig();
		testRunnerConfig = new TestRunnerConfig();
		witnessFinderConfig = new WitnessFinderConfig();
		learningConfig = new LearningConfig();
	}

	public MapperConfig getMapperConfig() {
		return mapperConfig;
	}
	
	public LearningConfig getLearningConfig() {
		return learningConfig;
	}

	public String getAlphabet() {
		return alphabet;
	}
	
	public String getOutput() {
		return output;
	}
	
	public boolean isLogTests() {
		return logTests;
	}
	
	public boolean isVerifyBugs() {
		return verifyBugs;
	}
	
	public TestRunnerConfig getTestRunnerConfig() {
		return testRunnerConfig;
	}
	
	public String getModel() {
		return model;
	}
	
	public int getUncategorizedBugBound() {
		return uncategorizedBugBound;
	}
	
	public abstract BugPatternSpecificationConfig getSpecificationConfig();
	
	public WitnessFinderConfig getWitnessFinderConfig() {
		return witnessFinderConfig;
	}
}
