package se.uu.it.dtlsfuzzer.config;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;

import se.uu.it.dtlsfuzzer.witness.SearchConfig;

public class WitnessFinderConfig {
	@Parameter(names = "-generationStrategy", required=false, description = "Strategy for generating witnesses from the language.")
	private GenerationStrategy witnessGenerationStrategy = GenerationStrategy.SHORTEST;
	
	@ParametersDelegate
	private SearchConfig searchConfig;
	
	@Parameter(names = "-bound", required=false, description = "Upper bound placed on the number of witnesses generated before the search ceases.")
	private int bound = 100;

	public WitnessFinderConfig() {
		searchConfig = new SearchConfig();	
	}
	
	public GenerationStrategy getWitnessGenerationStrategy() {
		return witnessGenerationStrategy;
	}

	public SearchConfig getSearchConfig() {
		return searchConfig;
	}

	public int getBound() {
		return bound;
	}
}
