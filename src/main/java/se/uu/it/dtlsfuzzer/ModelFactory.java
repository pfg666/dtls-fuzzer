package se.uu.it.dtlsfuzzer;

import java.io.FileNotFoundException;

import com.alexmerz.graphviz.ParseException;
import com.pfg666.dotparser.fsm.mealy.MealyDotParser;

import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.words.Alphabet;
import se.uu.it.dtlsfuzzer.sut.input.NameTlsInputMapping;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

public class ModelFactory {
	public static FastMealy<TlsInput, TlsOutput> buildTlsModel(Alphabet<TlsInput> alphabet, String modelPath) throws FileNotFoundException, ParseException {
		NameTlsInputMapping definitions = new NameTlsInputMapping(alphabet);
		MealyDotParser<TlsInput, TlsOutput> dotParser = new MealyDotParser<>(
				new TlsProcessor(definitions));
		FastMealy<TlsInput, TlsOutput> machine = dotParser
				.parseAutomaton(modelPath).get(0);
		return machine;
	}
}
