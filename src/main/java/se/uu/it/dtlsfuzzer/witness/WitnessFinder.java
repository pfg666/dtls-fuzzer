package se.uu.it.dtlsfuzzer.witness;

import de.learnlib.api.oracle.MembershipOracle;
import net.automatalib.words.Word;
import se.uu.it.dtlsfuzzer.specification.DtlsLabelMapping;
import se.uu.it.dtlsfuzzer.specification.DtlsLanguageAdapter;
import se.uu.it.dtlsfuzzer.specification.MessageLabel;
import se.uu.it.dtlsfuzzer.specification.TlsFlow;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

public class WitnessFinder {
	
	private MembershipOracle<TlsInput, Word<TlsOutput>> sutOracle;
	private DtlsLabelMapping mapping;
	private int bound;
	private SequenceGenerator<MessageLabel> generator;
	
	public WitnessFinder(MembershipOracle<TlsInput, Word<TlsOutput>> sutOracle, DtlsLabelMapping mapping,
			 SequenceGenerator<MessageLabel> generator) {
		this.sutOracle = sutOracle;
		this.mapping = mapping;
		this.generator = generator;
		this.bound = -1;
	}

	public WitnessFinder(MembershipOracle<TlsInput, Word<TlsOutput>> sutOracle, DtlsLabelMapping mapping,
			 SequenceGenerator<MessageLabel> generator, int bound) {
		this.sutOracle = sutOracle;
		this.mapping = mapping;
		this.generator = generator;
		this.bound = bound;
	}
	
	public TlsFlow findWitness(DtlsLanguageAdapter sutBugLanguage) {
		return findWitness(sutBugLanguage, true);
	}
	
	public TlsFlow findCounterexample(DtlsLanguageAdapter sutBugLanguage) {
		return findWitness(sutBugLanguage, false);
	}
	
	private TlsFlow findWitness(DtlsLanguageAdapter sutBugLanguage, boolean verificationOutcome) {
		int count = 0;
		for (Word<MessageLabel> sequence : generator.generateSequences(sutBugLanguage.getDfaModel(), sutBugLanguage.getAlphabet())) {
			Word<TlsInput> inputWord = mapping.toTlsInputWord(sequence);
			Word<TlsOutput> outputWord = sutOracle.answerQuery(inputWord);
			TlsFlow tlsFlow = new TlsFlow(inputWord, outputWord);
			boolean exhibitsBug = sutBugLanguage.acceptsPrefix(tlsFlow);
			if ( (verificationOutcome && exhibitsBug) || (!verificationOutcome && !exhibitsBug) ) {
				return tlsFlow;
			}
			count ++;
			if (count >= bound && bound != -1) {
				break;
			}
		}
		return null;
	}
}
