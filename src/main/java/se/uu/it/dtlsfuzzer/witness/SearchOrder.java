package se.uu.it.dtlsfuzzer.witness;

public enum SearchOrder {
	DEFAULT, // don't intervene in the exploration
	MIN_VISIT, // prioritize paths which visit states the least number of times
	MIN_STATE_MIN_VISIT,// prioritize paths which visit states the least number of times 
	MIN_STATE, // prioritize paths which visit the fewest number of distinct states
	MAX_STATE, // prioritize paths which visit the largest number of distinct states
	MIN_VISIT_MIN_STATE
}
