package se.uu.it.dtlsfuzzer.witness;

import java.util.Collection;

import net.automatalib.automata.fsa.DFA;
import net.automatalib.words.Word;

public interface SequenceGenerator<I> {
	Iterable<Word<I>> generateSequences(DFA<?, I> bugLanguage, Collection<I> alphabet);
	
}
