package se.uu.it.dtlsfuzzer.witness;

import com.beust.jcommander.Parameter;

public class SearchConfig {
	@Parameter(names = "-stateVisitBound", required=false, description = "Bound on the maximum number of times a state can be visited")
	private int stateVisitBound = 1;
	@Parameter(names = "-order", required=false, description = "Tune the BFS search by imposing an ordering on the queue storing the next states to visit."
			+ "By default, elements are removed from the queue in the order in which they were inserted.")
	private SearchOrder order = SearchOrder.MIN_VISIT_MIN_STATE;
	
	public void setStateVisitBound(int stateVisitBound) {
		this.stateVisitBound = stateVisitBound;
	}
	
	public int getStateVisitBound() {
		return stateVisitBound;
	}
	
	public SearchOrder getOrder() {
		return order;
	}
}
