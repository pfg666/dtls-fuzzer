package se.uu.it.dtlsfuzzer.witness;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import net.automatalib.automata.UniversalDeterministicAutomaton;
import net.automatalib.words.Word;
import se.uu.it.dtlsfuzzer.utils.AutomatonUtils;
import se.uu.it.dtlsfuzzer.utils.AutomatonUtils.PredMap;
import se.uu.it.dtlsfuzzer.utils.AutomatonUtils.PredStruct;

/**
 * Generates iterables for exploring ways to get from the initial state to target states.
 */
public class ModelExplorer<S, I> {
	private static final int MAX_TOVISIT = 100000;
	private PredMap<S, I> predMap;
	private UniversalDeterministicAutomaton<S, I, ?, ?, ?>  model;

	public ModelExplorer(UniversalDeterministicAutomaton<S, I, ?, ?, ?> model, Collection<I> inputs) {
		this.model = model;
		this.predMap = AutomatonUtils.generatePredecessorMap(model, inputs);
	}

	public Iterable<Word<I>> wordsToTargetStates(Collection<S> targetStates, SearchConfig options) {
		return new Iterable<Word<I>>() {
			@Override
			public Iterator<Word<I>> iterator() {
				return new BFSPathToStateIterator(targetStates, options.getStateVisitBound(), options.getOrder());
			}
		};
	}
	
	
	private class BFSPathToStateIterator implements Iterator<Word<I>> {
		private Queue<SearchState> toVisit;
		private Word<I> nextWord;
		private SearchState searchState;
		private Iterator<PredStruct<S, I>> visitingIter;
		private int stateVisitBound;
		private final AtomicLong idGenerator = new AtomicLong();

		private BFSPathToStateIterator(Collection<S> targetStates, int stateVisitBound, SearchOrder strategy) {
			switch(strategy) {
			case MIN_VISIT:
				toVisit = new PriorityQueue<SearchState>(new Comparator<SearchState>(){
					@Override
					public int compare(SearchState s1, SearchState s2) {
						// we prioritize states based on the max visited value and on the order in which they were created.  
						int compMax = Integer.compare(s1.maxVisited(), s2.maxVisited());
						if (compMax == 0) {
							return Long.compare(s1.getId(), s2.getId());
						}
						return compMax;
					}
				});
				break;
			case MIN_STATE:
				toVisit = new PriorityQueue<SearchState>(new Comparator<SearchState>(){
					@Override
					public int compare(SearchState s1, SearchState s2) {
						// we prioritize states based on the max visited value and on the order in which they were created.  
						int compStates = Integer.compare(s1.distinctStatesVisited(), s2.distinctStatesVisited());
						if (compStates == 0) {
							return Long.compare(s1.getId(), s2.getId());
						}
						return compStates;
					}
				});
				break;
			case MIN_STATE_MIN_VISIT:
				toVisit = new PriorityQueue<SearchState>(new Comparator<SearchState>(){
					@Override
					public int compare(SearchState s1, SearchState s2) {
						// we prioritize states based on the max visited value and on the order in which they were created.  
						int compStates = Integer.compare(s1.distinctStatesVisited(), s2.distinctStatesVisited());
						if (compStates == 0) {
							compStates = Integer.compare(s1.maxVisited(), s2.maxVisited());
							if (compStates == 0) {
								return Long.compare(s1.getId(), s2.getId());
							}
						}
						return compStates;
					}
				});
				break;	
			
			case MIN_VISIT_MIN_STATE:
				toVisit = new PriorityQueue<SearchState>(new Comparator<SearchState>(){
					@Override
					public int compare(SearchState s1, SearchState s2) {
						// we prioritize states based on the max visited value and on the order in which they were created.  
						int compStates = Integer.compare(s1.maxVisited(), s2.maxVisited());
						if (compStates == 0) {
							compStates = Integer.compare(s1.distinctStatesVisited(), s2.distinctStatesVisited());
							if (compStates == 0) {
								return Long.compare(s1.getId(), s2.getId());
							}
						}
						return compStates;
					}
				});
				break;
				
			case MAX_STATE:
				toVisit = new PriorityQueue<SearchState>(new Comparator<SearchState>(){
					@Override
					public int compare(SearchState s1, SearchState s2) {
						// we prioritize states based on the max visited value and on the order in which they were created.  
						int compStates = Integer.compare(s2.distinctStatesVisited(), s1.distinctStatesVisited());
						if (compStates == 0) {
							return Long.compare(s1.getId(), s2.getId());
						}
						return compStates;
					}
				});
				break;
			default:
				toVisit = new ArrayDeque<>();
				break;
			}
			
			for (S targetState : targetStates) {
				toVisit.add(new SearchState(targetState));
			}
			visitingIter = Collections.emptyListIterator();
			this.stateVisitBound = stateVisitBound;
		}

		@Override
		public boolean hasNext() {
			return computeNextWord() != null;
		}

		@Override
		public Word<I> next() {
			if (hasNext()) {
				Word<I> next = nextWord;
				nextWord = null;
				return next;
			}
			throw new NoSuchElementException();
		}

		/*
		 * We need use iterators to compensate for the lack of "yield" which would make life a lot easier.
		 */
		private Word<I> computeNextWord() {
			if (nextWord != null) {
				return nextWord;
			} else {
				while (!toVisit.isEmpty() || visitingIter.hasNext()) {
					
					while (!visitingIter.hasNext() && !toVisit.isEmpty()) {
						searchState = toVisit.poll();
						
						if (predMap.containsKey(searchState.getState())) {
							visitingIter = predMap.get(searchState.getState()).iterator();
						}
						
						if (model.getInitialState().equals(searchState.getState())) {
							nextWord = searchState.getSuffix();
							return nextWord;
						}
					}

					while (visitingIter.hasNext()) {
						PredStruct<S, I> predStruct = visitingIter.next(); 
						SearchState potentialState = new SearchState(predStruct.getState(), predStruct.getInput(), searchState);
						
						if (potentialState.maxVisited() <= stateVisitBound ) {
							if (toVisit.size() < MAX_TOVISIT) {
								toVisit.add(potentialState);
							}
							else {
							}
						}
					}
				}
			}

			return nextWord;
		}
		

		private class SearchState {
			
			
			private I input;
			private S state;
			private long id;
			private SearchState parent;
			private int distinctVisited = -1;
			private int maxVisited = -1;

			public SearchState(S endState) {
				state = endState;
				id = idGenerator.incrementAndGet();
			}

			public SearchState(S state, I input, SearchState parent) {
				super();
				this.id = idGenerator.incrementAndGet();
				this.state = state;
				this.input = input;
				this.parent = parent;
			}

			public Word<I> getSuffix() {
				if (parent == null) {
					return Word.epsilon();
				} else {
					return parent.getSuffix().prepend(input);
				}
			}

			public S getState() {
				return state;
			}
			
			public long getId() {
				return id;
			}
			
			public SearchState getParent() {
				return parent;
			}
			
			public int distinctStatesVisited() {
				if (distinctVisited == -1) {
					Set<S> visited = new HashSet<>();
					SearchState crtExplState = this;
					while (crtExplState != null) {
						S crtState = crtExplState.getState();
						visited.add(crtState);
						crtExplState = crtExplState.getParent();
					}
					distinctVisited = visited.size();
				}
				
				return distinctVisited;
			}
			
			public int maxVisited() {
				if (maxVisited == -1) {
					SearchState crtExplState = this;
					Map<S,Integer> timesVisited = new HashMap<>();
					while (crtExplState != null) {
						S crtState = crtExplState.getState();
						if (timesVisited.containsKey(crtState)) {
							timesVisited.put(crtState, timesVisited.get(crtState) + 1);
						} else {
							timesVisited.put(crtState, 1);
						}
						crtExplState = crtExplState.getParent();
					}
					maxVisited = timesVisited.values().stream().max(Comparator.naturalOrder()).orElse(0);
				} 
				return maxVisited;
			}
		}
	}
}
