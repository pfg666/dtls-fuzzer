package se.uu.it.dtlsfuzzer.sut.input;

import javax.xml.bind.annotation.XmlAttribute;

public abstract class AbstractInput {
	/**
	 * The name (abstraction) by which the input can be referred
	 */
	@XmlAttribute(name = "name", required = true)
	private String name = null;

	protected AbstractInput() {
	}

	protected AbstractInput(String name) {
		this.name = name;
	}
	public String toString() {
		return name;
	}
	
	protected void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
