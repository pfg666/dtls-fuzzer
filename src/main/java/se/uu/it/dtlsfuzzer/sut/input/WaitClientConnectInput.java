package se.uu.it.dtlsfuzzer.sut.input;

import java.util.Collections;
import java.util.List;

import de.rub.nds.tlsattacker.core.protocol.message.ProtocolMessage;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;

public class WaitClientConnectInput extends TlsInput {
	
	@Override
	public List<ProtocolMessage> generateMessages(State state, ExecutionContext context) {
		return Collections.emptyList();
	}

	@Override
	public TlsInputType getInputType() {
		return TlsInputType.EMPTY;
	}

}
