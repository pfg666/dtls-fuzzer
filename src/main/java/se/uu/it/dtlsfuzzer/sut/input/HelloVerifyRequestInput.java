package se.uu.it.dtlsfuzzer.sut.input;

import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;

import de.rub.nds.tlsattacker.core.protocol.message.HelloVerifyRequestMessage;
import de.rub.nds.tlsattacker.core.protocol.message.ProtocolMessage;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;

public class HelloVerifyRequestInput extends DtlsInput {

	/**
	 * option for resetting the digests
	 */
	@XmlAttribute(name = "resetDigest", required = false)
	private boolean resetDigest = true;
	
	@XmlAttribute(name = "digestHR", required = false)
	private boolean digestHR = false;

	public HelloVerifyRequestInput() {
		super("HELLO_VERIFY_REQUEST");
	}

	@Override
	public List<ProtocolMessage> generateMessages(State state, ExecutionContext context) {
		return Arrays.asList(new HelloVerifyRequestMessage(state.getConfig()));
	}

	@Override
	public TlsInputType getInputType() {
		return TlsInputType.HANDSHAKE;
	}

	public void postSendDtlsUpdate(State state, ExecutionContext context) {
		if (resetDigest) {
			state.getTlsContext().getDigest().reset();
		}
	}
}
