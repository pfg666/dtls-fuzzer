package se.uu.it.dtlsfuzzer.sut.input;

import java.util.Arrays;
import java.util.List;

import de.rub.nds.tlsattacker.core.protocol.message.ProtocolMessage;
import de.rub.nds.tlsattacker.core.protocol.message.ServerHelloDoneMessage;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;

public class ServerHelloDoneInput extends DtlsInput {

	public ServerHelloDoneInput() {
		super("SERVER_HELLO_DONE");
	}

	@Override
	public List<ProtocolMessage> generateMessages(State state, ExecutionContext context) {
		return Arrays.asList(new ServerHelloDoneMessage(state.getConfig()));
	}
	
	@Override
	public TlsInputType getInputType() {
		return TlsInputType.HANDSHAKE;
	}

}
