package se.uu.it.dtlsfuzzer.sut.input;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import de.rub.nds.tlsattacker.core.protocol.message.ProtocolMessage;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.config.MapperConfig;
import se.uu.it.dtlsfuzzer.config.SulDelegate;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;
import se.uu.it.dtlsfuzzer.execute.InputExecutor;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class TlsInput extends AbstractInput {

	@XmlAttribute(name = "extendedWait", required = false)
	private Integer extendedWait;
	
	protected TlsInput() {
		super();
	}
	
	protected TlsInput (String name) {
		super(name);
	}

	/**
	 * Returns the preferred executor for this input, or null, if there isn't one, meaning the input does not require alterations to the typical execution of an input.
	 */
	public InputExecutor getPreferredExecutor(SulDelegate sulConfig, MapperConfig mapperConfig) {
		return null;
	}

	public abstract List<ProtocolMessage> generateMessages(State state, ExecutionContext context);
	
	/**
	 * Enables the input for execution.
	 */
	public boolean isEnabled(State state, ExecutionContext context) {
		return true;
	}
	
	/**
	 * Updates context before sending the input
	 */
	public void preSendUpdate(State state, ExecutionContext context) {
	}
	
	/**
	 * Updates the context after sending the input.
	 */
	public void postSendUpdate(State state, ExecutionContext context) {
	}

	public Integer getExtendedWait() {
		return extendedWait;
	}
	
	public void setExtendedWait(Integer extendedWait) {
		this.extendedWait = extendedWait;
	}

	/**
	 * Updates the context after receiving an output.
	 */
	public TlsOutput postReceiveUpdate(TlsOutput output, State state,
			ExecutionContext context) {
		return output;
	}

	/**
	 * The type of the input should correspond to the type of the message the
	 * input generates.
	 */
	public abstract TlsInputType getInputType();
}
