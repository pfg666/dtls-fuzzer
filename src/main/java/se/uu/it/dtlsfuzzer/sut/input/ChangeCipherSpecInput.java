package se.uu.it.dtlsfuzzer.sut.input;

import java.util.Arrays;
import java.util.List;

import de.rub.nds.tlsattacker.core.protocol.message.ChangeCipherSpecMessage;
import de.rub.nds.tlsattacker.core.protocol.message.ProtocolMessage;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.execute.ExecutionContext;

public class ChangeCipherSpecInput extends DtlsInput {

	public ChangeCipherSpecInput() {
		super("CHANGE_CIPHER_SPEC");
	}
	
	@Override
	public void preSendDtlsUpdate(State state, ExecutionContext context) {
		context.setWriteRecordNumberEpoch0(state.getTlsContext().getWriteSequenceNumber() + 1);
	}

	public List<ProtocolMessage> generateMessages(State state, ExecutionContext context) {
		ChangeCipherSpecMessage ccs = new ChangeCipherSpecMessage(
				state.getConfig());
		return Arrays.asList(ccs);
	}

	@Override
	public void postSendDtlsUpdate(State state, ExecutionContext context) {
		state.getTlsContext().getRecordLayer().updateEncryptionCipher();
		state.getTlsContext().setWriteSequenceNumber(0);
	}

	@Override
	public TlsInputType getInputType() {
		return TlsInputType.CCS;
	}

}
