package se.uu.it.dtlsfuzzer.bugcheck;

import de.learnlib.api.SUL;
import de.learnlib.api.oracle.MembershipOracle.MealyMembershipOracle;
import de.rub.nds.tlsattacker.core.state.State;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.words.Alphabet;
import se.uu.it.dtlsfuzzer.bugcheck.patterns.BugPatterns;
import se.uu.it.dtlsfuzzer.config.BugCheckerConfig;
import se.uu.it.dtlsfuzzer.sut.TlsSUL;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.OutputMapper;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

public class BugCheckerContext {
	
	private FastMealy<TlsInput, TlsOutput> model;
	private Alphabet<TlsInput> alphabet;
	private SUL<TlsInput, TlsOutput> tlsSul;
	private MealyMembershipOracle<TlsInput, TlsOutput> sulOracle;
	private OutputMapper outputMapper;
	private TlsSUL actualTlsSul;
	private BugCheckerConfig config;
	private ModelInputs modelInputs;
	private BugPatterns bugPatterns;
	private StatisticsTracker statisticsTracker;
	
	public BugCheckerContext(BugCheckerConfig config) {
		this.config = config;
	}
	
	/* Utils */
	
	/* Getters */
	
	public State getState() {
		return actualTlsSul.getState();
	}
	
	public BugCheckerConfig getConfig() {
		return config;
	}
	 
	public FastMealy<TlsInput, TlsOutput> getModel() {
		return model;
	}
	
	public Alphabet<TlsInput> getAlphabet() {
		return alphabet;
	}
	public SUL<TlsInput, TlsOutput> getTlsSul() {
		return tlsSul;
	}
	
	public ModelInputs getModelInputs() {
		return modelInputs;
	}
	
	public MealyMembershipOracle<TlsInput, TlsOutput> getSulOracle() {
		return sulOracle;
	}
	
	public OutputMapper getOutputMapper() {
		return outputMapper;
	}
	
	public BugPatterns getBugPatterns() {
		return bugPatterns;
	}
	
	public StatisticsTracker getStatisticsTracker() {
		return statisticsTracker;
	}

	
	/* Setters */

	void setModel(FastMealy<TlsInput, TlsOutput> model) {
		this.model = model;
	}
	
	void setAlphabet(Alphabet<TlsInput> alphabet) {
		this.alphabet = alphabet;
	}

	void setWrappedTlsSul(SUL<TlsInput, TlsOutput> tlsSul) {
		this.tlsSul = tlsSul;
	}

	void setActualTlsSul(TlsSUL actualTlsSul) {
		this.actualTlsSul = actualTlsSul;
	}
	
	void setModelInputs(ModelInputs modelInputs) {
		this.modelInputs = modelInputs;
	}

	void setSulOracle(MealyMembershipOracle<TlsInput, TlsOutput> sulOracle) {
		this.sulOracle = sulOracle;
	}
	
	void setOutputMapper(OutputMapper outputMapper) {
		this.outputMapper = outputMapper;
	}

	public void setLoadedBugPatterns(BugPatterns bugPatterns) {
		this.bugPatterns = bugPatterns;
	}

	public void setStatisticsTracker(StatisticsTracker statisticsTracker) {
		this.statisticsTracker = statisticsTracker;
	}
}
