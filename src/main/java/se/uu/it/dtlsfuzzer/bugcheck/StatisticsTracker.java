package se.uu.it.dtlsfuzzer.bugcheck;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import de.learnlib.filter.statistic.Counter;
import se.uu.it.dtlsfuzzer.bugcheck.model.ModelBug;
import se.uu.it.dtlsfuzzer.bugcheck.patterns.BugPattern;

public class StatisticsTracker {
	
	private Counter inputCounter;
	private Counter resetCounter;
	private long startTime;
	private long totalTime;
	private BugCheckerContext context;
	private long totalBugs;

	private long verificationInputCount;
	private long verificationResetCount;
	
	private Set<BugPattern> loadedBugPatterns;
	private Set<BugPattern> foundBugPatterns;
	private Set<BugPattern> verifiedBugPatterns;
	
	private Map<BugPattern, Long> bugPatternVerificationInputCount;
	private Map<BugPattern, Long> bugPatternVerificationResetCount;
	
	public StatisticsTracker() {
		loadedBugPatterns = new TreeSet<>(bpComp());
		foundBugPatterns = new TreeSet<>(bpComp());
		verifiedBugPatterns = new TreeSet<>(bpComp());
		bugPatternVerificationInputCount = new TreeMap<>(bpComp());
		bugPatternVerificationResetCount = new TreeMap<>(bpComp());
	}
	
	private Comparator<BugPattern> bpComp() {
		return (bp1, bp2) -> bp1.getId().compareTo(bp2.getId());
	}
	
	public void setSutTracking(Counter inputCounter, Counter resetCounter) {
		this.inputCounter = inputCounter;
		this.resetCounter = resetCounter;
	}
	
	
	public void startBugChecking(BugCheckerContext context) {
		this.context = context;
		startTime = System.currentTimeMillis();
	}
	
	/**
	 * Gets the current time of the experiment
	 */
	public long currentTimeMillis() {
		return System.currentTimeMillis() - startTime;
	}
	
	public void loadBugPatterns(Collection<BugPattern> bugPatterns) {
		loadedBugPatterns = new LinkedHashSet<>(bugPatterns);
	}
	
	public void finishBugChecking(List<Bug> bugs, BugCheckerContext context) {
		totalTime = System.currentTimeMillis() - startTime; 
		for (Bug bug : bugs) {
			if (bug instanceof ModelBug) {
				ModelBug modelBug = (ModelBug) bug;
				modelBug.getBugPatterns().forEach(bp -> {
					foundBugPatterns.add(bp);
				});
				
				if (modelBug.getStatus() == BugVerificationStatus.VERIFICATION_SUCCESSFUL) {
					verifiedBugPatterns.addAll(modelBug.getBugPatterns());
				}
			}
		} 
		totalBugs = bugs.size();
	}
	
	public void startVerification(BugPattern bugPattern) {
		verificationInputCount = inputCounter.getCount();
		verificationResetCount = resetCounter.getCount();
	}
	
	public void endVerification(BugPattern bugPattern) {
		bugPatternVerificationInputCount.put(bugPattern, inputCounter.getCount() - verificationInputCount);
		bugPatternVerificationResetCount.put(bugPattern, resetCounter.getCount() - verificationResetCount);
	}
	
	public Statistics generateStatistics() {
		Statistics statistics = new Statistics(context);
		statistics.setSutUsed(inputCounter != null);
		
		if (inputCounter != null) {
			statistics.setInputs(inputCounter.getCount());
			statistics.setResets(resetCounter.getCount());
		}
		statistics.setTotalBugs(totalBugs);
		statistics.setLoadedBugPatterns(loadedBugPatterns);
		statistics.setFoundBugPatterns(foundBugPatterns);
		statistics.setVerifiedBugPatterns(verifiedBugPatterns);
		statistics.setBugPatternVerificationCounts(bugPatternVerificationInputCount, bugPatternVerificationResetCount);
		statistics.setTotalTime(totalTime);
		
		return statistics;
	}
}
