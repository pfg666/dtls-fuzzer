package se.uu.it.dtlsfuzzer.bugcheck.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import se.uu.it.dtlsfuzzer.bugcheck.Bug;
import se.uu.it.dtlsfuzzer.bugcheck.BugSeverity;
import se.uu.it.dtlsfuzzer.bugcheck.BugType;
import se.uu.it.dtlsfuzzer.bugcheck.BugVerificationStatus;
import se.uu.it.dtlsfuzzer.bugcheck.patterns.BugPattern;
import se.uu.it.dtlsfuzzer.specification.TlsFlow;

public class ModelBug extends Bug{
	
	private List<BugPattern> bugPatterns;
	private TlsFlow counterexample;
	
	public ModelBug(TlsFlow flow, List<BugPattern> bugPatterns) {
		super(flow);
		if (bugPatterns.isEmpty()) {
			throw new InternalError("There should be at least one pattern");
		}
		if (flow == null) {
			throw new InternalError("Flow cannot be null");
		}
		this.bugPatterns = new ArrayList<>(bugPatterns);
		Collections.sort(bugPatterns, (b1, b2) -> b1.getShortenedName().compareTo(b2.getShortenedName()));
		BugSeverity maxSeverity = bugPatterns.stream().map(bp -> bp.getSeverity()).max((s1, s2) -> Integer.compare(s1.ordinal(), s2.ordinal())).get();
		setSeverity(maxSeverity);
	}

	public ModelBug(TlsFlow flow, BugPattern pattern) {
		super(flow);
		this.bugPatterns = Arrays.asList(pattern);
		setSeverity(pattern.getSeverity());
	}
	
	public List<BugPattern> getBugPatterns() {
		return bugPatterns;
	}

	@Override
	public String getDescription() {
		StringBuilder sb = new StringBuilder();
		sb.append("Detected the following bug patterns in flow");
		sb.append(System.lineSeparator());
		for (BugPattern pattern : bugPatterns) {
			sb.append("Pattern: ").append(pattern.getName())
			.append(System.lineSeparator())
			.append("Severity: ").append(pattern.getSeverity())
			.append(System.lineSeparator())
			.append("Description: ").append(pattern.getDescription())
			.append(System.lineSeparator());
		}
		sb.append(getFlow().toCompactString())
		.append(System.lineSeparator());
		sb.append("Verification Status: ")
		.append(getStatus()).append(System.lineSeparator());
		if (getStatus() == BugVerificationStatus.VERIFICATION_FAILED) {
			sb.append("Counterexample: ").append(counterexample.toCompactString());
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
	
	
	public void verificationFailed(TlsFlow counterexample) {
		setStatus(BugVerificationStatus.VERIFICATION_FAILED);
		this.counterexample = counterexample;
	}
	
	public void verificationSuccessful() {
		setStatus(BugVerificationStatus.VERIFICATION_SUCCESSFUL);
	}
	
	@Override
	public BugType getType() {
		return BugType.MODEL;
	}
	
	public String getSubType() {
		return String.join(",", bugPatterns.stream()
				.map(bp -> bp.getShortenedName())
				.toArray(String []::new));
	}

	@Override
	public BugSeverity getDefaultSeverity() {
		return BugSeverity.UNKNOWN;
	}
	
	@Override
	public BugVerificationStatus getDefaultStatus() {
		return BugVerificationStatus.NOT_VERIFIED;
	}
}
