package se.uu.it.dtlsfuzzer.bugcheck.patterns;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import se.uu.it.dtlsfuzzer.bugcheck.BugSeverity;
import se.uu.it.dtlsfuzzer.specification.DtlsLanguageAdapter;

@XmlRootElement(name = "bugPatterns")
public class BugPatterns {
	
	@XmlElement(name = "specificationLanguage", required = true)
	private String specificationLanguagePath;
	
	@XmlTransient
	private DtlsLanguageAdapter specificationLanguage;
	
	@XmlElement(name = "genericBugPattern")
	private List<GenericBugPattern> genericBugPatterns;
	
	@XmlElement(name = "defaultBugSeverity")
	private BugSeverity defaultBugSeverity;
	
	BugPatterns() {
		genericBugPatterns = new ArrayList<>();
	}

	public String getSpecificationLanguagePath() {
		return specificationLanguagePath;
	}

	public DtlsLanguageAdapter getSpecificationLanguage() {
		return specificationLanguage;
	}
	
	void setSpecificationLanguage(DtlsLanguageAdapter specificationLanguage) {
		this.specificationLanguage = specificationLanguage;
	}

	public List<GenericBugPattern> getGenericBugPatterns() {
		return genericBugPatterns;
	}

	
	public List<BugPattern> getBugPatterns() {
		List<BugPattern> bugPatterns = new ArrayList<BugPattern>(genericBugPatterns.size());
		bugPatterns.addAll(genericBugPatterns);
		return bugPatterns;
	}
	
	public int bugPatternNumber() {
		return genericBugPatterns.size();
	}
	
	public BugSeverity getDefaultBugSeverity() {
		return defaultBugSeverity;
	}
}
