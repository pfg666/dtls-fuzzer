package se.uu.it.dtlsfuzzer.bugcheck;

import se.uu.it.dtlsfuzzer.sut.input.NameTlsInputMapping;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;

/**
 * Provides an interface for accessing inputs extracted from the model that are used in bug checking.
 */
public class ModelInputs {
	
	/* These strings should stay consistent with inputs in the examples directory.
	 * FIXME We should have an internal "Definitions" class which is a collection of named TLS inputs. 
	 * The alphabet would become a list of symbolic inputs/names, rather TlsInputs.
	 * We'd provide definitions for these inputs internally, in a "input_definitions.xml" file.
	 * 
	 * The default definition can be changed by providing a custom definitions file which re-defines inputs for the same name. 
	 *  
	 * */
	private static String APPLICATION="APPLICATION";
	private static String CLOSE_NOTIFY="Alert(WARNING,CLOSE_NOTIFY)";
	private static String UNEXPECTED_MESSAGE="Alert(FATAL,UNEXPECTED_MESSAGE)";
	private static String HELLO_VERIFY_REQUEST="HELLO_VERIFY_REQUEST";
	private static String HELLO_REQUEST="HELLO_REQUEST";
	private static String CHANGE_CIPHER_SPEC="CHANGE_CIPHER_SPEC";
	private static String CERTIFICATE_VERIFY="CERTIFICATE_VERIFY";
	private static String FINISHED="FINISHED";
	private static String CERTIFICATE="CERTIFICATE";
	
	private NameTlsInputMapping mapping;
	
	
	public ModelInputs(NameTlsInputMapping mapping) {
		this.mapping = mapping;
	}
	
	public TlsInput getApplication() {
		return mapping.getInput(APPLICATION);
	}
	
	public TlsInput getAlertCloseNotify() {
		return mapping.getInput(CLOSE_NOTIFY);
	}
	
	public TlsInput getAlertUnexpectedMessage() {
		return mapping.getInput(UNEXPECTED_MESSAGE);
	}
	
	public TlsInput getChangeCipherSpec() {
		return mapping.getInput(CHANGE_CIPHER_SPEC);
	}
	
	public TlsInput getCertificateVerify() {
		return mapping.getInput(CERTIFICATE_VERIFY);
	}
	
	public TlsInput getFinished() {
		return mapping.getInput(FINISHED);
	}
	
	public TlsInput getCertificate() {
		return mapping.getInput(CERTIFICATE);
	}
	
	public TlsInput getHelloVerifyRequest() {
		return mapping.getInput(HELLO_VERIFY_REQUEST);
	}
	
	public TlsInput getHelloRequest() {
		return mapping.getInput(HELLO_REQUEST);
	}
	
	public boolean isAlert(TlsInput input) {
		return input.equals(getAlertCloseNotify()) || input.equals(getAlertUnexpectedMessage());
	}
	
}
