package se.uu.it.dtlsfuzzer.bugcheck;

public enum BugVerificationStatus {
	VERIFICATION_SUCCESSFUL,
	VERIFICATION_FAILED,
	NOT_VERIFIED
}
