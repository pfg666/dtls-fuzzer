package se.uu.it.dtlsfuzzer.bugcheck;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import net.automatalib.words.Alphabet;
import se.uu.it.dtlsfuzzer.ExportableResult;
import se.uu.it.dtlsfuzzer.bugcheck.patterns.BugPattern;
import se.uu.it.dtlsfuzzer.config.BugCheckerConfig;
import se.uu.it.dtlsfuzzer.config.SulDelegate;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;

// TODO this class needs to be redesigned 
public class Statistics extends ExportableResult{
	private long totalTime;
	private long inputs;
	private long resets;
	private boolean sutUsed;
	private long totalBugs;
	
	// Model bug finder
	private Collection<BugPattern> foundBugPatterns;
	private Collection<BugPattern> loadedBugPatterns;
	private Collection<BugPattern> verifiedBugPatterns;
	private BugCheckerConfig config;
	private Alphabet<TlsInput> alphabet;
	
	private Map<BugPattern, Long> bpInputCount;
	private Map<BugPattern, Long> bpResetCount;
	
	public Statistics(BugCheckerContext context) {
		foundBugPatterns = Collections.emptyList();
		loadedBugPatterns = Collections.emptyList();
		verifiedBugPatterns = Collections.emptyList();
		config = context.getConfig();
		alphabet = context.getAlphabet();
	}
	
	
	@Override
	public String toString() {
		StringWriter sw = new StringWriter();
		export(sw);
		String statsString = sw.toString();
		return statsString;
	}

	public void doExport(PrintWriter out) {
		title("Run Description", out);
		generateRunDescription(out, config, alphabet);
		title("Statistics", out);

		section("General", out);
		if (sutUsed) {
			out.println("Number of inputs: " + inputs);
			out.println("Number of resets: " + resets);
		}
		out.println("Number of bugs: " + totalBugs);
		out.println("Time bug-checking took (ms): " + totalTime);
		
		section("Model Bug Finder", out);
		printCollection("Bug patterns loaded", loadedBugPatterns, BugPattern::getName, out);
		printCollection("Bug patterns found", foundBugPatterns, BugPattern::getName, out);
		printCollection("Bug patterns verified successfully", verifiedBugPatterns, BugPattern::getName, out);
		if (config.isVerifyBugs() && bpInputCount != null) {
			printCounterMap("Verification Inputs per Bug Pattern", bpInputCount, BugPattern::getName, out);
			printCounterMap("Verification Resets per Bug Pattern", bpResetCount, BugPattern::getName, out);
		}
		
		out.close();
	}
	
	private <T,MT> void printCounterMap(String name, Map<T, Long> map, Function<T,MT> mapping, PrintWriter out) {
		out.println(name);
		map.forEach((k,v) -> out.println("   " + mapping.apply(k) + " : " + v));
		out.println();
	}
	
	private <T,MT> void printCollection(String name, Collection<T> collection, Function<T,MT> mapping, PrintWriter out) {
		out.format(name + " (%d): %s", collection.size(), collection.stream().map(mapping).collect(Collectors.toList()).toString());
		out.println();
	}
	
	public void generateRunDescription(PrintWriter out, BugCheckerConfig config, Alphabet<TlsInput> alphabet) {
		section("Bug Checking Parameters", out);
		out.println("SUT: " + (config.isClient() ? "client" : "server") );
		out.println("Alphabet: " + alphabet);
		
		out.println(String.format("Loaded Bug Patterns (%d): %s", loadedBugPatterns.size(), 
					loadedBugPatterns.stream()
					.map(bp -> bp.getName()).collect(Collectors.toList()).toString()));
		out.println("Bug Verification Enabled: " + config.isVerifyBugs());
		out.println("Uncategorized Bug Bound: " + config.getUncategorizedBugBound());
		
		if (sutUsed) {
			section("TLS SUL Parameters", out);
			SulDelegate sulDelegate = config.getSulDelegate();
			out.println("Protocol: " + sulDelegate.getProtocolVersion());
			out.println("ResetWait: " + sulDelegate.getResetWait());
			out.println("Timeout: " + sulDelegate.getTimeout());
			if (sulDelegate.getCommand() != null) {
				out.println("RunWait: " + sulDelegate.getRunWait());
				out.println("Command: " + sulDelegate.getCommand());
			}
		}
	}
	public long getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}

	public long getInputs() {
		return inputs;
	}
	public void setInputs(long inputs) {
		this.inputs = inputs;
	}
	public long getResets() {
		return resets;
	}
	public void setResets(long resets) {
		this.resets = resets;
	}
	
	public void setSutUsed(boolean sutUsed) {
		this.sutUsed = sutUsed;
	}

	public void setTotalBugs(long totalBugs) {
		this.totalBugs = totalBugs;
	}

	public void setFoundBugPatterns(Collection<BugPattern> modelBugPatternsFound) {
		this.foundBugPatterns = modelBugPatternsFound;
	}

	public void setLoadedBugPatterns(Collection<BugPattern> loadedBugPatterns) {
		this.loadedBugPatterns = loadedBugPatterns;
	}
	
	public void setVerifiedBugPatterns(Collection<BugPattern> verifiedBugPatterns) {
		this.verifiedBugPatterns = verifiedBugPatterns;
	}
	
	public void setBugPatternVerificationCounts(Map<BugPattern, Long> bpInputCount, Map<BugPattern, Long> bpResetCount) {
		this.bpInputCount = bpInputCount;
		this.bpResetCount = bpResetCount;
	}
	
}
