package se.uu.it.dtlsfuzzer.bugcheck;

import java.util.Arrays;
import java.util.Optional;

import de.rub.nds.tlsattacker.core.constants.CertificateKeyType;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

/**
 * Provides a interface for analyzing outputs so that how the actual strings are formed is decoupled from the checking code.
 */
public class ModelOutputs {
	private static String APPLICATION="APPLICATION";
	private static String FINISHED="FINISHED";
	private static String ALERT="Alert";
	private static String CLOSE_NOTIFY="Alert(WARNING,CLOSE_NOTIFY)";
	private static String UNEXPECTED_MESSAGE="Alert(FATAL,UNEXPECTED_MESSAGE)";
	private static String CLIENT_HELLO="CLIENT_HELLO";
	private static String SERVER_HELLO="SERVER_HELLO";
	private static String CHANGE_CIPHER_SPEC="CHANGE_CIPHER_SPEC";
	private static String CERTIFICATE="CERTIFICATE";
	private static String EMPTY_CERTIFICATE="EMPTY_CERTIFICATE";
	private static String HELLO_VERIFY_REQUEST="HELLO_VERIFY_REQUEST";
	
	public static boolean hasApplication(TlsOutput output) {
		return output.getAbstractOutput().contains(APPLICATION);
	}
	
	public static TlsOutput getApplicationOutput() {
		return new TlsOutput(APPLICATION);
	}
	
	public static boolean isApplication(TlsOutput output) {
		return output.getAbstractOutput().equals(APPLICATION);
	}
	
	public static boolean hasCertificate(TlsOutput output) {
		return output.getAbstractOutput().contains(CERTIFICATE);
	}
	
	public static boolean hasNonEmptyCertificate(TlsOutput output) {
		for (TlsOutput atomicOutput : output.getAtomicOutputs()) {
			if (atomicOutput.getAbstractOutput().contains(CERTIFICATE) && !atomicOutput.getAbstractOutput().equals(EMPTY_CERTIFICATE)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasEmptyCertificate(TlsOutput output) {
		return output.getAbstractOutput().contains(EMPTY_CERTIFICATE);
	}
	
	public static boolean hasServerHello(TlsOutput output) {
		return output.getAbstractOutput().contains(SERVER_HELLO);
	}
	
	public static boolean hasClientHello(TlsOutput output) {
		return output.getAbstractOutput().contains(CLIENT_HELLO);
	}
	
	public static String getClientHelloString() {
		return CLIENT_HELLO;
	}
	
	public static boolean hasHelloVerifyRequest(TlsOutput output) {
		return output.getAbstractOutput().contains(HELLO_VERIFY_REQUEST);
	}
	
	public static boolean hasChangeCipherSpec(TlsOutput output) {
		return output.getAbstractOutput().contains(CHANGE_CIPHER_SPEC);
	}
	
	public static boolean hasAlert(TlsOutput output) {
		return output.getAbstractOutput().contains(ALERT);
	}
	
	public static boolean hasFinished(TlsOutput output) {
		return output.getAbstractOutput().contains(FINISHED);
	}
	
	public static CertificateKeyType getCertificateType(TlsOutput output) {
		Optional<CertificateKeyType> opt = Arrays.stream(CertificateKeyType.values()).filter(ctype -> output.getAbstractOutput().contains(ctype.name())).findFirst();
		return opt.orElseGet(() -> null);
	}
	
}
