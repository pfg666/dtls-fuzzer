package se.uu.it.dtlsfuzzer.bugcheck;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alexmerz.graphviz.ParseException;

import de.learnlib.api.oracle.MembershipOracle.MealyMembershipOracle;
import de.learnlib.oracle.membership.SULOracle;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.util.ts.copy.TSCopy;
import net.automatalib.words.Alphabet;
import se.uu.it.dtlsfuzzer.CleanupTasks;
import se.uu.it.dtlsfuzzer.ModelFactory;
import se.uu.it.dtlsfuzzer.StateFuzzer;
import se.uu.it.dtlsfuzzer.TestParser;
import se.uu.it.dtlsfuzzer.bugcheck.model.ModelBugFinder;
import se.uu.it.dtlsfuzzer.config.BugCheckerConfig;
import se.uu.it.dtlsfuzzer.config.MapperConfig;
import se.uu.it.dtlsfuzzer.config.StateFuzzerClientConfig;
import se.uu.it.dtlsfuzzer.config.StateFuzzerConfig;
import se.uu.it.dtlsfuzzer.config.StateFuzzerServerConfig;
import se.uu.it.dtlsfuzzer.config.SulClientDelegate;
import se.uu.it.dtlsfuzzer.config.SulServerDelegate;
import se.uu.it.dtlsfuzzer.execute.PhasedInputExecutor;
import se.uu.it.dtlsfuzzer.learn.Extractor;
import se.uu.it.dtlsfuzzer.learn.Extractor.ExtractorResult;
import se.uu.it.dtlsfuzzer.learn.StateMachine;
import se.uu.it.dtlsfuzzer.sut.CachingSULOracle;
import se.uu.it.dtlsfuzzer.sut.ObservationTree;
import se.uu.it.dtlsfuzzer.sut.TlsSULBuilder;
import se.uu.it.dtlsfuzzer.sut.input.AlphabetFactory;
import se.uu.it.dtlsfuzzer.sut.input.NameTlsInputMapping;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.OutputMapper;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

/**
 * Checks systems against bugs using the learned model.
 */
public class BugChecker {
	
	private static final Logger LOGGER = LogManager.getLogger(BugChecker.class);
	
	private static final int BUG_ID_LENGTH = 3;
	private static final String REPORT = "bug_report.txt";
	private static final String BUG_DIR = "bugs";
	
	private BugCheckerConfig config;
	private CleanupTasks cleanupTasks;
	private StatisticsTracker tracker;
	
	
	public BugChecker(BugCheckerConfig config) {
		this.config = config;
		this.tracker = new StatisticsTracker();
	}
	
	public BugCheckerReport bugcheck() throws ParseException, IOException, JAXBException, XMLStreamException {
		BugCheckerReport report = new BugCheckerReport();
		cleanupTasks = new CleanupTasks();
		try {
			BugCheckerContext context = buildContext();
			loadModel(context);
			tracker.startBugChecking(context);
			List<Bug> bugs = new LinkedList<Bug>();
			ModelBugFinder modelBugFinder = new ModelBugFinder(context);
			modelBugFinder.findBugs(context, bugs);
			tracker.finishBugChecking(bugs, context);
			report.setStatistics(tracker.generateStatistics());
			report.addBugs(bugs);
			
			exportResultToFolder(report);
			LOGGER.info(report.exportToString());
		} finally {
			cleanupTasks.execute();
		}
		return report;
	}

	private BugCheckerContext buildContext() throws ParseException, IOException, JAXBException, XMLStreamException {
		LOGGER.debug("Bug Checker context initialization started");
		BugCheckerContext context = new BugCheckerContext(config);
		context.setStatisticsTracker(tracker);
		Alphabet<TlsInput> alphabet = AlphabetFactory.buildAlphabet(config);
		MapperConfig mapperConfig = config.getMapperConfig();
		if (isTestingRequired()) {
			TlsSULBuilder builder = new TlsSULBuilder(config.getSulDelegate(), mapperConfig, new PhasedInputExecutor(mapperConfig), cleanupTasks);
			tracker.setSutTracking(builder.getInputCounter(), builder.getResetCounter());
			
			
			context.setActualTlsSul(builder.getTLSSul());
			context.setWrappedTlsSul(builder.getWrappedTlsSUL());
			
			// we cache so there is less worry about duplicating tests
			MealyMembershipOracle<TlsInput, TlsOutput> sulOracle = new SULOracle<>(builder.getWrappedTlsSUL());
			ObservationTree<TlsInput, TlsOutput> cache = new ObservationTree<TlsInput,TlsOutput>();
			if (mapperConfig.isSocketClosedAsTimeout()) {
				sulOracle = new CachingSULOracle<TlsInput, TlsOutput>(
					sulOracle, new ObservationTree<TlsInput,TlsOutput>(), false);
			} else {
				sulOracle = new CachingSULOracle<TlsInput, TlsOutput>(
						sulOracle, cache, false, TlsOutput.socketClosed());
			}
			
			context.setSulOracle(new SULOracle<>(builder.getWrappedTlsSUL()));

		} else {
			LOGGER.info("Current configuration does not necessitate running tests, hence skipping SUT instantiation");
		}
		context.setAlphabet(alphabet);
		OutputMapper outputMapper = new OutputMapper(config.getMapperConfig());
		context.setOutputMapper(outputMapper);
		
		new File(config.getOutput()).mkdirs();

		LOGGER.debug("Bug Checker context initialization finished");
		return context;
	}
	
	private void loadModel(BugCheckerContext context) throws FileNotFoundException, ParseException {
		FastMealy<TlsInput, TlsOutput> model = null;
		if (config.getModel() != null) {
			model = ModelFactory.buildTlsModel(context.getAlphabet(), config.getModel());
		} else {
			LOGGER.info("Model not provided, hence generating it via state fuzzing");
			StateFuzzerConfig stateFuzzerConfig = null;
			if (config.isClient()) {
				stateFuzzerConfig = new StateFuzzerClientConfig((SulServerDelegate) config.getSulDelegate());
			} else {
				stateFuzzerConfig = new StateFuzzerServerConfig((SulClientDelegate) config.getSulDelegate());
			}
			stateFuzzerConfig.setLearningConfig(config.getLearningConfig());
			stateFuzzerConfig.setOutput(config.getOutput());
			
			Extractor extractor = new Extractor(stateFuzzerConfig, context.getAlphabet(), cleanupTasks);
			ExtractorResult result = extractor.extractStateMachine();
			StateMachine stateMachine = null;
			if (result.getLearnedModel() != null) {
				stateMachine = result.getLearnedModel();
				LOGGER.info("State fuzzing converged, using learned model");
			} else {
				if (!result.getHypotheses().isEmpty()) {
					LOGGER.info("State fuzzing did not converge, using last hypothesis");
					stateMachine = result.getHypotheses().get(result.getHypotheses().size()-1);
				} else {
					throw new RuntimeException("Failed to generate model for system");
				}
			}
			
			model = new FastMealy<TlsInput, TlsOutput>(context.getAlphabet());
			AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, stateMachine.getMealyMachine(), context.getAlphabet(), model);
		}
		context.setModel(model);
		ModelInputs modelInputs = new ModelInputs(new NameTlsInputMapping(model.getInputAlphabet()));
		context.setModelInputs(modelInputs);
	}
	
	private boolean isTestingRequired() {
		return config.isVerifyBugs() || config.getModel() == null;
	}
	
	private void exportResultToFolder(BugCheckerReport report) throws IOException {
		LOGGER.info("Bug Checker exporting results and test cases");
		File outputFolder = new File(config.getOutput());
		outputFolder.mkdirs();
		File reportFile = new File(outputFolder, REPORT);
		TestParser parser = new TestParser();
		
		try (FileWriter fw = new FileWriter(reportFile)) {
			report.export(fw);
		}
		
		File bugFolder = new File(config.getOutput(), BUG_DIR);
		bugFolder.mkdir();
		
		for (Bug bug : report.getBugs()) {
			File bugFile = new File(bugFolder, bugToCompactRepresentation(bug));
			parser.writeTest(bug.getFlow().getInputWord(), bugFile);
		}
		LOGGER.info("Bug Checker exporting results done and test cases");
	}
	
	private String bugToCompactRepresentation(Bug bug) {
		String repr = "bug_id_" + idToString(bug.getId()) + "_type_"+bug.getType().name().toLowerCase();
		if (bug.getSubType() != null) {
			repr += "_subtype_" + bug.getSubType().toLowerCase();
		}
		return repr;
	}
	
	private String idToString(Integer id) {
		String idString = String.valueOf(id);
		StringBuilder builder = new StringBuilder();
		
		for (int i=0; i< BUG_ID_LENGTH - idString.length(); i++) {
			builder.append('0');
		}
		builder.append(idString);
		return builder.toString();
	}
}
