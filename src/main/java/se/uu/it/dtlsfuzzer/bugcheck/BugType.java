package se.uu.it.dtlsfuzzer.bugcheck;

public enum BugType {
	MODEL("Bug derived from model (most likely a handshake with Invalid Sequence of Messages)"),
	UNENCRYPTED_MESSAGE_PROCESSING("Successful Processing of Unencrypted Message"),
	INVALID_EPOCH("Handshake with Invalid Epoch"),
	MISHANDLED_REORDERING("Bad behavior in processing re-ordered messages");
	
	private String description;
	
	private BugType(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
}
