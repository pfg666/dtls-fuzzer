package se.uu.it.dtlsfuzzer.bugcheck.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.learnlib.oracle.membership.SimulatorOracle.MealySimulatorOracle;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import se.uu.it.dtlsfuzzer.bugcheck.Bug;
import se.uu.it.dtlsfuzzer.bugcheck.BugCheckerContext;
import se.uu.it.dtlsfuzzer.bugcheck.BugFinder;
import se.uu.it.dtlsfuzzer.bugcheck.StatisticsTracker;
import se.uu.it.dtlsfuzzer.bugcheck.patterns.BugPattern;
import se.uu.it.dtlsfuzzer.bugcheck.patterns.BugPatternLoader;
import se.uu.it.dtlsfuzzer.bugcheck.patterns.BugPatterns;
import se.uu.it.dtlsfuzzer.config.BugCheckerConfig;
import se.uu.it.dtlsfuzzer.config.GenerationStrategy;
import se.uu.it.dtlsfuzzer.config.WitnessFinderConfig;
import se.uu.it.dtlsfuzzer.specification.DtlsLabelMapping;
import se.uu.it.dtlsfuzzer.specification.DtlsLanguageAdapter;
import se.uu.it.dtlsfuzzer.specification.DtlsLanguageAdapterBuilder;
import se.uu.it.dtlsfuzzer.specification.MessageLabel;
import se.uu.it.dtlsfuzzer.specification.TlsFlow;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;
import se.uu.it.dtlsfuzzer.witness.SequenceGenerator;
import se.uu.it.dtlsfuzzer.witness.SequenceGeneratorFactory;
import se.uu.it.dtlsfuzzer.witness.WitnessFinder;

public class ModelBugFinder implements BugFinder {
	private static final Logger LOGGER = LogManager.getLogger(ModelBugFinder.class);

	private BugPatterns patterns;
	private StatisticsTracker tracker;
	private DtlsLabelMapping mapping;
	private FastMealy<TlsInput, TlsOutput> model;
	private File outputDir;
	private Alphabet<TlsInput> alphabet;
	private boolean client;
	private WitnessFinder witnessFinder;

	private BugCheckerConfig config;

	public ModelBugFinder(BugCheckerContext context) {
		mapping = DtlsLabelMapping.buildMapping(context.getModel(), context.getAlphabet(), context.getConfig().isClient());
		model = context.getModel();
		patterns = BugPatternLoader.loadPatterns(context.getConfig().getSpecificationConfig(), mapping);
		context.setLoadedBugPatterns(patterns);
		alphabet = context.getAlphabet();
		config = context.getConfig();
		tracker = context.getStatisticsTracker();
		tracker.loadBugPatterns(patterns.getBugPatterns());
		client = context.getConfig().isClient();
		outputDir = new File(context.getConfig().getOutput());
		if (config.isVerifyBugs()) {
			WitnessFinderConfig witnessFinderConfig = context.getConfig().getWitnessFinderConfig();
			SequenceGenerator<MessageLabel> sequenceGenerator = SequenceGeneratorFactory.buildGenerator(witnessFinderConfig.getWitnessGenerationStrategy(), witnessFinderConfig.getSearchConfig(), null);
			witnessFinder = new WitnessFinder(context.getSulOracle(), mapping, sequenceGenerator, witnessFinderConfig.getBound());
		} else {
			MealySimulatorOracle<TlsInput, TlsOutput> mealyOracle = new MealySimulatorOracle<>(model);
			SequenceGenerator<MessageLabel> sequenceGenerator = SequenceGeneratorFactory.buildGenerator(GenerationStrategy.SHORTEST, null, null);
			witnessFinder = new WitnessFinder(mealyOracle, mapping, sequenceGenerator, -1);
		}
	}

	@Override
	public void findBugs(BugCheckerContext context, List<Bug> bugs) {
		DtlsLanguageAdapter sutFullLanguage = DtlsLanguageAdapterBuilder
				.fromSystemModel(model, alphabet, mapping, client);
		
		DtlsLanguageAdapter sutLanguage = sutFullLanguage; 
		exportDfa(sutLanguage, "sutLanguage.dot");
		List<BugPattern> detectedPatterns = new LinkedList<>(); 

		// match against each loaded bug pattern 
		for (BugPattern bugPattern : patterns.getBugPatterns()) {
			LOGGER.info("Checking bug pattern {}", bugPattern.getShortenedName());
			DtlsLanguageAdapter bugLanguage = bugPattern.generateBugLanguage();
			
			if (bugLanguage.isEmpty()) {
				LOGGER.info("The bug pattern {} is an empty language when considering only the input/output labels from the SUT model. ", bugPattern.getName());
				continue;
			}
			
			exportDfa(bugLanguage, bugPattern.getShortenedName() + "BugLanguage.dot");
			// the language of invalid handshakes exhbiting this buggy behavior
			DtlsLanguageAdapter sutBugLanguage = bugPattern.generateBugLanguage()
					.intersect(sutLanguage)
					.minimize(); 
			
			
			if (!sutBugLanguage.isEmpty()) {
				detectedPatterns.add(bugPattern);
				LOGGER.info("sutBugLanguage not empty, finding witness");
				exportDfa(sutBugLanguage, "sut" + bugPattern.getShortenedName() + "BugLanguage.dot");
				if (config.isVerifyBugs()) {
					tracker.startVerification(bugPattern);
					TlsFlow witness = witnessFinder.findWitness(sutBugLanguage);
					tracker.endVerification(bugPattern);
					if (witness != null) {
						ModelBug bug = new ModelBug(witness, Arrays.asList(bugPattern));
						bug.verificationSuccessful();
						bugs.add(bug);
						LOGGER.info("Found valid witness {}", witness.toCompactString());
					} else {
						// could not verify bug
						TlsFlow counterexample = witnessFinder.findCounterexample(sutBugLanguage);
						TlsFlow modelFlow = TlsFlow.buildTlsFlow(model, counterexample.getInputWord(), true);
						ModelBug bug = new ModelBug(modelFlow, Arrays.asList(bugPattern));
						bug.verificationFailed(counterexample);
						bugs.add(bug);
						LOGGER.info("Could not find valid witness, giving counterexample {}", counterexample.toCompactString());
					}
				} else {
					TlsFlow witness = witnessFinder.findWitness(sutBugLanguage);
					ModelBug bug = new ModelBug(witness, Arrays.asList(bugPattern));
					bugs.add(bug);
					LOGGER.info("Found witness {}", witness.toCompactString());
				}
			}
		}

		// check for the existence of unidentified handshake bugs
		DtlsLanguageAdapter spec = patterns.getSpecificationLanguage();
		handleUncategorized(sutLanguage, spec, detectedPatterns, bugs);
	}
	
	private void handleUncategorized(DtlsLanguageAdapter sutLanguage, DtlsLanguageAdapter specLanguage, Collection<BugPattern> bugPatterns, List<Bug> bugs) {
		exportDfa(specLanguage, "specificationLanguage.dot");
		LOGGER.info("Generating deviant traces and checking whether they are categorized");
		DtlsLanguageAdapter specBugLanguage = specLanguage.complement().minimize();
		DtlsLanguageAdapter sutSpecBugLanguage = sutLanguage.intersect(specBugLanguage).minimize();
		
		int flows = 0;
		SequenceGenerator<MessageLabel> sequenceGenerator = SequenceGeneratorFactory.buildGenerator(GenerationStrategy.DEVIANT, null, specLanguage.getDfaModel());
		for (Word<MessageLabel> sequence : sequenceGenerator.generateSequences(sutSpecBugLanguage.getDfaModel(), mapping.getIOLabels())) {
			if (!bugPatterns.stream().anyMatch(bp -> bp.generateBugLanguage().accepts(sequence.asList()))) {
				if (specLanguage.accepts(sequence.asList())) {
					throw new InternalError("Accepting sequence in uncategorized bug language accepted by specification");
				}
				
				Word<TlsInput> inputWord = mapping.toTlsInputWord(sequence);
				TlsFlow flow = TlsFlow.buildTlsFlow(model, inputWord, true);
				ModelBug bug = new ModelBug(flow, BugPattern.uncategorized());
				bugs.add(bug);
				flows ++;
				if (flows >= config.getUncategorizedBugBound()) {
					break;
				} 				
			}
		}
	}

	private void exportDfa(DtlsLanguageAdapter spec, String name) {
		try {
			spec.export(new FileWriter(new File(outputDir, name)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
