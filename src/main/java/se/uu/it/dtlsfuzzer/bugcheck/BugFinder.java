package se.uu.it.dtlsfuzzer.bugcheck;

import java.util.List;

public interface BugFinder {
	public void findBugs(BugCheckerContext context, List<Bug> bugs);
}
