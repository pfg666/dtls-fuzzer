package se.uu.it.dtlsfuzzer.bugcheck;

import java.util.List;

public class BugFinderResult {
	private List<Bug> bugs;
	public BugFinderResult(List<Bug> bugs) {
		this.bugs = bugs;
	}
	
	public List<Bug> getBugs() {
		return bugs;
	}
}
