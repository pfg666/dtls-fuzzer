package se.uu.it.dtlsfuzzer.bugcheck;

import se.uu.it.dtlsfuzzer.specification.TlsFlow;

public abstract class Bug {
	private static int BUG_COUNT;
	
	private static Integer getFreshBugId() {
		return ++BUG_COUNT;
	}
	
	private TlsFlow flow;
	private Integer id;
	private BugSeverity severity;
	private BugVerificationStatus status;
	
	public Bug(TlsFlow flow) {
		this.flow = flow;
		id = getFreshBugId();
		severity = getDefaultSeverity();
		status = getDefaultStatus();
	}
	
	public abstract String getDescription();
	
	public final BugSeverity getSeverity() {
		return severity;
	}
	
	protected void setSeverity(BugSeverity severity) {
		this.severity = severity;
	}
	
	public BugVerificationStatus getStatus() {
		return status;
	}

	protected void setStatus(BugVerificationStatus status) {
		this.status = status;
	}
	
	public abstract BugSeverity getDefaultSeverity();
	
	public abstract BugVerificationStatus getDefaultStatus();
	
	public abstract BugType getType();
	
	/**
	 * Returns the subtype of a bug, if such a subtype exists.
	 * Otherwise returns null.
	 */
	public String getSubType() {
		return null;
	}
	
	public final Integer getId() {
		return id;
	}
	
	protected TlsFlow getFlow() {
		return flow;
	}
	
}
