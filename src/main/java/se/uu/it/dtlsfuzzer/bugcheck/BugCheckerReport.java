package se.uu.it.dtlsfuzzer.bugcheck;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import se.uu.it.dtlsfuzzer.ExportableResult;

public class BugCheckerReport extends ExportableResult{
	
	private List<Bug> bugs;
	// the types of bugs checked for
	private List<BugType> checkedBugTypes;
	private Statistics statistics;
	
	public BugCheckerReport() {
		bugs = new LinkedList<>();
		checkedBugTypes = Arrays.asList(BugType.values());
	}
	
	/**
	 * Sets the Bug Types for which checking was performed.
	 */
	public void setCheckedBugTypes(Collection<BugType> checkedBugTypes) {
		this.checkedBugTypes = new LinkedList<>(checkedBugTypes);
	}
	
	public void addBugs(Collection<Bug> bugs) {
		this.bugs.addAll(bugs);
	}
	
	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}
	
	public List<Bug> getBugs() {
		return bugs;
	}
	
	
	protected void doExport(PrintWriter pw) {
		title("Bug Report", pw);
		pw.println("Total Number of Bugs Found: " + bugs.size());
//		pw.println("Distribution by Type");
//		for ( BugType type : checkedBugTypes) {
//			long numBugs = bugs.stream().filter(b -> b.getType().equals(type)).count();
//			pw.println("Bugs of Type " + type.name() + " : " + numBugs);
//		}
//		pw.println();
//		pw.println("Distribution by Severity:");
//		for ( BugSeverity severity : BugSeverity.values()) {
//			long numBugs = bugs.stream().filter(b -> b.getSeverity().equals(severity)).count();
//			pw.println("Bugs of Severity " + severity.name() + " : " + numBugs);
//		}
		if (!bugs.isEmpty()) {
			section("Listing Bugs", pw);
			for (Bug bug : bugs) {
				pw.println("Bug Id: " + bug.getId());
				pw.println(bug.getDescription());
			}
			
//			section("Listing Bugs Ordered by Severity", pw);
//			Collections.sort(bugs, Comparator.<Bug,BugSeverity>comparing( b -> b.getSeverity(), Comparator.reverseOrder() ) );
//			BugSeverity crtSeverity = null;
//			for (Bug bug : bugs) {
//				if (!bug.getSeverity().equals(crtSeverity)) {
//					pw.println("Bugs of Severity " + bug.getSeverity() + " : ");
//					crtSeverity = bug.getSeverity();
//				}
//				pw.println("Bug Id: " + bug.getId());
//				pw.println(bug.getDescription());
//			}
//			
//			Collections.sort(bugs, Comparator.<Bug,BugType>comparing( b -> b.getType() ));
//			section("Listing Bugs Ordered by Type", pw);
//			BugType crtType = null;
//			for (Bug bug : bugs) {
//				if (!bug.getType().equals(crtType)) {
//					pw.println("Bugs of Type " + bug.getType() + " : ");
//					crtType = bug.getType();
//				}
//				pw.println("Bug Id: " + bug.getId());
//				pw.println(bug.getDescription());
//			}
		} else {
			section("No Bugs were Found", pw);
		}
		statistics.export(pw);
	}
}
