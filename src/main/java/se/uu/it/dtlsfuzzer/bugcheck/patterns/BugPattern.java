package se.uu.it.dtlsfuzzer.bugcheck.patterns;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.NotImplementedException;

import se.uu.it.dtlsfuzzer.bugcheck.BugSeverity;
import se.uu.it.dtlsfuzzer.specification.DtlsLanguageAdapter;

public abstract class BugPattern {
	private static int CUR_ID = 1;
	private static BugPattern UNCATEGORIZED;
	public static BugPattern uncategorized() {
		if (UNCATEGORIZED == null) {
			BugPattern bp = new BugPattern() {
				@Override
				DtlsLanguageAdapter doGenerateBugLanguage() {
					throw new NotImplementedException("Uncategorized bug pattern does not provide a bug language");
				}
				
			};
			bp.id = 0;
			bp.name = "Uncategorized";
			bp.description = "Uncategorized behavior which does not conform to the specification.";
			bp.severity = BugSeverity.UNKNOWN;
			UNCATEGORIZED = bp;
		}
		return UNCATEGORIZED;
	}
	
	public BugPattern() {
		id = CUR_ID ++;
	}
	
	@XmlTransient
	private int id;
	
	//@XmlAttribute(name = "name", required = false)
	@XmlElement(name="name", required = true)
	private String name;
	@XmlElement(name = "description", required = true)
	private String description;
	@XmlElement(name = "severity", required = false)
	private BugSeverity severity;
	
	@XmlTransient
	private DtlsLanguageAdapter bugLanguage;
	
	/**
	 * Generates the bug language described by this bug pattern. 
	 * On first call, it stores the generated language to a variable, allowing for subsequent calls to simply return the variable.
	 */
	public DtlsLanguageAdapter generateBugLanguage() {
		if (bugLanguage == null) {
			bugLanguage = doGenerateBugLanguage();
		}
		return bugLanguage;
	}
	
	abstract DtlsLanguageAdapter doGenerateBugLanguage();
	
	public Integer getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getShortenedName() {
		return name.replaceAll("\\s", "");
	}
	
	public String getDescription() {
		return description;
	}
	public BugSeverity getSeverity() {
		return severity;
	}
	
	void setSeverity(BugSeverity severity) {
		this.severity = severity;
	}
}
