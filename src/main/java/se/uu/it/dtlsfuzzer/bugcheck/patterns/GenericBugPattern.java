package se.uu.it.dtlsfuzzer.bugcheck.patterns;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import se.uu.it.dtlsfuzzer.specification.DtlsLanguageAdapter;

public class GenericBugPattern extends BugPattern {

	@XmlElement(name="bugLanguage", required = true)
	private String bugLanguagePath;
	
	@XmlTransient
	private DtlsLanguageAdapter bugLanguage;
	
	@Override
	DtlsLanguageAdapter doGenerateBugLanguage() {
		return bugLanguage;
	}
	
	public String getBugLanguagePath() {
		return bugLanguagePath;
	}
	
	void setBugLanguage(DtlsLanguageAdapter bugLanguage) {
		this.bugLanguage = bugLanguage;
	}

	public DtlsLanguageAdapter getBugLanguage() {
		return bugLanguage;
	}
}
