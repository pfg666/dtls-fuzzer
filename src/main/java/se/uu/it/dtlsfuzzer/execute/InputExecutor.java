package se.uu.it.dtlsfuzzer.execute;

import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

public interface InputExecutor {
	public TlsOutput execute(TlsInput input, State state, ExecutionContext context);
}
