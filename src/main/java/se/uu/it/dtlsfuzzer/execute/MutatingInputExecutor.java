package se.uu.it.dtlsfuzzer.execute;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.config.MapperConfig;
import se.uu.it.dtlsfuzzer.mutate.Mutation;
import se.uu.it.dtlsfuzzer.mutate.Mutator;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

/**
 * A mutating input executor applies mutators when sending a message at
 * different points. These are: (1) when a message is split into fragments (in
 * the case of DTLS); (2) when a message is packed into records. <br/>
 * Mutators at each point are called to generate mutations, which are applied in
 * a chained fashion on the fragmentation/packing result. The mutations
 * generated/applied for the last execution can be retrieved.
 */
public class MutatingInputExecutor extends PhasedInputExecutor {
	private static final Logger LOGGER = LogManager
			.getLogger(MutatingInputExecutor.class);
	private List<Mutator> mutators;
	private List<Mutation> appliedMutations;

	private MutatingInputExecutor(MapperConfig config, Mutator ...mutators) {
		this(config, Arrays.asList(mutators));
	}

	public MutatingInputExecutor(MapperConfig config, List<Mutator> mutators) {
		super(config);
		this.mutators = mutators;
	}
	
	protected TlsOutput doExecute(TlsInput input, State state, ExecutionContext context) {
		appliedMutations = new LinkedList<>();
		return super.doExecute(input, state, context);
	}
	
	protected void executePhase(Phase currentPhase, ProcessingUnit unit, State state, ExecutionContext context) {
		for (Mutator mutator : mutators) {
			if (Arrays.asList(mutator.getMutationType().getPhases()).contains(currentPhase)) {
				Mutation mutation = mutator.generateMutation(unit, state.getTlsContext(), context);
				mutation.mutate(currentPhase, unit, state, context);
			}
		}
	}

	public List<Mutation> getLastAppliedMutations() {
		return appliedMutations;
	}
}
