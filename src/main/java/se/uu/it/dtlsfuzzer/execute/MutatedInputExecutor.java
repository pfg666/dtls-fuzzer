package se.uu.it.dtlsfuzzer.execute;

import java.util.Arrays;
import java.util.List;

import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.config.MapperConfig;
import se.uu.it.dtlsfuzzer.mutate.Mutation;

/**
 * A mutated input executor applies mutations at different stages in sending an
 * input. These stages coincide with those of the {@link MutatingInputExecutor}.
 * 
 * It server as a "replayer" for already generated mutations which produced
 * interesting results.
 */
public class MutatedInputExecutor extends PhasedInputExecutor {

	
	private List<Mutation> mutations;
	
	public MutatedInputExecutor(MapperConfig config, Mutation ...mutations) {
		this(config, Arrays.asList(mutations));
	}

	public MutatedInputExecutor(MapperConfig config, List<Mutation> mutations) {
		super(config);
		this.mutations = mutations;
	}
	
	protected void executePhase(Phase currentPhase, ProcessingUnit unit, State state, ExecutionContext context) {
		super.executePhase(currentPhase, unit, state, context);
		for (Mutation mutation : mutations) {
			if (Arrays.asList(mutation.getPhases()).contains(currentPhase)) {
				mutation.mutate(currentPhase, unit, state, context);
			}
		}
	}
}
