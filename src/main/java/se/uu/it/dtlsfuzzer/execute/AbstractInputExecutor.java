package se.uu.it.dtlsfuzzer.execute;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rub.nds.tlsattacker.core.protocol.message.ProtocolMessage;
import de.rub.nds.tlsattacker.core.state.State;
import se.uu.it.dtlsfuzzer.config.MapperConfig;
import se.uu.it.dtlsfuzzer.sut.input.TlsInput;
import se.uu.it.dtlsfuzzer.sut.output.OutputMapper;
import se.uu.it.dtlsfuzzer.sut.output.TlsOutput;

public abstract class AbstractInputExecutor implements InputExecutor{
	private static final Logger LOGGER = LogManager
			.getLogger(AbstractInputExecutor.class.getName());
	private OutputMapper outputMapper;
	
	public AbstractInputExecutor(MapperConfig config) {
		this.outputMapper = new OutputMapper(config); 
	}
	
	public final TlsOutput execute(TlsInput input, State state, ExecutionContext context) {
		LOGGER.info("Executing input {}", input.getName());
		context.getStepContext().setInput(input);
		if (context.isExecutionEnabled() && input.isEnabled(state, context)) {
			return doExecute(input, state, context);
		} else {
			return outputMapper.disabled(); 
		}
	}

	protected abstract TlsOutput doExecute(TlsInput input, State state, ExecutionContext context);
	
	/**
	 * Template method for executing an input. 
	 * Takes a message sender as parameter. 
	 * Input executors need not use this function, but they should always call the update calls on the input in the order suggested.  
	 */
	protected TlsOutput doExecute(TlsInput input, State state, ExecutionContext context, MessageSender sender) {
		List<ProtocolMessage> messages = input.generateMessages(state, context);
		input.preSendUpdate(state, context);
		for (ProtocolMessage message : messages) {
			LOGGER.info("Sending Message " + message.toCompactString());
			sender.sendMessage(message, state, context);
		}
		input.postSendUpdate(state, context);
		TlsOutput output = outputMapper.receiveOutput(state, context);
		input.postReceiveUpdate(output, state, context);
		return output;
	}
	
	protected OutputMapper getOutputMapper() {
		return outputMapper;
	}
	
	static interface MessageSender {
		void sendMessage(ProtocolMessage message, State state, ExecutionContext context);
	}
}
