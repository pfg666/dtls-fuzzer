readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
readonly BUGCHECK_DIR="$SCRIPT_DIR/output/bugcheck"
readonly COLLATOR_SCRIPT="$SCRIPT_DIR/experiments/scripts/collate_results.py"
readonly MAX_DURATION="4h"

if [ $# = 0 ]; then
    echo "Usage: run_bugchecker.sh config [extra_args]"
    echo "Where config is client|server|usenix-server|usenix-client|latest|jsse"
    exit 1
fi

config=$1
bugcheck_dir=$BUGCHECK_DIR 
if [[ $2 == "-d" ]]; then
    bugcheck_dir=$3
    shift; shift;
fi


global_extra_args=$2
piondtls_ver=('usenix' '1.5.2' '2.0.2')
scandium_ver=('2.0.0-M16' '2.3.0')
jsse_minor_ver_min="1"
jsse_minor_ver_max="9"
jsse_major_ver="11.0"

function launch() {
    args_file=$1
    jvm_args=$2
    #app_args=$3 
    echo "Executing bugchecker with $args_file"
    echo timeout $MAX_DURATION java $jvm_args -jar $SCRIPT_DIR/target/dtls-fuzzer.jar $args_file $global_extra_args
    timeout $MAX_DURATION java $jvm_args -jar $SCRIPT_DIR/target/dtls-fuzzer.jar $args_file $global_extra_args
}

function collateResults() {
    python3 $COLLATOR_SCRIPT -c -o $bugcheck_dir/"$config"_status.csv $bugcheck_dir
}

# Reasons for commenting experiments out:
#e = the associated learning experiment ended in error resulting in an uninformative hyp
#s = superset of the alphabet is used in a different learning experiment which terminated successfully (not due to learning timeout or error) 
#c = experiment uses unconventional alphabet configuration
#m = experiment missing

# Where nonfreshrandom and regular configurations yield the same results, we chose regular configurations
if [[ $config == server ]]; then
    launch args/ctinydtls/bugcheck_ctinydtls_ecdhe_cert_req
    launch args/ctinydtls/bugcheck_ctinydtls_psk
    launch args/gnutls/bugcheck_gnutls_all_cert_req
    launch args/jsse/bugcheck_jsse_ecdhe_cert_req
    launch args/mbedtls/bugcheck_mbedtls_all_cert_req
    launch args/openssl/bugcheck_openssl_all_cert_req
    for ver in "${piondtls_ver[@]}"; do
        launch args/piondtls/bugcheck_piondtls_ecdhe_cert_req "-Dpiondtls.version=$ver"
        launch args/piondtls/bugcheck_piondtls_psk "-Dpiondtls.version=$ver"
    done
    for ver in "${scandium_ver[@]}"; do
        launch args/scandium/bugcheck_scandium_ecdhe_cert_req "-Dscandium.version=$ver"
    #    launch args/scandium/bugcheck_scandium_ecdhe_cert_req_excl
        launch args/scandium/bugcheck_scandium_psk "-Dscandium.version=$ver"
    done
    launch args/wolfssl/bugcheck_wolfssl_psk
elif [[ $config == client ]]; then 
    launch args/ctinydtls/bugcheck_ctinydtls_client_ecdhe_cert
    #c launch args/ctinydtls/bugcheck_ctinydtls_client_ecdhe_cert_shorths
    launch args/ctinydtls/bugcheck_ctinydtls_client_psk
    #c launch args/ctinydtls/bugcheck_ctinydtls_client_psk_shorths
    
    #s launch args/gnutls/bugcheck_gnutls_client_dhe_ecdhe_rsa_cert
    launch args/gnutls/bugcheck_gnutls_client_dhe_ecdhe_rsa_cert_reneg
    #s launch args/gnutls/bugcheck_gnutls_client_psk
    launch args/gnutls/bugcheck_gnutls_client_psk_reneg

    launch args/jsse/bugcheck_jsse_client_ecdhe_cert

    launch args/openssl/bugcheck_openssl_client_dhe_ecdhe_rsa_cert
    launch args/openssl/bugcheck_openssl_client_dhe_ecdhe_rsa_cert_reneg
    #s launch args/openssl/bugcheck_openssl_client_ecdhe_cert
    launch args/openssl/bugcheck_openssl_client_ecdhe_cert_reneg
    #e launch args/openssl/bugcheck_openssl_client_psk_reneg
    launch args/openssl/bugcheck_openssl_client_psk

    # launch args/mbedtls/bugcheck_mbedtls_client_dhe_ecdhe_rsa_cert
    launch args/mbedtls/bugcheck_mbedtls_client_dhe_ecdhe_rsa_cert_reneg
    #launch args/mbedtls/bugcheck_mbedtls_client_dhe_ecdhe_rsa_cert_reneg_nofreshrandom
    #sm launch args/mbedtls/bugcheck_mbedtls_client_ecdhe_cert
    launch args/mbedtls/bugcheck_mbedtls_client_ecdhe_cert_reneg
    #s launch args/mbedtls/bugcheck_mbedtls_client_psk
    launch args/mbedtls/bugcheck_mbedtls_client_psk_reneg
    

    for ver in $piondtls_ver; do
        launch args/piondtls/bugcheck_piondtls_client_ecdhe_cert
        launch args/piondtls/bugcheck_piondtls_client_psk
    done

    for ver in $scandium_ver; do
        launch args/scandium/bugcheck_scandium_client_ecdhe_cert
        launch args/scandium/bugcheck_scandium_client_psk
    done
    launch args/wolfssl/bugcheck_wolfssl_client_psk
elif [[ $config == jsse ]]; then
    for i in $(seq $jsse_minor_ver_min $jsse_minor_ver_max); do #| xargs -i "
        launch args/jsse/bugcheck_jsse_ecdhe_cert_req "-Djsse.version=$jsse_major_ver.$i"
    done
elif [[ $config == usenix-server ]]; then
    launch args/ctinydtls/bugcheck_ctinydtls_ecdhe_cert_req
    launch args/ctinydtls/bugcheck_ctinydtls_psk
    launch args/etinydtls/bugcheck_etinydtls_ecdhe_cert_req
    launch args/etinydtls/bugcheck_etinydtls_psk
    launch args/gnutls/bugcheck_gnutls_all_cert_req "-Dgnutls.version=3.5.19"
    launch args/gnutls/bugcheck_gnutls_all_cert_req
    launch args/jsse/bugcheck_jsse_ecdhe_cert_req
    launch args/mbedtls/bugcheck_mbedtls_all_cert_req
    launch args/openssl/bugcheck_openssl_all_cert_req
    launch args/piondtls/bugcheck_piondtls_ecdhe_cert_req "-Dpiondtls.version=usenix"
    launch args/piondtls/bugcheck_piondtls_psk "-Dpiondtls.version=usenix"
    launch args/scandium/bugcheck_scandium_ecdhe_cert_req "-Dscandium.version=2.0.0-M16"
    launch args/scandium/bugcheck_scandium_psk "-Dscandium.version=2.0.0-M16"
elif [[ $config == usenix-client ]]; then
    launch args/ctinydtls/bugcheck_ctinydtls_client_ecdhe_cert
    launch args/ctinydtls/bugcheck_ctinydtls_client_psk
    launch args/etinydtls/bugcheck_etinydtls_client_ecdhe_cert
    launch args/etinydtls/bugcheck_etinydtls_client_psk
    launch args/gnutls/bugcheck_gnutls_client_dhe_ecdhe_rsa_cert_reneg
    launch args/gnutls/bugcheck_gnutls_client_psk_reneg
    launch args/jsse/bugcheck_jsse_client_ecdhe_cert
    launch args/openssl/bugcheck_openssl_client_dhe_ecdhe_rsa_cert
    launch args/openssl/bugcheck_openssl_client_dhe_ecdhe_rsa_cert_reneg
    launch args/openssl/bugcheck_openssl_client_ecdhe_cert_reneg
    launch args/openssl/bugcheck_openssl_client_psk
    launch args/mbedtls/bugcheck_mbedtls_client_dhe_ecdhe_rsa_cert
    launch args/mbedtls/bugcheck_mbedtls_client_dhe_ecdhe_rsa_cert_reneg_nofreshrandom
    launch args/mbedtls/bugcheck_mbedtls_client_ecdhe_cert_reneg
    launch args/mbedtls/bugcheck_mbedtls_client_psk_reneg
    launch args/piondtls/bugcheck_piondtls_client_ecdhe_cert  "-Dpiondtls.version=usenix"
    launch args/piondtls/bugcheck_piondtls_client_psk  "-Dpiondtls.version=usenix"
    launch args/scandium/bugcheck_scandium_client_ecdhe_cert "-Dscandium.version=2.0.0-M16"
    launch args/scandium/bugcheck_scandium_client_psk "-Dscandium.version=2.0.0-M16"
elif [[ $config == latest ]]; then
    # commented are configurations for which learning is under way or which will be executed locally due to the special changes to the arg 
    
    # servers
    #launch args/gnutls/bugcheck_gnutls_all_cert_req "-Dgnutls.version=3.7.1"
    #launch args/jsse/bugcheck_jsse_ecdhe_cert_req "-Djsse.version=16.0.1"
    launch args/mbedtls/bugcheck_mbedtls_all_cert_req "-Dmbedtls.version=2.26.0"
    launch args/openssl/bugcheck_openssl_all_cert_req "-Dopenssl.version=1.1.1k"
    #launch args/piondtls/bugcheck_piondtls_ecdhe_cert_req "-Dpiondtls.version=2.0.9" 
    #launch args/piondtls/bugcheck_piondtls_psk "-Dpiondtls.version=2.0.9"
    launch args/scandium/bugcheck_scandium_ecdhe_cert_req "-Dscandium.version=2.6.2"
    launch args/scandium/bugcheck_scandium_psk "-Dscandium.version=2.6.2"
    

    #launch args/gnutls/bugcheck_gnutls_client_dhe_ecdhe_rsa_cert_reneg "-Dgnutls.version=3.7.1"
    #launch args/gnutls/bugcheck_gnutls_client_psk_reneg "-Dgnutls.version=3.7.1"
    #launch args/jsse/bugcheck_jsse_client_ecdhe_cert "-Djsse.version=16.0.1"
    # launch args/openssl/bugcheck_openssl_client_dhe_ecdhe_rsa_cert "-Dopenssl.version=1.1.1k"
    launch args/openssl/bugcheck_openssl_client_dhe_ecdhe_rsa_cert_reneg "-Dopenssl.version=1.1.1k"
    launch args/openssl/bugcheck_openssl_client_dhe_ecdhe_rsa_cert_reneg_nofreshrandom "-Dopenssl.version=1.1.1k"
    launch args/openssl/bugcheck_openssl_client_ecdhe_cert_reneg "-Dopenssl.version=1.1.1k"
    launch args/openssl/bugcheck_openssl_client_ecdhe_cert_reneg_nofreshrandom "-Dopenssl.version=1.1.1k"
    launch args/openssl/bugcheck_openssl_client_psk_reneg "-Dopenssl.version=1.1.1k"
    launch args/openssl/bugcheck_openssl_client_psk_reneg_nofreshrandom "-Dopenssl.version=1.1.1k"
    
    launch args/mbedtls/bugcheck_mbedtls_client_dhe_ecdhe_rsa_cert_reneg "-Dmbedtls.version=2.26.0"
    launch args/mbedtls/bugcheck_mbedtls_client_psk_reneg "-Dmbedtls.version=2.26.0"
    #launch args/piondtls/bugcheck_piondtls_client_ecdhe_cert  "-Dpiondtls.version=2.0.9" "-key $SCRIPT_DIR/experiments/keystore/piondtls_ec_secp256r1_key.pem -cert $SCRIPT_DIR/experiments/keystore/piondtls_ec_secp256r1_cert.pem"
    #launch args/piondtls/bugcheck_piondtls_client_psk  "-Dpiondtls.version=2.0.9"
    launch args/scandium/bugcheck_scandium_client_ecdhe_cert "-Dscandium.version=2.6.2"
    launch args/scandium/bugcheck_scandium_client_psk "-Dscandium.version=2.6.2"

#elif [[ $config == latest-client ]]; then

else
    
    echo "Invalid config $config"
fi

collateResults