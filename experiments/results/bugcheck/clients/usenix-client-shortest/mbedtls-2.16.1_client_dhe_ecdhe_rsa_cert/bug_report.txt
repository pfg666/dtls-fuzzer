
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 2

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Handshake completed with invalid message sequence
Severity: LOW
Description: The handshake was completed using invalid message sequence numbers.
Flow: HELLO_VERIFY_REQUEST/CLIENT_HELLO+ HELLO_VERIFY_REQUEST/CLIENT_HELLO RSA_SERVER_HELLO/TIMEOUT CERTIFICATE/TIMEOUT SERVER_HELLO_DONE/RSA_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Incorrect CertificateRequest Response
Severity: LOW
Description: Responding to a CertificateRequest with the certificate of incompatible type.
Flow: RSA_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT RSA_FIXED_ECDH_CERTIFICATE_REQUEST/TIMEOUT SERVER_HELLO_DONE/RSA_CERTIFICATE|RSA_CLIENT_KEY_EXCHANGE|CERTIFICATE_VERIFY|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, ECDH_SERVER_HELLO, ECDH_SERVER_KEY_EXCHANGE, DH_SERVER_HELLO, DH_SERVER_KEY_EXCHANGE, RSA_SERVER_HELLO, RSA_SIGN_CERTIFICATE_REQUEST, RSA_FIXED_ECDH_CERTIFICATE_REQUEST, RSA_FIXED_DH_CERTIFICATE_REQUEST, DSS_SIGN_CERTIFICATE_REQUEST, DSS_FIXED_DH_CERTIFICATE_REQUEST, ECDSA_SIGN_CERTIFICATE_REQUEST, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, CERTIFICATE, EMPTY_CERTIFICATE, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (19): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Unrequested Certificate, Incorrect CertificateRequest Response]
Bug Verification Enabled: true
Uncategorized Bug Bound: 10

--------------------------------------------------------------------------------
TLS SUL Parameters
--------------------------------------------------------------------------------

Protocol: DTLS12
ResetWait: 0
Timeout: 100
RunWait: 0
Command: /home/pfg666/GitHub/dtls-fuzzer/suts/mbedtls-2.16.1/programs/ssl/ssl_client2 dtls=1 psk=1234 mtu=5000 key_file=/home/pfg666/GitHub/dtls-fuzzer/experiments/keystore/rsa2048_key.pem crt_file=/home/pfg666/GitHub/dtls-fuzzer/experiments/keystore/rsa2048_cert.pem server_port=36358 exchanges=100 hs_timeout=20000-120000 ca_file=/home/pfg666/GitHub/dtls-fuzzer/experiments/keystore/rsa2048_cert.pem auth_mode=optional renegotiation=1

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of inputs: 9
Number of resets: 2
Number of bugs: 2
Time bug-checking took (ms): 2762

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (19): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Unrequested Certificate, Incorrect CertificateRequest Response]
Bug patterns found (2): [Handshake completed with invalid message sequence, Incorrect CertificateRequest Response]
Bug patterns verified successfully (2): [Handshake completed with invalid message sequence, Incorrect CertificateRequest Response]
Verification Inputs per Bug Pattern
   Handshake completed with invalid message sequence : 5
   Incorrect CertificateRequest Response : 4

Verification Resets per Bug Pattern
   Handshake completed with invalid message sequence : 1
   Incorrect CertificateRequest Response : 1

