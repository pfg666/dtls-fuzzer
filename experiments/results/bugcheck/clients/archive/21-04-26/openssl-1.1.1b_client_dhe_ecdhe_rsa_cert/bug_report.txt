
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 5

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Continue After CloseNotify
Severity: LOW
Description: Allowing handshake to continue after a CloseNotify alert.
Flow: RSA_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT SERVER_HELLO_DONE/RSA_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT APPLICATION/TIMEOUT Alert(WARNING,CLOSE_NOTIFY)/TIMEOUT FINISHED/APPLICATION|Alert(WARNING,CLOSE_NOTIFY) 
Verification Status: NOT_VERIFIED

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Continue After Fatal Alert
Severity: LOW
Description: Allowing handshake to continue after a fatal alert.
Flow: RSA_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT SERVER_HELLO_DONE/RSA_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT APPLICATION/TIMEOUT Alert(FATAL,UNEXPECTED_MESSAGE)/TIMEOUT FINISHED/APPLICATION 
Verification Status: NOT_VERIFIED

Bug Id: 3
Detected the following bug patterns in flow
Pattern: Handshake completed with invalid message sequence
Severity: LOW
Description: The handshake was completed using invalid message sequence numbers.
Flow: RSA_SERVER_HELLO/CLIENT_HELLO HELLO_VERIFY_REQUEST/CLIENT_HELLO RSA_SERVER_HELLO/TIMEOUT CERTIFICATE/TIMEOUT SERVER_HELLO_DONE/RSA_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 4
Detected the following bug patterns in flow
Pattern: Switching Cipher Suite
Severity: LOW
Description: Switching cipher suite using a second ServerHello
Flow: ECDH_SERVER_HELLO/CLIENT_HELLO HELLO_VERIFY_REQUEST/CLIENT_HELLO RSA_SERVER_HELLO/TIMEOUT CERTIFICATE/TIMEOUT SERVER_HELLO_DONE/RSA_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 5
Detected the following bug patterns in flow
Pattern: Incorrect CertificateRequest Response
Severity: LOW
Description: Responding to a CertificateRequest with the certificate of incompatible type.
Flow: RSA_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT RSA_FIXED_ECDH_CERTIFICATE_REQUEST/TIMEOUT SERVER_HELLO_DONE/RSA_CERTIFICATE|RSA_CLIENT_KEY_EXCHANGE|CERTIFICATE_VERIFY|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, ECDH_SERVER_HELLO, ECDH_SERVER_KEY_EXCHANGE, DH_SERVER_HELLO, DH_SERVER_KEY_EXCHANGE, RSA_SERVER_HELLO, RSA_SIGN_CERTIFICATE_REQUEST, RSA_FIXED_ECDH_CERTIFICATE_REQUEST, RSA_FIXED_DH_CERTIFICATE_REQUEST, DSS_SIGN_CERTIFICATE_REQUEST, DSS_FIXED_DH_CERTIFICATE_REQUEST, ECDSA_SIGN_CERTIFICATE_REQUEST, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, CERTIFICATE, EMPTY_CERTIFICATE, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (16): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug Verification Enabled: false
Uncategorized Bug Bound: 10

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of bugs: 5
Time bug-checking took (ms): 1456

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (16): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug patterns found (5): [Continue After CloseNotify, Continue After Fatal Alert, Handshake completed with invalid message sequence, Switching Cipher Suite, Incorrect CertificateRequest Response]
Bug patterns verified successfully (0): []
