
================================================================================
Bug Report
================================================================================


--------------------------------------------------------------------------------
Overall Statistics
--------------------------------------------------------------------------------

Total Number of Bugs Found: 0
Distribution by Type
Bugs of Type MODEL : 0
Bugs of Type UNENCRYPTED_MESSAGE_PROCESSING : 0
Bugs of Type INVALID_EPOCH : 0
Bugs of Type MISHANDLED_REORDERING : 0

Distribution by Severity:
Bugs of Severity UNKNOWN : 0
Bugs of Severity VERY_LOW : 0
Bugs of Severity LOW : 0
Bugs of Severity MEDIUM : 0
Bugs of Severity HIGH : 0
Bugs of Severity VERY_HIGH : 0

--------------------------------------------------------------------------------
No Bugs were Found
--------------------------------------------------------------------------------


================================================================================
Handshake Finder Result
================================================================================


--------------------------------------------------------------------------------
Overall Statistics
--------------------------------------------------------------------------------

Exploration Inputs: [HELLO_VERIFY_REQUEST, PSK_SERVER_HELLO, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED]
Expected Handshake Flows: 2
Alternative Handshake Flows: 0

--------------------------------------------------------------------------------
Expected Handshake Flow Listing
--------------------------------------------------------------------------------

Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT 
Flow: HELLO_VERIFY_REQUEST/CLIENT_HELLO+ PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT 

--------------------------------------------------------------------------------
No Alternative Handshake Flows were found
--------------------------------------------------------------------------------

