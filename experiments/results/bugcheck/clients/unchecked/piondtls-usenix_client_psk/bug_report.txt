
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 6

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Continue After Fatal Alert
Severity: LOW
Description: Allowing handshake to continue after a fatal alert.
Flow: HELLO_VERIFY_REQUEST/CLIENT_HELLO+ HELLO_VERIFY_REQUEST/Alert(FATAL,HANDSHAKE_FAILURE) PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Early Finished
Severity: HIGH
Description: Handshake is missing ChangeCipherSpec.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO+ SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED FINISHED/TIMEOUT APPLICATION/APPLICATION 
Verification Status: NOT_VERIFIED

Bug Id: 3
Detected the following bug patterns in flow
Pattern: Finished Before CCS
Severity: HIGH
Description: Finished sent before ChangeCipherSpec
Flow: PSK_SERVER_HELLO/CLIENT_HELLO+ SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED FINISHED/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT APPLICATION/APPLICATION 
Verification Status: NOT_VERIFIED

Bug Id: 4
Detected the following bug patterns in flow
Pattern: Handshake completed with invalid message sequence
Severity: LOW
Description: The handshake was completed using invalid message sequence numbers.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO+ PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 5
Detected the following bug patterns in flow
Pattern: ServerHello Flight Restart
Severity: LOW
Description: ServerHello flight restarted after a ServerHelloDone
Flow: SERVER_HELLO_DONE/CLIENT_HELLO PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED FINISHED/TIMEOUT APPLICATION/APPLICATION 
Verification Status: NOT_VERIFIED

Bug Id: 6
Detected the following bug patterns in flow
Pattern: Unexpected ClientHello Response
Severity: LOW
Description: ClientHello in response to an input for which no ClientHello is expected.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO+ 
Verification Status: NOT_VERIFIED


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, PSK_SERVER_HELLO, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (18): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug Verification Enabled: false
Uncategorized Bug Bound: 10

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of bugs: 6
Time bug-checking took (ms): 721

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (18): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug patterns found (6): [Continue After Fatal Alert, Early Finished, Finished Before CCS, Handshake completed with invalid message sequence, ServerHello Flight Restart, Unexpected ClientHello Response]
Bug patterns verified successfully (0): []
