
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 1

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Incorrect CertificateRequest Response
Severity: LOW
Description: Responding to a CertificateRequest with the certificate of incompatible type.
Flow: ECDH_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT RSA_SIGN_CERTIFICATE_REQUEST/TIMEOUT SERVER_HELLO_DONE/ECDSA_CERTIFICATE|ECDH_CLIENT_KEY_EXCHANGE|CERTIFICATE_VERIFY|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, ECDH_SERVER_HELLO, ECDH_SERVER_KEY_EXCHANGE, RSA_SIGN_CERTIFICATE_REQUEST, RSA_FIXED_ECDH_CERTIFICATE_REQUEST, RSA_FIXED_DH_CERTIFICATE_REQUEST, DSS_SIGN_CERTIFICATE_REQUEST, DSS_FIXED_DH_CERTIFICATE_REQUEST, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, CERTIFICATE, EMPTY_CERTIFICATE, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (18): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug Verification Enabled: false
Uncategorized Bug Bound: 10

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of bugs: 1
Time bug-checking took (ms): 920

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (18): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug patterns found (1): [Incorrect CertificateRequest Response]
Bug patterns verified successfully (0): []
