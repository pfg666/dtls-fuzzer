
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 10

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Continue After Fatal Alert
Severity: LOW
Description: Allowing handshake to continue after a fatal alert.
Flow: HELLO_VERIFY_REQUEST/CLIENT_HELLO+ HELLO_VERIFY_REQUEST/Alert(FATAL,HANDSHAKE_FAILURE) ECDH_SERVER_HELLO/TIMEOUT CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT SERVER_HELLO_DONE/ECDH_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Finished Before CCS
Severity: HIGH
Description: Finished sent before ChangeCipherSpec
Flow: ECDH_SERVER_HELLO/CLIENT_HELLO+ CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT SERVER_HELLO_DONE/ECDH_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED FINISHED/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT APPLICATION/APPLICATION 
Verification Status: NOT_VERIFIED

Bug Id: 3
Detected the following bug patterns in flow
Pattern: Handshake completed with invalid message sequence
Severity: LOW
Description: The handshake was completed using invalid message sequence numbers.
Flow: ECDH_SERVER_HELLO/CLIENT_HELLO+ ECDH_SERVER_HELLO/TIMEOUT CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT SERVER_HELLO_DONE/ECDH_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 4
Detected the following bug patterns in flow
Pattern: Multiple Certificate
Severity: LOW
Description: Handshake contains repeated Certificate records.
Flow: CERTIFICATE/CLIENT_HELLO ECDH_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT SERVER_HELLO_DONE/ECDH_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 5
Detected the following bug patterns in flow
Pattern: Multiple CertificateRequest
Severity: LOW
Description: Handshake contains repeated CertificateRequest records.
Flow: RSA_SIGN_CERTIFICATE_REQUEST/CLIENT_HELLO ECDH_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT RSA_SIGN_CERTIFICATE_REQUEST/TIMEOUT SERVER_HELLO_DONE/ECDSA_CERTIFICATE|ECDH_CLIENT_KEY_EXCHANGE|CERTIFICATE_VERIFY|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 6
Detected the following bug patterns in flow
Pattern: Multiple ServerKeyExchange
Severity: LOW
Description: Handshake contains repeated ServerKeyExchange records.
Flow: ECDH_SERVER_KEY_EXCHANGE/CLIENT_HELLO ECDH_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT SERVER_HELLO_DONE/ECDH_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 7
Detected the following bug patterns in flow
Pattern: Reorder Cert CertReq
Severity: LOW
Description: Sending CertReq before Cert
Flow: RSA_SIGN_CERTIFICATE_REQUEST/CLIENT_HELLO ECDH_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT RSA_SIGN_CERTIFICATE_REQUEST/TIMEOUT SERVER_HELLO_DONE/ECDSA_CERTIFICATE|ECDH_CLIENT_KEY_EXCHANGE|CERTIFICATE_VERIFY|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 8
Detected the following bug patterns in flow
Pattern: ServerHello Flight Restart
Severity: LOW
Description: ServerHello flight restarted after a ServerHelloDone
Flow: SERVER_HELLO_DONE/CLIENT_HELLO ECDH_SERVER_HELLO/CLIENT_HELLO CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT SERVER_HELLO_DONE/ECDH_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT APPLICATION/APPLICATION 
Verification Status: NOT_VERIFIED

Bug Id: 9
Detected the following bug patterns in flow
Pattern: Unexpected ClientHello Response
Severity: LOW
Description: ClientHello in response to an input for which no ClientHello is expected.
Flow: ECDH_SERVER_HELLO/CLIENT_HELLO+ 
Verification Status: NOT_VERIFIED

Bug Id: 10
Detected the following bug patterns in flow
Pattern: Incorrect CertificateRequest Response
Severity: LOW
Description: Responding to a CertificateRequest with the certificate of incompatible type.
Flow: ECDH_SERVER_HELLO/CLIENT_HELLO+ CERTIFICATE/TIMEOUT ECDH_SERVER_KEY_EXCHANGE/TIMEOUT RSA_SIGN_CERTIFICATE_REQUEST/TIMEOUT SERVER_HELLO_DONE/ECDSA_CERTIFICATE|ECDH_CLIENT_KEY_EXCHANGE|CERTIFICATE_VERIFY|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, ECDH_SERVER_HELLO, ECDH_SERVER_KEY_EXCHANGE, RSA_SIGN_CERTIFICATE_REQUEST, RSA_FIXED_ECDH_CERTIFICATE_REQUEST, RSA_FIXED_DH_CERTIFICATE_REQUEST, DSS_SIGN_CERTIFICATE_REQUEST, DSS_FIXED_DH_CERTIFICATE_REQUEST, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, CERTIFICATE, EMPTY_CERTIFICATE, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (18): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug Verification Enabled: false
Uncategorized Bug Bound: 10

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of bugs: 10
Time bug-checking took (ms): 1270

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (18): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug patterns found (10): [Continue After Fatal Alert, Finished Before CCS, Handshake completed with invalid message sequence, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, Reorder Cert CertReq, ServerHello Flight Restart, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug patterns verified successfully (0): []
