
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 3

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Continue After CloseNotify
Severity: LOW
Description: Allowing handshake to continue after a CloseNotify alert.
Flow: Alert(WARNING,CLOSE_NOTIFY)/CLIENT_HELLO HELLO_VERIFY_REQUEST/CLIENT_HELLO 
Verification Status: NOT_VERIFIED

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Continue After Fatal Alert
Severity: LOW
Description: Allowing handshake to continue after a fatal alert.
Flow: Alert(FATAL,UNEXPECTED_MESSAGE)/CLIENT_HELLO HELLO_VERIFY_REQUEST/CLIENT_HELLO 
Verification Status: NOT_VERIFIED

Bug Id: 3
Detected the following bug patterns in flow
Pattern: Handshake completed with invalid message sequence
Severity: LOW
Description: The handshake was completed using invalid message sequence numbers.
Flow: HELLO_VERIFY_REQUEST/CLIENT_HELLO+ HELLO_VERIFY_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, PSK_SERVER_HELLO, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, HELLO_REQUEST, APPLICATION, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (18): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug Verification Enabled: false
Uncategorized Bug Bound: 10

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of bugs: 3
Time bug-checking took (ms): 674

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (18): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Incorrect CertificateRequest Response]
Bug patterns found (3): [Continue After CloseNotify, Continue After Fatal Alert, Handshake completed with invalid message sequence]
Bug patterns verified successfully (0): []
