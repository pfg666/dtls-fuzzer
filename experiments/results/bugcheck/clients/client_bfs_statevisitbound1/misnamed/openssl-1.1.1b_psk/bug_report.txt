
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 3

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Continue After CloseNotify
Severity: LOW
Description: Allowing handshake to continue after a CloseNotify alert.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT APPLICATION/TIMEOUT Alert(WARNING,CLOSE_NOTIFY)/TIMEOUT FINISHED/APPLICATION|Alert(WARNING,CLOSE_NOTIFY) 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Continue After Fatal Alert
Severity: LOW
Description: Allowing handshake to continue after a fatal alert.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT APPLICATION/TIMEOUT Alert(FATAL,UNEXPECTED_MESSAGE)/TIMEOUT FINISHED/APPLICATION 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 3
Detected the following bug patterns in flow
Pattern: Handshake completed with invalid message sequence
Severity: LOW
Description: The handshake was completed using invalid message sequence numbers.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO HELLO_VERIFY_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, PSK_SERVER_HELLO, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (19): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Unrequested Certificate, Incorrect CertificateRequest Response]
Bug Verification Enabled: true
Uncategorized Bug Bound: 10

--------------------------------------------------------------------------------
TLS SUL Parameters
--------------------------------------------------------------------------------

Protocol: DTLS12
ResetWait: 0
Timeout: 20
RunWait: 0
Command: /home/pfg666/GitHub/dtls-fuzzer-bugcheck/suts/openssl-1.1.1b/apps/openssl s_client -psk 1234 -key /home/pfg666/GitHub/dtls-fuzzer-bugcheck/experiments/keystore/rsa2048_key.pem -cert /home/pfg666/GitHub/dtls-fuzzer-bugcheck/experiments/keystore/rsa2048_cert.pem -CAfile /home/pfg666/GitHub/dtls-fuzzer-bugcheck/experiments/keystore/rsa2048_cert.pem -port 12789 -dtls1_2 -mtu 5000

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of inputs: 16
Number of resets: 3
Number of bugs: 3
Time bug-checking took (ms): 2679

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (19): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Unrequested Certificate, Incorrect CertificateRequest Response]
Bug patterns found (3): [Continue After CloseNotify, Continue After Fatal Alert, Handshake completed with invalid message sequence]
Bug patterns verified successfully (3): [Continue After CloseNotify, Continue After Fatal Alert, Handshake completed with invalid message sequence]
Verification Inputs per Bug Pattern
   Continue After CloseNotify : 6
   Continue After Fatal Alert : 6
   Handshake completed with invalid message sequence : 4

Verification Resets per Bug Pattern
   Continue After CloseNotify : 1
   Continue After Fatal Alert : 1
   Handshake completed with invalid message sequence : 1

