
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 9

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Continue After CloseNotify
Severity: LOW
Description: Allowing handshake to continue after a CloseNotify alert.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT APPLICATION/TIMEOUT Alert(WARNING,CLOSE_NOTIFY)/TIMEOUT FINISHED/APPLICATION|Alert(WARNING,CLOSE_NOTIFY) 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Continue After Fatal Alert
Severity: LOW
Description: Allowing handshake to continue after a fatal alert.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT APPLICATION/TIMEOUT Alert(FATAL,UNEXPECTED_MESSAGE)/TIMEOUT FINISHED/APPLICATION 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 3
Detected the following bug patterns in flow
Pattern: Handshake completed with invalid message sequence
Severity: LOW
Description: The handshake was completed using invalid message sequence numbers.
Flow: HELLO_VERIFY_REQUEST/CLIENT_HELLO+ HELLO_VERIFY_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 4
Detected the following bug patterns in flow
Pattern: Unexpected ClientHello Response
Severity: LOW
Description: ClientHello in response to an input for which no ClientHello is expected.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT HELLO_VERIFY_REQUEST/TIMEOUT HELLO_REQUEST/CLIENT_HELLO HELLO_VERIFY_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/CLIENT_HELLO 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 5
Detected the following bug patterns in flow
Pattern: Uncategorized
Severity: UNKNOWN
Description: Uncategorized behavior which does not conform to the specification.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT SERVER_HELLO_DONE/TIMEOUT HELLO_REQUEST/CLIENT_HELLO HELLO_VERIFY_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: NOT_VERIFIED

Bug Id: 6
Detected the following bug patterns in flow
Pattern: Uncategorized
Severity: UNKNOWN
Description: Uncategorized behavior which does not conform to the specification.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/TIMEOUT HELLO_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/TIMEOUT HELLO_VERIFY_REQUEST/CLIENT_HELLO|PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|UNKNOWN_MESSAGE 
Verification Status: NOT_VERIFIED

Bug Id: 7
Detected the following bug patterns in flow
Pattern: Uncategorized
Severity: UNKNOWN
Description: Uncategorized behavior which does not conform to the specification.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT SERVER_HELLO_DONE/TIMEOUT HELLO_VERIFY_REQUEST/TIMEOUT HELLO_REQUEST/CLIENT_HELLO HELLO_VERIFY_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED|Alert(FATAL,UNEXPECTED_MESSAGE) 
Verification Status: NOT_VERIFIED

Bug Id: 8
Detected the following bug patterns in flow
Pattern: Uncategorized
Severity: UNKNOWN
Description: Uncategorized behavior which does not conform to the specification.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/TIMEOUT HELLO_VERIFY_REQUEST/TIMEOUT HELLO_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/TIMEOUT HELLO_VERIFY_REQUEST/CLIENT_HELLO|PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|UNKNOWN_MESSAGE 
Verification Status: NOT_VERIFIED

Bug Id: 9
Detected the following bug patterns in flow
Pattern: Uncategorized
Severity: UNKNOWN
Description: Uncategorized behavior which does not conform to the specification.
Flow: PSK_SERVER_HELLO/CLIENT_HELLO SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/TIMEOUT PSK_SERVER_HELLO/TIMEOUT HELLO_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/TIMEOUT HELLO_VERIFY_REQUEST/CLIENT_HELLO HELLO_VERIFY_REQUEST/TIMEOUT SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|UNKNOWN_MESSAGE 
Verification Status: NOT_VERIFIED


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, PSK_SERVER_HELLO, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, HELLO_REQUEST, APPLICATION, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (19): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Unrequested Certificate, Incorrect CertificateRequest Response]
Bug Verification Enabled: true
Uncategorized Bug Bound: 10

--------------------------------------------------------------------------------
TLS SUL Parameters
--------------------------------------------------------------------------------

Protocol: DTLS12
ResetWait: 0
Timeout: 20
RunWait: 0
Command: /home/pfg666/GitHub/dtls-fuzzer-bugcheck/suts/openssl-1.1.1k/apps/openssl s_client -psk 1234 -key /home/pfg666/GitHub/dtls-fuzzer-bugcheck/experiments/keystore/rsa2048_key.pem -cert /home/pfg666/GitHub/dtls-fuzzer-bugcheck/experiments/keystore/rsa2048_cert.pem -CAfile /home/pfg666/GitHub/dtls-fuzzer-bugcheck/experiments/keystore/rsa2048_cert.pem -port 20349 -dtls1_2 -mtu 5000

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of inputs: 24
Number of resets: 4
Number of bugs: 9
Time bug-checking took (ms): 8670

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (19): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Unrequested Certificate, Incorrect CertificateRequest Response]
Bug patterns found (5): [Uncategorized, Continue After CloseNotify, Continue After Fatal Alert, Handshake completed with invalid message sequence, Unexpected ClientHello Response]
Bug patterns verified successfully (4): [Continue After CloseNotify, Continue After Fatal Alert, Handshake completed with invalid message sequence, Unexpected ClientHello Response]
Verification Inputs per Bug Pattern
   Continue After CloseNotify : 6
   Continue After Fatal Alert : 6
   Handshake completed with invalid message sequence : 4
   Unexpected ClientHello Response : 8

Verification Resets per Bug Pattern
   Continue After CloseNotify : 1
   Continue After Fatal Alert : 1
   Handshake completed with invalid message sequence : 1
   Unexpected ClientHello Response : 1

