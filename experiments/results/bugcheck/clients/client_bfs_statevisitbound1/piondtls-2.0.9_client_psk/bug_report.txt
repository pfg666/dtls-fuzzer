
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 1

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Handshake completed with invalid message sequence
Severity: LOW
Description: The handshake was completed using invalid message sequence numbers.
Flow: HELLO_VERIFY_REQUEST/CLIENT_HELLO+ HELLO_VERIFY_REQUEST/CLIENT_HELLO PSK_SERVER_HELLO/TIMEOUT SERVER_HELLO_DONE/PSK_CLIENT_KEY_EXCHANGE|CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: client
Alphabet: [HELLO_VERIFY_REQUEST, PSK_SERVER_HELLO, SERVER_HELLO_DONE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (19): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Unrequested Certificate, Incorrect CertificateRequest Response]
Bug Verification Enabled: true
Uncategorized Bug Bound: 10

--------------------------------------------------------------------------------
TLS SUL Parameters
--------------------------------------------------------------------------------

Protocol: DTLS12
ResetWait: 100
Timeout: 100
RunWait: 0
Command: /home/pfg666/GitHub/dtls-fuzzer/suts/piondtls-2.0.9/dtls-clientserver -role client -port 23016 -cipherSuite TLS_PSK_WITH_AES_128_CCM_8

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of inputs: 4
Number of resets: 1
Number of bugs: 1
Time bug-checking took (ms): 1711

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (19): [Continue After CloseNotify, Continue After Fatal Alert, Early Finished, Finished Before CCS, Incorrect DecryptError Alert, Handshake completed with invalid message sequence, Multiple ChangeCipherSpec, Multiple Certificate, Multiple CertificateRequest, Multiple ServerKeyExchange, DH without ServerKeyExchange, Premature HelloRequest, Reorder Cert CertReq, Retransmitted Flight 5, ServerHello Flight Restart, Switching Cipher Suite, Unexpected ClientHello Response, Unrequested Certificate, Incorrect CertificateRequest Response]
Bug patterns found (1): [Handshake completed with invalid message sequence]
Bug patterns verified successfully (1): [Handshake completed with invalid message sequence]
Verification Inputs per Bug Pattern
   Handshake completed with invalid message sequence : 4

Verification Resets per Bug Pattern
   Handshake completed with invalid message sequence : 1

