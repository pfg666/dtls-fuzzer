
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 10

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: CertificateVerify-less Client Authentication
Severity: HIGH
Description: Handshake is missing CertificateVerify but has Certificate when certificate authentication is required.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CERTIFICATE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Certificate-less Client Authentication
Severity: HIGH
Description: Handshake is missing a client Certificate when certificate authentication is required.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 3
Detected the following bug patterns in flow
Pattern: ClientKeyExchange before Certificate
Severity: HIGH
Description: ClientKeyExchange appears before Certificate.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CERTIFICATE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 4
Detected the following bug patterns in flow
Pattern: Unauthenticated ClientKeyExchange
Severity: HIGH
Description: CertificateVerify appears before ClientKeyExchange.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE CERTIFICATE/TIMEOUT CERTIFICATE_VERIFY/TIMEOUT ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 5
Detected the following bug patterns in flow
Pattern: ChangeCipherSpec before CertificateVerify
Severity: LOW
Description: ChangeCipherSpec appears before CertificateVerify.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE/TIMEOUT FINISHED/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE_VERIFY/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 6
Detected the following bug patterns in flow
Pattern: Multiple ChangeCipherSpec
Severity: LOW
Description: Handshake contains repeated ChangeCipherSpec records.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE/TIMEOUT FINISHED/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT ECDH_CLIENT_HELLO/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 7
Detected the following bug patterns in flow
Pattern: Crash on CCS
Severity: LOW
Description: The server crashes after processing a CCS message.
Flow: Alert(WARNING,CLOSE_NOTIFY)/TIMEOUT ECDH_CLIENT_HELLO/TIMEOUT CHANGE_CIPHER_SPEC/SOCKET_CLOSED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 8
Detected the following bug patterns in flow
Pattern: Multiple Certificate
Severity: LOW
Description: Handshake contains repeated Certificate records.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE/TIMEOUT FINISHED/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 9
Detected the following bug patterns in flow
Pattern: Continue After CloseNotify
Severity: LOW
Description: Handshake continues after receipt/generation of a CloseNotify Alert.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT Alert(WARNING,CLOSE_NOTIFY)/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 10
Detected the following bug patterns in flow
Pattern: Continue After Fatal Alert
Severity: LOW
Description: Handshake continues after receipt/generation of a Fatal Alert.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT Alert(FATAL,UNEXPECTED_MESSAGE)/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: server
Alphabet: [ECDH_CLIENT_HELLO, ECDH_CLIENT_KEY_EXCHANGE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, CERTIFICATE, EMPTY_CERTIFICATE, CERTIFICATE_VERIFY, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (21): [Early Finished, CertificateVerify-less Client Authentication, Certificate-less Client Authentication, ClientKeyExchange before Certificate, Unauthenticated ClientKeyExchange, ChangeCipherSpec before CertificateVerify, Finished before ChangeCipherSpec, Multiple ChangeCipherSpec, Handshake Restarted, Invalid Finished as Retransmission, Non-conforming Cookie, Invalid HelloVerifyRequest Response, HelloVerifyRequest Retransmission, Crash on CCS, Insecure Renegotiation, Invalid DecryptError Alert, Internal Error on Finished, Invalid Handshake Start, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Bug Verification Enabled: true
Uncategorized Bug Bound: 10

--------------------------------------------------------------------------------
TLS SUL Parameters
--------------------------------------------------------------------------------

Protocol: DTLS12
ResetWait: 0
Timeout: 200
RunWait: 2000
Command: modules/jdk-12.0.2/bin/java -jar suts/jsse-12.0.2-dtls-clientserver.jar -port 10235 -hostname localhost -runWait 1000 -keyLocation experiments/keystore/rsa2048.jks -trustLocation experiments/keystore/rsa2048.jks -threadStarterIpPort localhost:60683 -operation FULL -auth NEEDED

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of inputs: 63
Number of resets: 10
Number of bugs: 10
Time bug-checking took (ms): 29149

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (21): [Early Finished, CertificateVerify-less Client Authentication, Certificate-less Client Authentication, ClientKeyExchange before Certificate, Unauthenticated ClientKeyExchange, ChangeCipherSpec before CertificateVerify, Finished before ChangeCipherSpec, Multiple ChangeCipherSpec, Handshake Restarted, Invalid Finished as Retransmission, Non-conforming Cookie, Invalid HelloVerifyRequest Response, HelloVerifyRequest Retransmission, Crash on CCS, Insecure Renegotiation, Invalid DecryptError Alert, Internal Error on Finished, Invalid Handshake Start, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Bug patterns found (10): [CertificateVerify-less Client Authentication, Certificate-less Client Authentication, ClientKeyExchange before Certificate, Unauthenticated ClientKeyExchange, ChangeCipherSpec before CertificateVerify, Multiple ChangeCipherSpec, Crash on CCS, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Bug patterns verified successfully (10): [CertificateVerify-less Client Authentication, Certificate-less Client Authentication, ClientKeyExchange before Certificate, Unauthenticated ClientKeyExchange, ChangeCipherSpec before CertificateVerify, Multiple ChangeCipherSpec, Crash on CCS, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Verification Inputs per Bug Pattern
   CertificateVerify-less Client Authentication : 6
   Certificate-less Client Authentication : 5
   ClientKeyExchange before Certificate : 6
   Unauthenticated ClientKeyExchange : 7
   ChangeCipherSpec before CertificateVerify : 8
   Multiple ChangeCipherSpec : 8
   Crash on CCS : 3
   Multiple Certificate : 8
   Continue After CloseNotify : 6
   Continue After Fatal Alert : 6

Verification Resets per Bug Pattern
   CertificateVerify-less Client Authentication : 1
   Certificate-less Client Authentication : 1
   ClientKeyExchange before Certificate : 1
   Unauthenticated ClientKeyExchange : 1
   ChangeCipherSpec before CertificateVerify : 1
   Multiple ChangeCipherSpec : 1
   Crash on CCS : 1
   Multiple Certificate : 1
   Continue After CloseNotify : 1
   Continue After Fatal Alert : 1

