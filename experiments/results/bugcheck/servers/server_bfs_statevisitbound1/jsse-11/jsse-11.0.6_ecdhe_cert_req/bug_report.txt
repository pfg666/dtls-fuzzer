
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 7

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: ClientKeyExchange before Certificate
Severity: HIGH
Description: ClientKeyExchange appears before Certificate.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CERTIFICATE/TIMEOUT CERTIFICATE_VERIFY/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 2
Detected the following bug patterns in flow
Pattern: ChangeCipherSpec before CertificateVerify
Severity: LOW
Description: ChangeCipherSpec appears before CertificateVerify.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE/TIMEOUT CERTIFICATE_VERIFY/TIMEOUT FINISHED/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT ECDH_CLIENT_HELLO/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 3
Detected the following bug patterns in flow
Pattern: Multiple ChangeCipherSpec
Severity: LOW
Description: Handshake contains repeated ChangeCipherSpec records.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE/TIMEOUT CERTIFICATE_VERIFY/TIMEOUT FINISHED/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT ECDH_CLIENT_HELLO/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 4
Detected the following bug patterns in flow
Pattern: Crash on CCS
Severity: LOW
Description: The server crashes after processing a CCS message.
Flow: Alert(WARNING,CLOSE_NOTIFY)/TIMEOUT ECDH_CLIENT_HELLO/TIMEOUT CHANGE_CIPHER_SPEC/SOCKET_CLOSED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 5
Detected the following bug patterns in flow
Pattern: Multiple Certificate
Severity: LOW
Description: Handshake contains repeated Certificate records.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE/TIMEOUT CERTIFICATE_VERIFY/TIMEOUT FINISHED/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT CERTIFICATE/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 6
Detected the following bug patterns in flow
Pattern: Continue After CloseNotify
Severity: LOW
Description: Handshake continues after receipt/generation of a CloseNotify Alert.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CERTIFICATE/TIMEOUT CERTIFICATE_VERIFY/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT Alert(WARNING,CLOSE_NOTIFY)/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 7
Detected the following bug patterns in flow
Pattern: Continue After Fatal Alert
Severity: LOW
Description: Handshake continues after receipt/generation of a Fatal Alert.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT CERTIFICATE/TIMEOUT CERTIFICATE_VERIFY/TIMEOUT CHANGE_CIPHER_SPEC/TIMEOUT Alert(FATAL,UNEXPECTED_MESSAGE)/TIMEOUT FINISHED/CHANGE_CIPHER_SPEC|FINISHED 
Verification Status: VERIFICATION_SUCCESSFUL


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: server
Alphabet: [ECDH_CLIENT_HELLO, ECDH_CLIENT_KEY_EXCHANGE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, CERTIFICATE, EMPTY_CERTIFICATE, CERTIFICATE_VERIFY, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (21): [Early Finished, CertificateVerify-less Client Authentication, Certificate-less Client Authentication, ClientKeyExchange before Certificate, Unauthenticated ClientKeyExchange, ChangeCipherSpec before CertificateVerify, Finished before ChangeCipherSpec, Multiple ChangeCipherSpec, Handshake Restarted, Invalid Finished as Retransmission, Non-conforming Cookie, Invalid HelloVerifyRequest Response, HelloVerifyRequest Retransmission, Crash on CCS, Insecure Renegotiation, Invalid DecryptError Alert, Internal Error on Finished, Invalid Handshake Start, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Bug Verification Enabled: true
Uncategorized Bug Bound: 10

--------------------------------------------------------------------------------
TLS SUL Parameters
--------------------------------------------------------------------------------

Protocol: DTLS12
ResetWait: 0
Timeout: 200
RunWait: 2000
Command: modules/jdk-11.0.6/bin/java -jar suts/jsse-11.0.6-dtls-clientserver.jar -port 11141 -hostname localhost -runWait 1000 -keyLocation experiments/keystore/rsa2048.jks -trustLocation experiments/keystore/rsa2048.jks -threadStarterIpPort localhost:44981 -operation FULL -auth NEEDED

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of inputs: 53
Number of resets: 7
Number of bugs: 7
Time bug-checking took (ms): 21617

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (21): [Early Finished, CertificateVerify-less Client Authentication, Certificate-less Client Authentication, ClientKeyExchange before Certificate, Unauthenticated ClientKeyExchange, ChangeCipherSpec before CertificateVerify, Finished before ChangeCipherSpec, Multiple ChangeCipherSpec, Handshake Restarted, Invalid Finished as Retransmission, Non-conforming Cookie, Invalid HelloVerifyRequest Response, HelloVerifyRequest Retransmission, Crash on CCS, Insecure Renegotiation, Invalid DecryptError Alert, Internal Error on Finished, Invalid Handshake Start, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Bug patterns found (7): [ClientKeyExchange before Certificate, ChangeCipherSpec before CertificateVerify, Multiple ChangeCipherSpec, Crash on CCS, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Bug patterns verified successfully (7): [ClientKeyExchange before Certificate, ChangeCipherSpec before CertificateVerify, Multiple ChangeCipherSpec, Crash on CCS, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Verification Inputs per Bug Pattern
   ClientKeyExchange before Certificate : 7
   ChangeCipherSpec before CertificateVerify : 9
   Multiple ChangeCipherSpec : 9
   Crash on CCS : 3
   Multiple Certificate : 9
   Continue After CloseNotify : 8
   Continue After Fatal Alert : 8

Verification Resets per Bug Pattern
   ClientKeyExchange before Certificate : 1
   ChangeCipherSpec before CertificateVerify : 1
   Multiple ChangeCipherSpec : 1
   Crash on CCS : 1
   Multiple Certificate : 1
   Continue After CloseNotify : 1
   Continue After Fatal Alert : 1

