
================================================================================
Bug Report
================================================================================

Total Number of Bugs Found: 3

--------------------------------------------------------------------------------
Listing Bugs
--------------------------------------------------------------------------------

Bug Id: 1
Detected the following bug patterns in flow
Pattern: Handshake Restarted
Severity: LOW
Description: The handshake was restarted in the middle of a current handshake using invalid message sequence numbers.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST PSK_CLIENT_HELLO/SERVER_HELLO|SERVER_HELLO_DONE ECDH_CLIENT_HELLO/TIMEOUT ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 2
Detected the following bug patterns in flow
Pattern: Non-conforming Cookie
Severity: LOW
Description: The first two ClientHello messages use different cipher suites, indicating that cipher suites are not included in the cookie computation.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST DH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|DHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE 
Verification Status: VERIFICATION_SUCCESSFUL

Bug Id: 3
Detected the following bug patterns in flow
Pattern: Invalid Handshake Start
Severity: LOW
Description: The handshake was started using an invalid sequence of messages.
Flow: ECDH_CLIENT_HELLO/HELLO_VERIFY_REQUEST ECDH_CLIENT_KEY_EXCHANGE/TIMEOUT ECDH_CLIENT_HELLO/SERVER_HELLO|RSA_CERTIFICATE|ECDHE_SERVER_KEY_EXCHANGE|CERTIFICATE_REQUEST|SERVER_HELLO_DONE 
Verification Status: VERIFICATION_SUCCESSFUL


================================================================================
Run Description
================================================================================


--------------------------------------------------------------------------------
Bug Checking Parameters
--------------------------------------------------------------------------------

SUT: server
Alphabet: [ECDH_CLIENT_HELLO, ECDH_CLIENT_KEY_EXCHANGE, DH_CLIENT_HELLO, DH_CLIENT_KEY_EXCHANGE, PSK_CLIENT_HELLO, PSK_CLIENT_KEY_EXCHANGE, RSA_CLIENT_HELLO, RSA_CLIENT_KEY_EXCHANGE, CHANGE_CIPHER_SPEC, FINISHED, APPLICATION, CERTIFICATE, EMPTY_CERTIFICATE, CERTIFICATE_VERIFY, Alert(WARNING,CLOSE_NOTIFY), Alert(FATAL,UNEXPECTED_MESSAGE)]
Loaded Bug Patterns (21): [Early Finished, CertificateVerify-less Client Authentication, Certificate-less Client Authentication, ClientKeyExchange before Certificate, Unauthenticated ClientKeyExchange, ChangeCipherSpec before CertificateVerify, Finished before ChangeCipherSpec, Multiple ChangeCipherSpec, Handshake Restarted, Invalid Finished as Retransmission, Non-conforming Cookie, Invalid HelloVerifyRequest Response, HelloVerifyRequest Retransmission, Crash on CCS, Insecure Renegotiation, Invalid DecryptError Alert, Internal Error on Finished, Invalid Handshake Start, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Bug Verification Enabled: true
Uncategorized Bug Bound: 10

--------------------------------------------------------------------------------
TLS SUL Parameters
--------------------------------------------------------------------------------

Protocol: DTLS12
ResetWait: 0
Timeout: 100
RunWait: 100
Command: /home/pfg666/GitHub/dtls-fuzzer/suts/mbedtls-2.16.1/programs/ssl/ssl_server2 dtls=1 psk=1234 mtu=5000 key_file=/home/pfg666/GitHub/dtls-fuzzer/experiments/keystore/rsa2048_key.pem crt_file=/home/pfg666/GitHub/dtls-fuzzer/experiments/keystore/rsa2048_cert.pem server_port=29244 exchanges=100 hs_timeout=20000-120000 ca_file=/home/pfg666/GitHub/dtls-fuzzer/experiments/keystore/rsa2048_cert.pem auth_mode=required renegotiation=1

================================================================================
Statistics
================================================================================


--------------------------------------------------------------------------------
General
--------------------------------------------------------------------------------

Number of inputs: 9
Number of resets: 3
Number of bugs: 3
Time bug-checking took (ms): 3224

--------------------------------------------------------------------------------
Model Bug Finder
--------------------------------------------------------------------------------

Bug patterns loaded (21): [Early Finished, CertificateVerify-less Client Authentication, Certificate-less Client Authentication, ClientKeyExchange before Certificate, Unauthenticated ClientKeyExchange, ChangeCipherSpec before CertificateVerify, Finished before ChangeCipherSpec, Multiple ChangeCipherSpec, Handshake Restarted, Invalid Finished as Retransmission, Non-conforming Cookie, Invalid HelloVerifyRequest Response, HelloVerifyRequest Retransmission, Crash on CCS, Insecure Renegotiation, Invalid DecryptError Alert, Internal Error on Finished, Invalid Handshake Start, Multiple Certificate, Continue After CloseNotify, Continue After Fatal Alert]
Bug patterns found (3): [Handshake Restarted, Non-conforming Cookie, Invalid Handshake Start]
Bug patterns verified successfully (3): [Handshake Restarted, Non-conforming Cookie, Invalid Handshake Start]
Verification Inputs per Bug Pattern
   Handshake Restarted : 4
   Non-conforming Cookie : 2
   Invalid Handshake Start : 3

Verification Resets per Bug Pattern
   Handshake Restarted : 1
   Non-conforming Cookie : 1
   Invalid Handshake Start : 1

