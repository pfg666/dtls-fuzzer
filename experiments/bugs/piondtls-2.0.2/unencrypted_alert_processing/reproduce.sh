readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

readonly PIONDTLS_VERSION="2.0.4"
readonly PIONDTLS_V2="piondtls-$PIONDTLS_VERSION" # likely works with other v2.x.x versions
readonly PIONDTLS_V2_ARCH_URL="https://github.com/assist-project/pion-dtls-examples/archive/v$PIONDTLS_VERSION.tar.gz"
#https://github.com/assist-project/pion-dtls-examples/archive/v2.0.4.tar.gz

readonly TLSATTACKER_VER="3.0b"
readonly TLSATTACKER_FULLNAME="TLS-Attacker-$TLSATTACKER_VER"
readonly TLSATTACKER_ARCH_URL="https://github.com/RUB-NDS/TLS-Attacker/archive/$TLSATTACKER_VER.tar.gz"


# downloads an archive and unpacks it
function solve_arch() {
    arch_url=$1
    target_dir=$2
    temp_dir=/tmp/`(basename $arch_url)`
    echo $temp_dir
    echo "Fetching/unpacking from $arch_url into $target_dir"
    if [[ ! -f "$temp_dir" ]]
    then
        echo "Downloading archive from url to $temp_dir"
        wget -nc --no-check-certificate $arch_url -O $temp_dir
    fi
    
    mkdir $target_dir
    # ${temp_dir##*.} retrieves the substring between the last index of . and the end of $temp_dir
    arch=`echo "${temp_dir##*.}"`
    if [[ $arch == "xz" ]]
    then
        tar_param="-xJf"
    else 
        tar_param="zxvf"
    fi
    echo $tar_param
    if [ $target_dir ] ; then
        tar $tar_param $temp_dir -C $target_dir --strip-components=1
    else 
        tar $tar_param $temp_dir
    fi
}

# builds PionDTLS
function make_piondtls() {
    sut_dir=$1
    ( cd $sut_dir; go build -o dtls-clientserver main/main.go )
}

# sets up PionDTLS server program in $PIONDTLS_V2 directory
function setup_piondtls() {
    if [[ ! -d $PIONDTLS_V2 ]]
    then 
        # install the right version
        go get github.com/pion/dtls/v2@v$PIONDTLS_VERSION
        # download PionDTLS test program
        solve_arch $PIONDTLS_V2_ARCH_URL $PIONDTLS_V2
        # build it
        make_piondtls $PIONDTLS_V2
    fi
}

# sets up a TLS-Attacker 3.0b
function setup_tlsattacker() {
    if [[ ! -d $TLSATTACKER_FULLNAME ]]
    then 
        solve_arch $TLSATTACKER_ARCH_URL $TLSATTACKER_FULLNAME
        ( cd $TLSATTACKER_FULLNAME; mvn install -DskipTests )
    fi
}

cd $SCRIPT_DIR

# setting up TLS-Attacker and PionDTLS
setup_piondtls
setup_tlsattacker

# run PionDTLS server using PSK cipher suites to keep things simple
( cd $PIONDTLS_V2; ./dtls-clientserver -role server -port 20220 -cipherSuite TLS_PSK_WITH_AES_128_CCM_8 ) &

# run TLS-Attacker to execute workflow with malicious alert message
java -jar $TLSATTACKER_FULLNAME/apps/TLS-Client.jar -connect localhost:20220 -version DTLS12 -workflow_input workflow.xml  -config tlsattacker.config

# kill leftover processes
kill $!
bash stop_proc_at_port.sh 20220 server