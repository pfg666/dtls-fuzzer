### Description
- Type: Bug
- Priority: Minor 

## Non-conformance Bug
**OS**  
Linux, Ubuntu 20

**mbed TLS build:**  
Version: 2.26.0
Configuration: vanilla configuration

**Affected Versions**
DTLS 1.2

**Expected behavior**
According to the [TLS 1.2. RFC](https://tools.ietf.org/html/rfc5246#page-38), the client should ignore HelloRequest if received in the middle of a handshake. 

> The one message that is not bound by these ordering rules
>   is the HelloRequest message, which can be sent at any time, but which
>   SHOULD be ignored by the client if it arrives in the middle of a
 >  handshake.

**Actual Behavior**
Our testing shows that the MbedTLS client does not conform to the standard. It responds to a mid-handshake HelloRequest by re-transmitting the original ClientHello. This violation does not appear to have any security or interoperability consequences, but it does indicate that the state machine in the [DTLS RFC ](https://www.rfc-editor.org/rfc/rfc6347.html#page-22) is not followed properly.

**Steps to Reproduce**
I attached files necessary for reproduction using [TLS-Attacker](https://github.com/tls-attacker/TLS-Attacker), a Java-based tool for testing TLS libraries. Also included in the archive is a capture of the interaction,  generated on my machine. TLS-Attacker requires  the JDK for Java 8. On Ubuntu, this can be installed  by running:
`sudo apt-get install openjdk-8-jdk`

Unpack the archive, `cd` to resulting folder and run `bash reproduce.sh`, while running an instance of Wireshark on the side. The reproduction script will: 
* setup MbedTLS and Tls-Attacker;
* launch an MbedTLS client, configured with a high DTLS timeout value (to avoid retransmissions);
* launch TLS-Attacker, execute test.
 
If everything works as planned, Wireshark should show an interaction similar to that in the image below:

![hello_request](https://user-images.githubusercontent.com/2325013/115994229-615cfb00-a5d6-11eb-91ed-5aa2d3cea3cb.png)

Notice the third ClientHello, which the client sends in response to HelloRequest.

[reproduction.tar.gz](https://github.com/ARMmbed/mbedtls/files/6371977/reproduction.tar.gz)

Thanks!

