readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

readonly MBEDTLS="mbedtls-2.26.0"
readonly MBEDTLS_ARCH_URL="https://github.com/ARMmbed/mbedtls/archive/$MBEDTLS.tar.gz"

readonly TLSATTACKER_VER="3.0b"
readonly TLSATTACKER_FULLNAME="TLS-Attacker-$TLSATTACKER_VER"
readonly TLSATTACKER_ARCH_URL="https://github.com/RUB-NDS/TLS-Attacker/archive/$TLSATTACKER_VER.tar.gz"

readonly WORKFLOW="$SCRIPT_DIR/workflow.xml"

readonly PORT=20000

# downloads and unpacks an archive
function solve_arch() {
    arch_url=$1
    target_dir=$2
    temp_dir=/tmp/`(basename $arch_url)`
    echo $temp_dir
    echo "Fetching/unpacking from $arch_url into $target_dir"
    if [[ ! -f "$temp_dir" ]]
    then
        echo "Downloading archive from url to $temp_dir"
        wget -nc --no-check-certificate $arch_url -O $temp_dir
    fi
    
    mkdir $target_dir
    # ${temp_dir##*.} retrieves the substring between the last index of . and the end of $temp_dir
    arch=`echo "${temp_dir##*.}"`
    if [[ $arch == "xz" ]]
    then
        tar_param="-xJf"
    else 
        tar_param="zxvf"
    fi
    echo $tar_param
    if [ $target_dir ] ; then
        tar $tar_param $temp_dir -C $target_dir --strip-components=1
    else 
        tar $tar_param $temp_dir
    fi
}


function make_mbedtls() {
    mbedtls_dir=$1
    (
        cd $mbedtls_dir
        make
    )
}

# downloads and builds MbedTLS
function setup_mbedtls() {
    if [[ ! -d $MBEDTLS ]]
    then
        solve_arch $MBEDTLS_ARCH_URL $MBEDTLS
        make_mbedtls $MBEDTLS
    fi
}

# downloads and builds TLS-Attacker
function setup_tlsattacker() {
    if [[ ! -d $TLSATTACKER_FULLNAME ]]
    then
        solve_arch $TLSATTACKER_ARCH_URL $TLSATTACKER_FULLNAME
        ( cd $TLSATTACKER_FULLNAME; mvn install -DskipTests )
    fi
}

# launches MbedTLS client, note that setting the timeout to a high value is essential (otherwise we get retransmissions)
function launch_mbedtls_client() {

    echo "Launching mbedtls client"
    echo $MBEDTLS/programs/ssl/ssl_client2 dtls=1 psk=1234 server_port=$PORT exchanges=1 hs_timeout=20000-120000 
    $MBEDTLS/programs/ssl/ssl_client2 dtls=1 psk=1234 server_port=$PORT exchanges=1 hs_timeout=20000-120000 
}

# runs TLS-Attacker's TLS-Server utility to execute the workflow
function launch_tls-server() {
    client_workflow=$1

    echo "Launching TLS-Server"
    echo java -jar $TLSATTACKER_FULLNAME/apps/TLS-Server.jar -port $PORT -version DTLS12 -workflow_input $client_workflow  -config tlsattacker.config
    java -jar $TLSATTACKER_FULLNAME/apps/TLS-Server.jar -port $PORT -version DTLS12 -workflow_input $client_workflow  -config tlsattacker.config
}


cd $SCRIPT_DIR

# setting up MbedTLS and TLS-Attacker (which needs JDK 8)
setup_mbedtls
setup_tlsattacker

# runs mbedtls_client after a brief delay, stores the PID
(sleep 2; launch_mbedtls_client ) &
mbedtls_pid=$!

echo $mbedtls_pid

# launches TLS-Server utility to execute the test case
launch_tls-server $WORKFLOW

# killing left-over processes (some remain, but will terminate shortly)
kill $mbedtls_pid