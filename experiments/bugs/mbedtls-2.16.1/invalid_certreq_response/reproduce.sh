readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

readonly OPENSSL="openssl-1.1.1g"
readonly OPENSSL_ARCH_URL='https://www.openssl.org/source/openssl-1.1.1g.tar.gz'

readonly MBEDTLS="mbedtls-2.25.0"
readonly MBEDTLS_ARCH_URL='https://github.com/ARMmbed/mbedtls/archive/mbedtls-2.25.0.tar.gz'

readonly KEY=rsa2048_key.pem
readonly CERT=rsa2048_cert.pem
OPENSSL_PID=100000
MBEDTLS_PID=100000

# downloads and unpacks an archive
function solve_arch() {
    arch_url=$1
    target_dir=$2
    temp_dir=/tmp/`(basename $arch_url)`
    echo $temp_dir
    echo "Fetching/unpacking from $arch_url into $target_dir"
    if [[ ! -f "$temp_dir" ]]
    then
        echo "Downloading archive from url to $temp_dir"
        wget -nc --no-check-certificate $arch_url -O $temp_dir
    fi
    
    mkdir $target_dir
    # ${temp_dir##*.} retrieves the substring between the last index of . and the end of $temp_dir
    arch=`echo "${temp_dir##*.}"`
    if [[ $arch == "xz" ]]
    then
        tar_param="-xJf"
    else 
        tar_param="zxvf"
    fi
    echo $tar_param
    if [ $target_dir ] ; then
        tar $tar_param $temp_dir -C $target_dir --strip-components=1
    else 
        tar $tar_param $temp_dir
    fi
}

# builds the OpenSSL 1.1.1g
function make_openssl() {
    openssl_dir=$1
    (
        cd $openssl_dir
        ./config
        make
    )
}

# downloads and builds OpenSSL
function setup_openssl() {
    if [[ ! -d $OPENSSL ]]
    then 
        solve_arch $OPENSSL_ARCH_URL $OPENSSL
        make_openssl $OPENSSL
    fi
}

function make_mbedtls() {
    mbedtls_dir=$1
    (
        cd $mbedtls_dir
        make
    )
}

# downloads and builds MbedTLS
function setup_mbedtls() {
    if [[ ! -d $MBEDTLS ]]
    then
        solve_arch $MBEDTLS_ARCH_URL $MBEDTLS
        make_mbedtls $MBEDTLS
    fi
}


function launch_openssl() {
    # '-verify 1' means that authentication is only optional, '-client_sigalgs  ECDSA+SHA512' restricts the certificate types allowed for authentication 
    echo Command to execute OpenSSL server:
    echo LD_LIBRARY_PATH=$SCRIPT_DIR/$OPENSSL $OPENSSL/apps/openssl s_server -mtu 5000 -client_sigalgs  ECDSA+SHA512 -key $KEY -cert $CERT -CAfile $CERT -verify 1 -accept 20000 -dtls1_2 -timeout 5000  
}

function launch_mbedtls() {
    echo Command to execute MbedTLS client:
    echo $MBEDTLS/programs/ssl/ssl_client2 dtls=1 key_file=$KEY crt_file=$CERT server_port=20000 exchanges=1 ca_file=$CERT auth_mode=optional
}


cd $SCRIPT_DIR

# setting up MbedTLS and OpenSSL
setup_mbedtls
setup_openssl

# unfortunately, executing OpenSSL in background kills it, hence we just echo the commands to execute
echo Execute the following commands in two separate terminals 
launch_openssl
launch_mbedtls