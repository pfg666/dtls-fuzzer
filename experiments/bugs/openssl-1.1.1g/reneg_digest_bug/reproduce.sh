readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

readonly SUT="openssl-1.1.1g"
readonly SUT_ARCH_URL='https://www.openssl.org/source/openssl-1.1.1g.tar.gz'
SUT_PID=100000
readonly TLSATTACKER_VER="3.0b"
readonly TLSATTACKER_FULLNAME="TLS-Attacker-$TLSATTACKER_VER"
readonly TLSATTACKER_ARCH_URL="https://github.com/RUB-NDS/TLS-Attacker/archive/$TLSATTACKER_VER.tar.gz"
TLSATTACKER_PID=100000

# downloads and unpacks and archive
function solve_arch() {
    arch_url=$1
    target_dir=$2
    temp_dir=/tmp/`(basename $arch_url)`
    echo $temp_dir
    echo "Fetching/unpacking from $arch_url into $target_dir"
    if [[ ! -f "$temp_dir" ]]
    then
        echo "Downloading archive from url to $temp_dir"
        wget -nc --no-check-certificate $arch_url -O $temp_dir
    fi
    
    mkdir $target_dir
    # ${temp_dir##*.} retrieves the substring between the last index of . and the end of $temp_dir
    arch=`echo "${temp_dir##*.}"`
    if [[ $arch == "xz" ]]
    then
        tar_param="-xJf"
    else 
        tar_param="zxvf"
    fi
    echo $tar_param
    if [ $target_dir ] ; then
        tar $tar_param $temp_dir -C $target_dir --strip-components=1
    else 
        tar $tar_param $temp_dir
    fi
}

# Applies patches for SUTs that require them
function apply_patch() {
    sut=$1
    sut_dir=$1
    sut_patch=$SCRIPT_DIR/$sut.patch
    
    if [[ -f $sut_patch ]] 
    then
        echo "Applying patch $sut_patch"
        rep_url=`get_rep_url $sut`
        if [[ -n "$rep_url" ]]; then
            echo "via git apply"
            ( cd $sut_dir; git apply $sut_patch )
        else 
            echo "via patch"
            patch -s -p0 < $sut_patch
        fi
    fi
}

# builds the sut
function make_sut() {
    sut_dir=$1
    (
        cd $sut_dir
        ./config
        make
    )
}

function setup_tlsattacker() {
    if [[ ! -d $TLSATTACKER_FULLNAME ]]
    then 
        solve_arch $TLSATTACKER_ARCH_URL $TLSATTACKER_FULLNAME
        ( cd $TLSATTACKER_FULLNAME; mvn install -DskipTests )
    fi
}

function setup_sut() {
    if [[ ! -d $SUT ]]
    then 
        solve_arch $SUT_ARCH_URL $SUT
        apply_patch $SUT
        make_sut $SUT
    fi
}

function launch_sut() {
    #( LD_LIBRARY_PATH=$SCRIPT_DIR/$SUT $SUT/apps/openssl s_client -psk 1234 -port 11111 -dtls1_2 -mtu 5000 ) &
    LD_LIBRARY_PATH=$SCRIPT_DIR/$SUT $SUT/apps/openssl s_client -psk 1234 -port 11111 -dtls1_2 -mtu 5000 
    SUT_PID=$!
}

function launch_tlsattacker() {
    ( java -jar TLS-Attacker-3.0b/apps/TLS-Server.jar -port 11111 -config tlsattacker.config -version DTLS12 -workflow_trace_type SERVER_RENEGOTIATION -cipher TLS_PSK_WITH_AES_256_CBC_SHA384 ) &
    TLSATTACKER_PID=$!
}


cd $SCRIPT_DIR

# setting up TLS-Attacker and the DTLS SUT
setup_sut
setup_tlsattacker

launch_tlsattacker

sleep 7

launch_sut

#sleep 20
#wait $SUT_PID

kill $TLSATTACKER_PID

#kill $SUT_PID

bash stop_proc_at_port.sh 11111 server
bash stop_proc_at_port.sh 11111 client
