### Description
- Type: Bug
- Priority: Minor 

## Non-conformance Bug
**OS**
Linux, Ubuntu 20

**OpenSSL build**
Version: 1.1.1k

**Affected Versions**
DTLS 1.2

**Expected behavior**
The Client Certificate message is only sent if the server requests a certificate.

**Actual behavior**
Our testing has shown a case where an OpenSSL client can send a Certificate in a renegotiated handshake if the server sent a CertificateRequest in the previous handshake. 

1. -> ServerHello
2. -> Certificate
3. -> ServerHelloDone
4. <- ClientKeyExchange
5. <- ChangeCipherSpec
6. <- Finished
7. -> ChangeCipherSpec
8. -> Finished
9. -> **CertificateRequest**
10. -> **HelloRequest**
11. <- ClientHello
12. -> HelloVerifyRequest
13. <- ClientHello
14. -> ServerHello
15. -> Certificate
16. -> Certificate
17. -> ServerHelloDone
18. <- **Certificate**
19. <- ClientKeyExchange
20. <- CertificateVerify
21. <- ChangeCipherSpec
22. <- Finished

**Steps to Reproduce**
I have attached the files necessary for reproduction using [DTLS-fuzzer](https://gitlab.com/pfg666/dtls-fuzzer). This is an extension of [TLS-Attacker](https://github.com/tls-attacker/TLS-Attacker), a Java-based tool for testing TLS libraries. DTLS-fuzzer requires  the JDK for Java 8. On Ubuntu, this can be installed  by running:
`sudo apt-get install openjdk-8-jdk`

The steps of reproduction are as follows:

1. Extract the archive and `cd` to the directory.
2. Run `bash setup_reproduction.sh` to install a local copy of an OpenSSL-1.1.1k client.
3. Run `java -jar dtls-fuzzer.jar multiple_cert` and wait until it outputs `[main] ERROR: TlsSUL - Start 0`
4. Open a new terminal and `cd` to the directory. From there, run 
`openssl-1.1.1k/apps/openssl s_client -psk 1234 -key dtls-fuzzer/rsa2048_key.pem -cert dtls-fuzzer/rsa2048_cert.pem -CAfile dtls-fuzzer/rsa2048_cert.pem -port 20000 -dtls1_2 -mtu 5000`

If everything works properly, the final lines of the output from DTLS-fuzzer should look something like this:  
`08:49:33 [main] INFO : TestRunner -`  
`- / CLIENT_HELLO`  
`RSA_SERVER_HELLO / -`  
`CERTIFICATE / -`  
`SERVER_HELLO_DONE / RSA_CLIENT_KEY_EXCHANGE; CHANGE_CIPHER_SPEC; FINISHED`  
`CHANGE_CIPHER_SPEC / -`  
`FINISHED / -`  
`RSA_SIGN_CERTIFICATE_REQUEST / -`  
`HELLO_REQUEST / CLIENT_HELLO `  
`HELLO_VERIFY_REQUEST / CLIENT_HELLO`  
`RSA_SERVER_HELLO / -`  
`CERTIFICATE / -`  
`CERTIFICATE / -`  
`SERVER_HELLO_DONE / RSA_CERTIFICATE; RSA_CLIENT_KEY_EXCHANGE; CERTIFICATE_VERIFY; CHANGE_CIPHER_SPEC; FINISHED`  
`Alert(WARNING,CLOSE_NOTIFY) / -`

This shows the sequence of messages sent to the client with corresponding output.

For a version with multiple ServerKeyExchange, run DTLS-fuzzer with `java -jar dtls-fuzzer.jar multiple_ske` instead. Also included are the `multiple_cert_auto` and `multiple_ske_auto`. Running DTLS-fuzzer with these will also automatically start the OpenSSL client.

