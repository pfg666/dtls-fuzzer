readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

readonly OPENSSL="openssl-1.1.1k"
readonly OPENSSL_ARCH_URL='https://www.openssl.org/source/openssl-1.1.1k.tar.gz'

# downloads and unpacks an archive
function solve_arch() {
    arch_url=$1
    target_dir=$2
    temp_dir=/tmp/`(basename $arch_url)`
    echo "Fetching/unpacking from $arch_url into $target_dir"
    if [[ ! -f "$temp_dir" ]]
    then
        echo "Downloading archive from url to $temp_dir"
        wget -nc --no-check-certificate $arch_url -O $temp_dir
    fi
    
    mkdir $target_dir
    # ${temp_dir##*.} retrieves the substring between the last index of . and the end of $temp_dir
    arch=`echo "${temp_dir##*.}"`
    if [[ $arch == "xz" ]]
    then
        tar_param="-xJf"
    else 
        tar_param="zxvf"
    fi
    echo $tar_param
    if [ $target_dir ] ; then
        tar $tar_param $temp_dir -C $target_dir --strip-components=1
    else 
        tar $tar_param $temp_dir
    fi
}

# applies a patch to apps/s_server.c 
function patch_s_server() {
    openssl_dir=$1
    echo "Patching up apps/s_server.c program"
    patch $openssl_dir/apps/s_server.c  < s_server.patch
}

# builds the OpenSSL 1.1.1k
function make_openssl() {
    openssl_dir=$1
    echo "Building OpenSSL"
    (
        cd $openssl_dir
        ./config -static -d
        make
    )
}

# downloads and builds OpenSSL
function setup_openssl() {
    if [[ ! -d $OPENSSL ]]
    then 
        solve_arch $OPENSSL_ARCH_URL $OPENSSL
        patch_s_server $OPENSSL
        make_openssl $OPENSSL
    else
        echo "OpenSSL already set up in $OPENSSL, so not setting it up again."
    fi
}


cd $SCRIPT_DIR

# setting up OpenSSL
setup_openssl

echo
echo "-----------------------------------"
echo
echo "Command to run dtls-fuzzer:"
echo java -jar dtls-fuzzer.jar multiple_cert
echo
echo "Command to run openssl client:"
echo openssl-1.1.1k/apps/openssl s_client -psk 1234 -key dtls-fuzzer/rsa2048_key.pem -cert dtls-fuzzer/rsa2048_cert.pem -CAfile dtls-fuzzer/rsa2048_cert.pem -port 20000 -dtls1_2 -mtu 5000
echo "Start dtls-fuzzer first, then run openssl client in a separate terminal."
