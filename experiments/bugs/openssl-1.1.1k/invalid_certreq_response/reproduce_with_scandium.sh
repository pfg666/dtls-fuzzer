readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

readonly OPENSSL="openssl-1.1.1k"
readonly OPENSSL_ARCH_URL='https://www.openssl.org/source/openssl-1.1.1k.tar.gz'

readonly KEY=rsa2048_key.pem
readonly CERT=rsa2048_cert.pem
readonly PORT=20000

readonly SCANDIUM="scandium-2.3.0-dtls-server.jar"

# downloads and unpacks an archive
function solve_arch() {
    arch_url=$1
    target_dir=$2
    temp_dir=/tmp/`(basename $arch_url)`
    echo "Fetching/unpacking from $arch_url into $target_dir"
    if [[ ! -f "$temp_dir" ]]
    then
        echo "Downloading archive from url to $temp_dir"
        wget -nc --no-check-certificate $arch_url -O $temp_dir
    fi
    
    mkdir $target_dir
    # ${temp_dir##*.} retrieves the substring between the last index of . and the end of $temp_dir
    arch=`echo "${temp_dir##*.}"`
    if [[ $arch == "xz" ]]
    then
        tar_param="-xJf"
    else 
        tar_param="zxvf"
    fi
    echo $tar_param
    if [ $target_dir ] ; then
        tar $tar_param $temp_dir -C $target_dir --strip-components=1
    else 
        tar $tar_param $temp_dir
    fi
}

# applies a patch to apps/s_server.c 
function patch_s_server() {
    openssl_dir=$1
    echo "Patching up apps/s_server.c program"
    patch $openssl_dir/apps/s_server.c  < s_server.patch
}

# builds the OpenSSL 1.1.1g
function make_openssl() {
    openssl_dir=$1
    echo "Building OpenSSL"
    (
        cd $openssl_dir
        ./config -static -d
        make
    )
}

# downloads and builds OpenSSL
function setup_openssl() {
    if [[ ! -d $OPENSSL ]]
    then 
        solve_arch $OPENSSL_ARCH_URL $OPENSSL
        patch_s_server $OPENSSL
        make_openssl $OPENSSL
    else
        echo "OpenSSL already set up in $OPENSSL, so not setting it up again."
    fi
}


function launch_openssl_server() {
    port=$1

    echo Command to execute OpenSSL server:
    echo $OPENSSL/apps/openssl s_server -accept $port -mtu 5000 -key $KEY -cert $CERT -CAfile $CERT -verify 1 -dtls1_2 -timeout 5000
}

function launch_openssl_client() {
    port=$1

    echo Command to execute OpenSSL client:
    echo $OPENSSL/apps/openssl s_client -port $port -key $KEY -cert $CERT -CAfile $CERT -dtls1_2 -mtu 5000
}

function launch_scandium_server() {
    port=$1

    echo java -jar $SCANDIUM -port $port -timeout 20000000 -trustLocation ec_secp256r1.jks -keyLocation ec_secp256r1.jks -clientAuth OPTIONAL
}


cd $SCRIPT_DIR

# setting up OpenSSL
setup_openssl

# Scandium is provided as jar 

# unfortunately, executing OpenSSL in background kills it, hence we just echo the commands to execute
echo Execute the following commands in separate terminals to reproduce non-conforming behavior in a OpenSSL client/server interaction
launch_openssl_server $PORT
launch_openssl_client $PORT
echo
echo
echo Execute the following commands in separate terminals to reproduce interoperability problem in a OpenSSL client/Scandium DTLS server interaction
echo Not passing certificate arguments to s_client will enable it to complete handshake with Scandium server.
echo Requires Java 8 JDK
launch_scandium_server $PORT
launch_openssl_client $PORT


