#    1. create a reproduction directory, change directory to it and place in it all the files attached in this email

#        mkdir reproduction
#
#        cd reproduction
#
#        (place attachment files in 'reproduction')

#    2. download, unpack and install TLS-Attacker 3.0b. TLS-Attacker requires Java 8 (JDK) and Maven (mvn utility).

        wget -nc --no-check-certificate https://github.com/RUB-NDS/TLS-Attacker/archive/3.0b.tar.gz -O TLS-Attacker-3.0b.tar.gz

        tar zxvf TLS-Attacker-3.0b.tar.gz

        cd TLS-Attacker-3.0b

        mvn install -DskipTests

        cd ..

#    3. download, unpack and build WolfSSL, enabling DTLS in the configuration

        wget -nc --no-check-certificate  https://github.com/wolfSSL/wolfssl/archive/v4.4.0-stable.tar.gz -O wolfssl-4.4.0.tar.gz

        tar zxvf wolfssl-4.4.0.tar.gz

        cd wolfssl-4.4.0-stable 

        bash autogen.sh

        ./configure --enable-dtls

        make

        cd ..

#    4. in one terminal launch the echoserver utility

        ( 
            cd wolfssl-4.4.0-stable
            examples/echoserver/echoserver 
        ) &

#    5. in a separate terminal launch TLS-Attacker's TLS-Client utility, telling it to execute a workflow 'workflow.xml' using the configuration 'tlsattacker.config', both files attached in this email
        
        java -jar TLS-Attacker-3.0b/apps/TLS-Client.jar -connect localhost:11111 -version DTLS12 -workflow_input workflow.xml  -config tlsattacker.config

        kill $!
        bash stop_proc_at_port.sh 11111 server

