readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

readonly WOLFSSL_440="wolfssl-4.4.0"
readonly WOLFSSL_440_PATCH="wolfssl-4.4.0.patch"
readonly WOLFSSL_440_ARCH_URL='https://github.com/wolfSSL/wolfssl/archive/v4.4.0-stable.tar.gz'
readonly TLSATTACKER_VER="3.0b"
readonly TLSATTACKER_FULLNAME="TLS-Attacker-$TLSATTACKER_VER"
readonly TLSATTACKER_ARCH_URL="https://github.com/RUB-NDS/TLS-Attacker/archive/$TLSATTACKER_VER.tar.gz"


# downloads and unpacks and archive
function solve_arch() {
    arch_url=$1
    target_dir=$2
    temp_dir=/tmp/`(basename $arch_url)`
    echo $temp_dir
    echo "Fetching/unpacking from $arch_url into $target_dir"
    if [[ ! -f "$temp_dir" ]]
    then
        echo "Downloading archive from url to $temp_dir"
        wget -nc --no-check-certificate $arch_url -O $temp_dir
    fi
    
    mkdir $target_dir
    # ${temp_dir##*.} retrieves the substring between the last index of . and the end of $temp_dir
    arch=`echo "${temp_dir##*.}"`
    if [[ $arch == "xz" ]]
    then
        tar_param="-xJf"
    else 
        tar_param="zxvf"
    fi
    echo $tar_param
    if [ $target_dir ] ; then
        tar $tar_param $temp_dir -C $target_dir --strip-components=1
    else 
        tar $tar_param $temp_dir
    fi
}

# applies patches to WolfSSL's echo example to make it work with PSK
function apply_patch() {
    sut_dir=$1
    #$(echo $sut | cut -d '-' -f 1)
    sut_patch=$2
    if [[ -f $sut_patch ]] 
    then
        echo "Applying patch $sut_patch"
        patch -s -p0 < $sut_patch
    fi
}

# builds wolfssl
function make_wolfssl() {
    sut_dir=$1
    (
        cd $sut_dir
        bash autogen.sh
        ./configure --enable-dtls 
        make
    )
}

function setup_tlsattacker() {
    if [[ ! -d $TLSATTACKER_FULLNAME ]]
    then
        solve_arch $TLSATTACKER_ARCH_URL $TLSATTACKER_FULLNAME
        ( cd $TLSATTACKER_FULLNAME; mvn install -DskipTests )
    fi
}

function setup_wolfssl() {
    if [[ ! -d $WOLFSSL_440 ]]
    then 
        solve_arch $WOLFSSL_440_ARCH_URL $WOLFSSL_440
        make_wolfssl $WOLFSSL_440
    fi
}

cd $SCRIPT_DIR

# setting up TLS-Attacker and WolfSSL
setup_wolfssl
setup_tlsattacker

# runs WolfSSL echo server
( cd $WOLFSSL_440; ./examples/echoserver/echoserver ) &

# runs TLS-Attacker with problematic server
java -jar $TLSATTACKER_FULLNAME/apps/TLS-Client.jar -connect localhost:11111 -version DTLS12 -workflow_input workflow.xml  -config tlsattacker.config

# kills leftover processes
kill $!
bash stop_proc_at_port.sh 11111 server
